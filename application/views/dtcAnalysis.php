<?php include "header.php" ?>
  <script src="<?php echo HTTP_JS_PATH; ?>scripts/dtc.js"></script>
  <link rel="stylesheet" href="<?php echo HTTP_CSS_PATH; ?>styles/dtc.css">
  <div class="page-content">
    <div class="page-header">
      <div class="container-fluid">
        <h2 class="h5 no-margin-bottom">DTC Distribution</h2>
      </div>
    </div>
    <!-- Breadcrumb-->
    <div class="container-fluid">
      <ul class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dtcController/">Home</a></li>
        <li class="breadcrumb-item active">DTC Distribution       </li>
      </ul>
    </div>

    <section class="no-padding-top">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="block margin-bottom-sm">
              <!-- first row -->
              <div class="row sectionHeight">
                <div class="col-lg-3">
                  <label class="page_label col-lg-12 remove_padding">Main service meter <span class="colorRed">*</span>:</label>
                </div>
                <div class="col-lg-4 floatLeft">
                  <input type="number" class="col-lg-10 custom_form-control" name="from_hrMeter" id="from_hrMeter" placeholder="From">
                </div>
                <div class="col-lg-4 floatLeft">
                    <input type="number" name="to_hrMeter" class="col-lg-10 custom_form-control" placeholder="To" id="to_hrMeter">
                </div>
              </div>
              <!-- second row -->
              <div class="row sectionHeight">
                <div class="col-lg-3">
                  <label class="page_label col-lg-12 remove_padding">Class <span class="colorRed">*</span>:</label>
                </div>
                <div class="col-lg-4 floatLeft">
                  <select class="form-group class_select col-lg-10" id="ClassName">
                    <option value="0">Select class</option> 
                    <option value="classI">Class I</option> 
                    <option value="classII">Class II</option> 
                    <option value="classIII">Class III</option> 
                    <option value="classIV">Class IV</option> 
                    <option value="classV">Class V</option> 
                    <option value="bigTruck">Big Trucks</option> 
                  </select>
                </div>
                <div class="col-lg-5 floatLeft remove_padding">
                  <div class="col-lg-10 floatLeft" id = "uCode">
                    <label id="unitcodeLabel" class="col-lg-6 floatLeft">Unit code <span class="colorRed">*</span>: </label>
                    <select class="form-group col-lg-6 class_select" id="unitCodeSelect" multiple style="height: 160px;"> 
                    </select>
                  </div>
                  <div class="col-lg-10 floatLeft" id="tType">
                    <label>Subsystem<span class="colorRed">*</span> : </label>
                    <select class="form-group class_select col-lg-7" id="bigtruckType"></select>
                  </div>
                </div>
              </div>
              <!-- third row -->
              <div class="row sectionHeight">
                <div class="col-lg-3 remove_padding">
                  <label class="page_label col-lg-12">Fault type <span class="colorRed">*</span>:</label>
                </div>
                <div class="col-lg-9 floatLeft">
                  <div class="col-lg-12 remove_padding">
                    <input type="checkbox" name="critical" id="criticalFault" class="checkboxAlign"><label for="criticalFault">Critical faults</label>  
                  </div>
                  <div class="col-lg-12 remove_padding">
                    <input type="checkbox" name="logging" id="loggingFault" class="checkboxAlign"><label for="loggingFault">Logging faults</label>
                  </div>
                  <div class="col-lg-12 remove_padding">
                    <input type="checkbox" name="Shutdown" id="shutdownFault" class="checkboxAlign"><label for="shutdownFault">Shutdown faults</label>  
                  </div>
                </div>
              </div>
              <!-- fourth row -->
              <div class="row">
                <div class="col-lg-3">
                  <input type="checkbox" name="check_dateRange" id="check_dateRange" style="vertical-align:  middle;"><label for="check_dateRange">Select warranty end date</label>
                </div>
                <div class="col-lg-4 remove_padding divHeight" id="thirdRow">
                  <i class="fa fa-calendar floatLeft calender" aria-hidden="true"></i>
                  <input type="text" name="toDate" class="fromDate col-lg-10 custom_form-control floatLeft" id="toDate" autocomplete="off" placeholder="   To Date">
                </div>
              </div>
              <!-- fifth row -->
              <div class="row">
                <div class="col-lg-12">
                  <button type="button" name="set" class="btn pointerCursor floatLeft" value="Generate Chart" id="set" style="/*float: left;margin-right: 10px;*/"><i class="fa fa-bar-chart" aria-hidden="true"></i>   Generate chart</button>
                  <form action="<?php echo base_url() ?>dtcController/exportToExcel" method="post">
                    <button type="submit" name="export_excel" class="btn pointerCursor" value="Export as Excel" title="Export as Excel" id="export_excel" style="/*float: left;margin-right: 10px;*/"><i class="fa fa-download" aria-hidden="true"></i> Export</button>  
                  </form>
                  <button type="button" name="print" class="btn pointerCursor" value="Print" id="print" title="All graphs and description table" onclick="printDiv()" style="" style="/*float: left;margin-right: 10px;*/"><i class="fa fa-print" aria-hidden="true"></i> Print</button>
                </div>
              </div>
            </div>
            
          </div>
        </div>
      </div>
    </section>

    
    <div class="col-lg-12" id="loadimage" style="display: none;">
      <img src='<?php echo HTTP_IMAGES_PATH; ?>forkliftanimation.gif' style="width: 18%;position: fixed;top: 35%;left: 45%;">
      <!-- <label style="width: 18%;position: fixed;top: 63%;left: 50%;"><strong>Please Wait . . . </strong></label> -->
    </div> 

    <section class="no-padding-top">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12" id="div_dtcwise">
            <div class="block margin-bottom-sm">
              <!-- <div class="col-lg-12" id="div_dtcwise"> -->
                <div class="row">
                  <div class="col-lg-8 chartBorder remove_padding floatLeft" id="chartArea">
                    <div class="card-header d-flex align-items-center background" id="chartHeader">
                      <div class="col-lg-12" id="chartName"><strong></strong></div>
                    </div>
                    <div id="chartContainer" class="background col-lg-12"></div>
                    <div class="pagination background">
                      <div style="text-align: center">
                        <ul id="pageNo"> 
                        </ul>  
                      </div>
                    </div>  
                  </div>
                  <div id="div_NoData" class='col-lg-12'><label class='errorMsg'>No data available.</label></div>
                  <div class="col-lg-4 floatLeft remove_padding" id="faultDescription" style="background-color: #2d3035;padding-left: 5px;"></div>
                </div>
                <div id="print_table" class="displayNone"></div>
              <!-- </div> -->
            </div>
          </div>
        </div>
      </div>
    </section>
    <div id="print_table" class="displayNone"></div>

    <?php include "footer.php" ?>

  </div>
</div>
    
  </body>
</html>