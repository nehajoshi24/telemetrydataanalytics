  <?php include "header.php"; ?>

  <script src="<?php echo HTTP_JS_PATH; ?>scripts/truckUtilization.js"></script>
  
  <link href="<?php echo base_url(); ?>assets/datatables/css/dataTables.bootstrap.min.css?ver=1.1" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/datatables/css/jquery.dataTables.min.css?ver=1.1" rel="stylesheet">

  <script src="<?php echo base_url(); ?>assets/datatables/js/dataTables.bootstrap.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/datatables/js/jquery.dataTables.min.js"></script>

      
      <div class="page-content">
        <!-- Page Header-->
        <div class="page-header no-margin-bottom">
          <div class="container-fluid">
            <h2 class="h5 no-margin-bottom">Truck Utilization</h2>
          </div>
        </div>
        <!-- Breadcrumb-->
        <div class="container-fluid">
          <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>TruckUtilization/">Home</a></li>
            <li class="breadcrumb-item active">Tables            </li>
          </ul>
        </div>
        <section class="no-padding-top">
          <div class="container-fluid">
            <div class="row">
              <!-- Site table -->
              <div class="col-lg-12" id="siteTable">
                <div class="block margin-bottom-sm">
                  <div class="table-responsive"> 
                    <table id="table" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Action</th>
                          <th>Site Name</th>
                          <th>Dealer Name</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              
              <!-- Dept table -->
              <div class="col-lg-12" id="deptTable_div">
                <div class="block margin-bottom-sm">
                  <div class="table-responsive"> 
                    <table id="deptTable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th><center><input type='checkbox' name='depts[]' id='depts_all'><center></th>
                          <th>Department Name</th>
                          <th>Site Name</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

              <!-- Equipment table -->
              <div class="col-lg-12" id="equipTable_div">             
                <div class="block margin-bottom-sm">
                  <div class="table-responsive"> 
                    <div class="col-lg-12 remove_padding" style="height: 50px;margin-top: 2%;">
                      <!-- <div class="col-lg-2 remove_padding"> -->
                          <label class="page_label remove_padding">Select Year <span class="colorRed">*</span>:</label>
                      <!-- </div> -->
                      <select class="col-lg-2 class_select" id="select_year"> 
                        <option value="2013">2013</option>
                        <option value="2014">2014</option>
                        <option value="2015">2015</option>
                        <option value="2016">2016</option>
                        <option value="2017">2017</option>
                        <option value="2018" selected>2018</option>
                      </select>
                    </div>

                    <table id="equip_table" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th><center><input type='checkbox' name='equips[]' id='equipments_all'><center></th>
                          <th>Equipment Name</th>
                          <th>Product Id</th>
                          <th>Serial Number</th>
                          <th>Class Name</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>


              <!--  buttons div -->
              <div class="col-lg-12">
                <input type="button" name="reset_all" id="reset_all" value="Clear all selection">
                <!-- <form></form> -->
                <input type="button" name="selected_site_btn_back" id="selected_site_btn_back" value="Back">
                <input type="button" name="selected_dept_btn_back" id="selected_dept_btn_back" value="Back">
                <input type="button" name="selected_equip_btn_back" id="selected_equip_btn_back" value="Back">

                <input type="button" name="selected_site_btn" id="selected_site_btn" value="Next">
                <input type="button" name="selected_dept_btn" id="selected_dept_btn" value="Next">
                <input type="button" name="selected_equip_btn" id="selected_equip_btn" value="Next">
              </div>

              <div class="col-lg-12" id="plotArea">
                <div class="col-lg-12 remove_padding" id="yearly_avg">
                  <div class="col-lg-12 chartBorder remove_padding" id="yearly_avg_chart" style="margin-top: 0px;">
                    <div id="chartContainer1" class="background col-lg-12"></div>
                  </div>
                    <!-- <div class="col-lg-6" id="buttonArea_absolute"> -->
                    <input type="button" name="details" id="show_absolute_details" value="Show details">
                </div>

                <div class="remove_padding col-lg-12 floatLeft" id="utilization_absolute"> 
                  <div class="col-lg-12 chartBorder remove_padding background" id="chartArea" style="margin-top: 0px;">
                    <div class="card-header d-flex align-items-center background" id="chartHeader" style="border-color: white;">
                      <div class="col-lg-12" id="chartName"></div>
                    </div>
                    <div id="chartContainer" class="background col-lg-12"></div>
                    <!-- pagination div -->
                    <div class="pagination background" style="float:right;">
                      <div style="text-align: center">
                        <ul id="pageNo"> 
                        </ul>  
                      </div>
                    </div>  
                  </div>
                  <!-- <div class="col-lg-6" id="buttonArea_absolute"> -->
                    <input type="button" name="show_avg" id="show_avg" value="Show average">
                  <!-- </div> -->
                </div>
              </div>

              <div class="col-lg-12" id="loadimage" style="display: none;">
                <img src='<?php echo HTTP_IMAGES_PATH; ?>forkliftanimation.gif' style="width: 18%;position: fixed;top: 35%;left: 45%;">
                <!-- <label style="width: 18%;position: fixed;top: 63%;left: 50%;"><strong>Please Wait . . . </strong></label> -->
              </div>
            </div>
          </div>
        </section>
        <br />
        <?php include "footer.php" ?> 
      </div>
    </div>
    
  </body>
</html>