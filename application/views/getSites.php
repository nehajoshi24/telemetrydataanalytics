<?php include "header.php" ?>     
  <div class="page-content">
    <!-- Page Header-->
    <div class="page-header no-margin-bottom">
      <div class="container-fluid">
        <h2 class="h5 no-margin-bottom">Select Site</h2>
      </div>
    </div>
    <!-- Breadcrumb-->
    <div class="container-fluid">
      <ul class="breadcrumb">
        <li class="breadcrumb-item"><a href="">Home</a></li>
        <li class="breadcrumb-item active">Select Sites for Operator scorecard            </li>
      </ul>
    </div>
    <section class="no-padding-top">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="block margin-bottom-sm">
              <?php if(isset($errorMessage)){
                echo $errorMessage;
              }
              ?>
              <div class="title"><strong>Please select site/s</strong></div>
              <form action="<?php echo base_url(); ?>operatorSCardController/getOperators" method="POST">   
                <select  multiple="multiple" id="selectSites" name="selectSites[]" class="custom_form-control col-lg-4" style="background-color: #2d3035 !important;border-radius: 0px;height: 200px;"> <!-- id="demo" -->
                  <?php
                    if(isset($siteData)){ 
                      $siteData = json_decode($siteData);
                      foreach($siteData as $data){   ?>
                            <option style="color:'#000';font-size: 15px;" value="<?php echo $data->idTier5; ?>"><?php echo $data->tier5Name ?></option>
                          <?php  
                      }
                    }
                    else{ ?>
                      <span>No Data Avaliable</span>
                    <?php }
                  ?>
                </select>
                <br>
                <input type="submit" class="btn" name="submit" id="next" value="Next" style="margin-left: 0%;" />
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
    <?php include "footer.php" ?>  
  </div>
</div>
  <script type="text/javascript">
      var base_url = "<?php echo base_url(); ?>";
      jQuery(function () {
            jQuery("#demo").gs_multiselect();
            
         });
    </script>
  </body>
</html>