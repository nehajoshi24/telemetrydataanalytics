<!DOCTYPE html>
<html>
  <head> 
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Telemetry Data Analytics</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
    
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/font-awesome/css/font-awesome.min.css">
    <!-- Custom Font Icons CSS-->
    <link rel="stylesheet" href="<?php echo HTTP_CSS_PATH; ?>font.css">
    <!-- Google fonts - Muli-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Muli:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="<?php echo HTTP_CSS_PATH; ?>style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="<?php echo HTTP_CSS_PATH; ?>custom.css">
    <link rel="stylesheet" href="<?php echo HTTP_CSS_PATH; ?>styles/common.css">
    <!-- Favicon-->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/hyster_logo.png">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  </head>

  <body>
    <header class="header">   
      <nav class="navbar navbar-expand-lg">
        <div class="search-panel">
          <div class="search-inner d-flex align-items-center justify-content-center">
            <div class="close-btn">Close <i class="fa fa-close"></i></div>
            <form id="searchForm" action="#">
              <div class="form-group">
                <input type="search" name="search" placeholder="What are you searching for...">
                <button type="submit" class="submit">Search</button>
              </div>
            </form>
          </div>
        </div>
        <div class="container-fluid d-flex align-items-center justify-content-between">
          <div class="navbar-header">
            <img src="<?php echo base_url(); ?>assets/img/hyster_logo.png" width="10%" alt="HYG" class="logoImg"/>
            <!-- Navbar Header--><a href="#" class="navbar-brand">
              <div class="brand-text brand-big visible text-uppercase"><strong>Data </strong><strong class="text-primary"> Analytics</strong></div>
              <div class="brand-text brand-sm"><strong class="text-primary">D</strong><strong>A</strong></div></a>
            <!-- Sidebar Toggle Btn-->
            <button class="sidebar-toggle"><i class="fa fa-long-arrow-left"></i></button>


          </div>
          <div class="list-inline-item logout"><a id="logout" href="<?php echo base_url(); ?>Login/logout" class="nav-link">Logout <i class="fa fa-sign-out" aria-hidden="true"></i></a></div>
        </div>
      </nav>
    </header>
    <!-- <header class="header">   
        <nav class="navbar navbar-expand-lg">
            <div class="search-panel">
              <div class="search-inner d-flex align-items-center justify-content-center">
                <div class="close-btn">Close <i class="fa fa-close"></i></div>
                <form id="searchForm" action="#">
                  <div class="form-group">
                    <input type="search" name="search" placeholder="What are you searching for...">
                    <button type="submit" class="submit">Search</button>
                  </div>
                </form>
              </div>
            </div>
            <div class="container-fluid d-flex align-items-center justify-content-between">
              <div class="navbar-header">
                <img src="<?php echo base_url(); ?>assets/img/hyster_logo.png" width="10%" alt="HYG" class="logoImg"/>
               <a href="index.html" class="navbar-brand">
                  <div class="brand-text brand-big visible text-uppercase"><strong class="text-primary">Data</strong><strong>Analytics</strong></div>
                  <div class="brand-text brand-sm"><strong class="text-primary">D</strong><strong>A</strong></div></a>
             
                <button class="sidebar-toggle"><i class="fa fa-long-arrow-left"></i></button>
              </div>
              <div class="right-menu list-inline no-margin-bottom">    
              
                <div class="list-inline-item logout"><a id="logout" href="login.html" class="nav-link">Logout <i class="icon-logout"></i></a></div>
              </div>
            </div>
        </nav>
    </header> -->

    <div class="d-flex align-items-stretch">
        <nav id="sidebar" style="z-index: 1;">
        <!-- Sidebar Header-->
        <div class="sidebar-header d-flex align-items-center">
          <div class="avatar"><img src="<?php echo base_url(); ?>assets/img/user_icon.jpg" alt="..." class="img-fluid rounded-circle"></div>
          <div class="title">
            <h1 class="h5">Admin</h1>
            <p>Admin</p>
          </div>
        </div>
        <!-- Sidebar Navidation Menus--><span class="heading">Main</span>
        <ul class="list-unstyled">
                <!-- <li ><a href="index.html"> <i class="icon-home"></i>Dashboard </a></li> -->
                <li id="dtcLi"><a href="<?php echo base_url(); ?>dtcController/"> <i class="fa fa-line-chart"></i>DTC Analysis </a></li>
                <li id="tuLi"><a href="<?php echo base_url(); ?>TruckUtilization/"> <i class="fa fa-pie-chart"></i>Truck Utilization </a></li>
                <li id="oscLi"><a href="<?php echo base_url(); ?>operatorSCardController/"> <i class="fa fa-credit-card"></i>Operator Score Card </a></li>
                <li id="msLi"><a href="<?php echo base_url(); ?>MaintenanceSummary/"> <i class="fa fa-truck"></i>Maintenance Summary </a></li>

                <!-- <li><a href="#exampledropdownDropdown" aria-expanded="false" data-toggle="collapse"> <i class="icon-windows"></i>Maintenance Summary </a>
                  <ul id="exampledropdownDropdown" class="collapse list-unstyled ">
                    <li><a href="#">Page</a></li>
                    <li><a href="#">Page</a></li>
                    <li><a href="#">Page</a></li>
                  </ul>
                </li>
                <li><a href="login.html"> <i class="icon-logout"></i>Login page </a></li> -->
        <!-- </ul><span class="heading">Extras</span>
        <ul class="list-unstyled">
          <li> <a href="#"> <i class="fa fa-truck"></i>Demo </a></li>
          <li> <a href="#"> <i class="icon-writing-whiteboard"></i>Demo </a></li>
          <li> <a href="#"> <i class="icon-chart"></i>Demo </a></li>
        </ul> -->
        </nav>
        
        <!-- <div class="col-lg-12" id="loadimage">
          <center><img src='<?php echo base_url(); ?>assets/img/InternetSlowdown_Day.gif' class="width10 temp"></center>
         
        </div> --> 
<!-- <img src='<?php echo base_url(); ?>assets/img/forkliftanimation.gif' class="width10 temp" style="top: 30%;width: 10%;left: 40%;position: absolute;"> -->
        <script type="text/javascript">
            var activeTab = "<?php echo $this->uri->segment(1); ?>";

            if(activeTab == 'Home'){
                $("#homeLi").addClass("active");
                $("#homeIcon").addClass("ActiveIcon");
                $("#homeLabel").addClass("ActiveLabel");
            }

            if(activeTab == 'dtcController'){
                $("#dtcLi").addClass("active");
                $("#dtcIcon").addClass("ActiveIcon");
                $("#dtcLabel").addClass("ActiveLabel");
            }

            if(activeTab == "operatorSCardController"){
                $("#oscLi").addClass("active");
                $("#oscIcon").addClass("ActiveIcon");
                $("#oscLabel").addClass("ActiveLabel");
            }

            if(activeTab == "TruckUtilization"){
                $("#tuLi").addClass("active");
                $("#tuIcon").addClass("ActiveIcon");
                $("#tuLabel").addClass("ActiveLabel");
            }

            if(activeTab == "MaintenanceSummary"){
                $("#msLi").addClass("active");
                $("#msIcon").addClass("ActiveIcon");
                $("#msLabel").addClass("ActiveLabel");
            }

            $(".left_border").css('height',$( document ).height(),'!important');
        </script>
      <input type="hidden" id="hdnBaseUrl" value="<?php echo base_url(); ?>"/>

      <!-- JavaScript files-->

    <script src="<?php echo base_url(); ?>assets/vendor/jquery/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.1.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/popper.js/umd/popper.min.js"> </script>
    <script src="<?php echo base_url(); ?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="<?php echo base_url(); ?>assets/vendor/chart.js/Chart.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/jquery-validation/jquery.validate.min.js"></script>
    <script src="<?php echo HTTP_JS_PATH; ?>front.js"></script>
    <script src="<?php echo HTTP_JS_PATH; ?>jquery-ui.min.js"></script>

    
    <script src="<?php echo HTTP_JS_PATH; ?>highcharts.js"></script> <!-- for huighcharts -->
    <script src="<?php echo HTTP_JS_PATH; ?>exporting.js"></script> <!-- for export functionality -->
    <script src="<?php echo HTTP_JS_PATH; ?>export-data.js"></script>
    <script src="<?php echo HTTP_JS_PATH; ?>jquery.multiselect.js"></script>
    <!-- <link rel="stylesheet" href="<?php echo HTTP_CSS_PATH; ?>jquery-ui.css"> -->
    <link rel="stylesheet" href="<?php echo HTTP_CSS_PATH; ?>jquery-ui.min.css">
  </body>

