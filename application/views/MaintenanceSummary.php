<?php include "header.php"; ?>
<script src="<?php echo base_url(); ?>assets/js/scripts/maintenanceSummary.js"></script>  
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCtqZBXtprS-X4-WoVGkd5wtVhJISMlHus"></script>
      <div class="page-content">
        <div class="page-header">
          <div class="container-fluid">
            <h2 class="h5 no-margin-bottom">Maintenance Summary</h2>
          </div>
        </div>
        <!-- Breadcrumb-->
        <div class="container-fluid">
          <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>MaintenanceSummary/">Home</a></li>
            <li class="breadcrumb-item active">Maintenance Summary         </li>
          </ul>
        </div>

        <section class="no-padding-top">
          <div class="container-fluid">
            <div class="row">
              <div class="col-lg-12">
                <div class="block margin-bottom-sm">
                  <div class="row sectionHeight">
                    <div class="col-lg-4 col-md-8 col-sm-10 col-xs-12" id="div_MSite" style="padding-left: 2px;float: left;">
                      <label>Site<span class="colorRed">*</span></label>:
                      <select class="form-group width75 class_select" id="MSite" style="width: 100%;"></select>
                    </div>

                    <div class="col-lg-2 col-md-4" id="div_MSite" style="padding: 0px;float: left;">
                      <label class="remove_padding">Class<span class="colorRed">*</span></label>:
                      <select class="form-group class_select" id="Maintaince_className" style="width: 95%;">
                        <option value="0">--Select class--</option> 
                        <option value="classI">Class I</option> 
                        <option value="classII">Class II</option> 
                        <option value="classIII">Class III</option> 
                        <option value="classIV">Class IV</option> 
                        <option value="classV">Class V</option> 
                        <option value="bigTruck">Big Trucks</option> 
                      </select>
                    </div>

                    <div class="col-lg-3 col-md-4" id="div_MEquipment" style="padding: 0px;float: left;">
                      <label>Equipment<span class="colorRed">*</span></label>:
                      <select class="form-group class_select" id="MEquipment" style="width: 95%;"></select>
                    </div>

                    <div class="col-lg-2" style="float: left;padding: 0px">
                      <input type="button" name="set" class="btn pointerCursor" value="Reload" id="maintenanceReload">
                    </div>
                  </div>
                  
                </div>
              </div>
            </div>
          </div>
        </section>

        <!-- container data -->
        <section class="no-padding-top"  id="countContainer" style="margin-bottom: -4%;">
          <div class="container-fluid">
            <div class="row">
              <div class="col-lg-12">
                <div class="margin-bottom-sm">
                  <div class="row" style="padding: 20px;">
                    <!-- first row left side -->
                    <div class="col-lg-6 block" style="border:5px solid #151515 !important; ">
                      <div class="title"><center><strong class="d-block">Fault code count</strong></center></div>
                      <div class="col-lg-12 col-md-12">
                        <!-- fault code 1st circle -->
                        <div class="col-lg-6 floatLeft">
                          <div class="stats-with-chart-2 block">
                            <div class="text" id="falutcode_month"><strong class="d-block"></strong><span class="d-block"></span></div>
                            <canvas id="falutcode_month_chart"></canvas>
                            <div class="" style="position: absolute;top: 90%;left: 50%;-webkit-transform: translate(-50%, -50%);transform: translate(-50%, -50%);display: inline-block;text-align: center;">Till now</div>
                          </div>
                        </div>
                        <!-- fault code 2nds circle -->
                        <div class="col-lg-6 floatLeft">
                          <div class="stats-with-chart-2 block">
                            <div class="text" id="falutcode_maintenance"><strong class="d-block"></strong><span class="d-block"></span></div>
                            <canvas id="falutcode_maintenance_chart"></canvas>
                            <div class="" style="position: absolute;top: 90%;left: 50%;-webkit-transform: translate(-50%, -50%);transform: translate(-50%, -50%);display: inline-block;text-align: center;">Since last maintenance</div>
                          </div>
                        </div>
                      </div>
                    </div> <!--  end -->

                    <div class="col-lg-6 block" style="border:5px solid #151515 !important; ">
                      <div class="title"><center><strong class="d-block">Checklist failure count</strong></center></div>
                      <div class="col-lg-12 col-md-12">
                        
                        <div class="col-lg-6 floatLeft">
                          <div class="stats-with-chart-2 block">
                            <div class="text" id="checklist_month"><strong class="d-block"></strong><span class="d-block"></span></div>
                            <canvas id="checklist_month_chart"></canvas>
                            <div class="" style="position: absolute;top: 90%;left: 50%;-webkit-transform: translate(-50%, -50%);transform: translate(-50%, -50%);display: inline-block;text-align: center;">Till now</div>
                          </div>
                        </div>
                        
                        <div class="col-lg-6 floatLeft">
                          <div class="stats-with-chart-2 block">
                            <div class="text" id="checklist_mentenance"><strong class="d-block"></strong><span class="d-block"></span></div>
                            <canvas id="checklist_mentenance_chart"></canvas>
                            <div class="" style="position: absolute;top: 90%;left: 50%;-webkit-transform: translate(-50%, -50%);transform: translate(-50%, -50%);display: inline-block;text-align: center;">Since last maintenance</div>
                          </div>
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        <section class="no-padding-top"  id="countContainer1">
          <div class="container-fluid">
            <div class="row">
              <div class="col-lg-12">
                <div class="margin-bottom-sm">
                  <div class="row" style="padding: 20px;">
                    <div class="col-lg-6 block" style="border:5px solid #151515 !important; ">
                      <div class="title"><center><strong class="d-block">Impact count</strong></center></div>
                      <div class="col-lg-12 col-md-12">
                      
                        <div class="col-lg-6 floatLeft">
                          <div class="stats-with-chart-2 block">
                            <div class="text" id="impact_month"><strong class="d-block"></strong><span class="d-block"></span></div>
                            <canvas id="impact_month_chart"></canvas>
                            <div class="" style="position: absolute;top: 90%;left: 50%;-webkit-transform: translate(-50%, -50%);transform: translate(-50%, -50%);display: inline-block;text-align: center;">Till now</div>
                          </div>
                        </div>
                      
                        <div class="col-lg-6 floatLeft">
                          <div class="stats-with-chart-2 block">
                            <div class="text" id="impact_mainetenance"><strong class="d-block"></strong><span class="d-block"></span></div>
                            <canvas id="impact_mainetenance_chart"></canvas>
                            <div class="" style="position: absolute;top: 90%;left: 50%;-webkit-transform: translate(-50%, -50%);transform: translate(-50%, -50%);display: inline-block;text-align: center;">Since last maintenance</div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6 block" style="border:5px solid #151515 !important; ">
                      <div class="title"><center><strong class="d-block">Main service meter (hours achieved)</strong></center></div>
                      <div class="col-lg-12 col-md-12">
                 
                        <div class="col-lg-6 floatLeft">
                          <div class="stats-with-chart-2 block">
                            <div class="text" id="mainServicemeter"><strong class="d-block"></strong><span class="d-block"></span></div>
                            <canvas id="mainServicemeter_total_chart"></canvas>
                            <div class="" style="position: absolute;top: 90%;left: 50%;-webkit-transform: translate(-50%, -50%);transform: translate(-50%, -50%);display: inline-block;text-align: center;">Till now</div>
                          </div>
                        </div>
                  
                        <div class="col-lg-6 floatLeft">
                          <div class="stats-with-chart-2 block">
                            <div class="text" id="mainServicemeter_maintenance"><strong class="d-block"></strong><span class="d-block"></span></div>
                            <canvas id="mainServicemeter_maintenance_chart"></canvas>
                            <div class="" style="position: absolute;top: 90%;left: 50%;-webkit-transform: translate(-50%, -50%);transform: translate(-50%, -50%);display: inline-block;text-align: center;">Since last maintenance</div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        <div class="col-lg-12" id="loadimage" style="display: none;">
          <img src='<?php echo HTTP_IMAGES_PATH; ?>forkliftanimation.gif' style="width: 18%;position: fixed;top: 35%;left: 45%;">
          <!-- <label style="width: 18%;position: fixed;top: 63%;left: 50%;"><strong>Please Wait . . . </strong></label> -->
        </div> 
        <section class="no-padding-top"  id="countContainer1">
          <div class="container-fluid">
            <div class="row">
              <div class="col-lg-12">
                <div class="margin-bottom-sm">
                  <div class="row">
                    <div id="map" class="mapStyle" ></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        <div class="col-lg-12" style="float: left;padding: 15px;">
          
        </div> 

        
        <?php include "footer.php"; ?>
        
      </div>
    </div>
    
  </body>
</html>