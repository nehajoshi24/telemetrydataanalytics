<footer class="footer">
	<div class="footer__block block no-margin-bottom">
		<div class="container-fluid text-center">
		  <!-- Please do not remove the backlink to us unless you support us at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
		  <p class="no-margin-bottom">2018 &copy; Hyster-Yale Group. All Rights Reserved.  Design by <a href="https://www.hyster-yale.com/Home/default.aspx">Hyster-yale India</a>.</p>
		</div>
	</div>
</footer>

<!-- <script src="<?php echo base_url(); ?>assets/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendor/popper.js/umd/popper.min.js"> </script>
<script src="<?php echo base_url(); ?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendor/jquery.cookie/jquery.cookie.js"> </script>
<script src="<?php echo base_url(); ?>assets/vendor/chart.js/Chart.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendor/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo HTTP_JS_PATH; ?>charts-home.js"></script>
<script src="<?php echo HTTP_JS_PATH; ?>front.js"></script> -->