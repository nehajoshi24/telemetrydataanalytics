<?php include "header.php"; ?>
<script src="<?php echo base_url(); ?>assets/js/scripts/operatorSCard.js"></script>  
<link rel="stylesheet" href="<?php echo HTTP_CSS_PATH; ?>styles/osc.css">
<div class="page-content">
	<!-- Page Header-->
	<div class="page-header no-margin-bottom">
	  <div class="container-fluid">
	    <h2 class="h5 no-margin-bottom">Operator Scorecard</h2>
	  </div>
	</div>
	<!-- Breadcrumb-->
	<div class="container-fluid">
	  <ul class="breadcrumb">
	    <li class="breadcrumb-item"><a href="">Home</a></li>
	    <li class="breadcrumb-item active">Operator scorecard            </li>
	  </ul>
	</div>

	<section class="no-padding-top" id="selectOp">
	  	<div class="container-fluid">
	    	<div class="row">
	    		<div class="col-lg-12">
	    			<div class="block">
		    			<div class="block-body">
		    				<?php
								if(isset($errorMessage)){
									echo $errorMessage;
								} 
							?>
							<span id="errorMsgOperator" class="errorMsg"></span>
					        <form action="<?php echo base_url();?>operatorSCardController/" method="POST">
					        	<div class="col-lg-12"><input type="submit" class="btn" name="submit" id="next" value="Back" style="margin-left: 0%;" /></div>
					        </form>
					        
					        <div class="col-lg-12 remove_padding" id="firstRow" style="height: 50px;margin-bottom: 20px;">
					            <div class="col-lg-3 remove_padding floatLeft">
					              <label class="page_label col-lg-12">Select date duration <span class="colorRed">*</span>:</label>
					            </div>
					            <div class="col-lg-3 remove_padding" style="float: left;">
					              <select class="regular_select col-sm-10 custom_form-control" name="select_dateRange"  id="select_dateRange" style="border-radius: 0px;background-color: #2d3035;">
					                  <option value="custom"><span>Custom</span></option>
					                  <option value="weekly" selected><span>Last Week</span></option>
					                  <option value="monthly"><span>Last Month</span></option>
					                  <option value="yearly"><span>Last Year</span></option>
					                </select>  
					            </div>
					            <div class="col-lg-6 remove_padding" style="float: left;">
					              <div class="col-lg-12 remove_padding" id = "custom_dateRange" style="float: left;margin-top: 0px !important;">
					                <div class="col-lg-5 floatLeft">
					                  <i class="fa fa-calendar floatLeft calender" aria-hidden="true"></i>
					                  <input type="text" name="fromDate" class="fromDate custom_form-control" id="fromDate" placeholder="   From Date">
					                </div>

					                <div class="col-lg-5 floatLeft">
					                  <i class="fa fa-calendar floatLeft calender" aria-hidden="true"></i>
					                  <input type="text" name="fromDate" class="fromDate custom_form-control floatLeft" id="toDate" placeholder="   To Date">
					                </div>
					                
					              </div>
					            </div>
				          	</div>

				          	<div class="col-lg-12 remove_padding" id="firstRow" style="height: 50px;">
					            <div class="col-lg-3 remove_padding floatLeft">
					              <label class="page_label col-lg-12">Select operators <span class="colorRed">*</span>:</label>
					            </div>
					            <div class="col-lg-3 remove_padding" style="float: left;">
				              		<select class="multiselect col-sm-10" multiple id="operatorIds" name="operatorIds" style="color:#fff;border: 0px !important;background-color: #2d3035;border-bottom: 1px solid !important;">
				                  	<?php
						                if(isset($operators)){ 
						                  $operators = json_decode($operators);
						                  foreach($operators as $data){   ?>
						                    <option style="color:'#000';font-size: 15px;" value="<?php echo $data->idOperator; ?>"><?php echo $data->firstName; echo " "; echo $data->lastName; ?></option>
						                    <?php
						                  }
						                }
						              ?>
					                </select>  
					                <div class="col-lg-12">
					                	<input type="submit" class="btn" name="submit" id="next_selectOp" value="Next" style="margin-left: 0%;" />
				             	 	</div>
					            </div>
				          	</div>
				          	<br><br><br><br><br>
				      	</div>
				  	</div>
	    		</div>
	    	</div>
		</div>
	</section>

	<section class="no-padding-top" id="menuScreen" style="display: none;">
  		<div class="container-fluid">
		    <div class="row" id="check_parameters">
				<div class="col-lg-3">
			        <div class="block">
		          		<div class="title"><strong class="d-block">Productivity </strong></div>
			          	<div class="block-body">
			              <div class="form-group">
			                <input type="checkbox" name="idle_time" id="idle_time" value="1" checked><label for="idle_time">Idle time</label><br>
			                <input type="checkbox" name="working_time" id="working_time" value="1" checked><label for="working_time">Working time</label><br>
			                <input type="checkbox" name="hydraulic_time" id="hydraulic_time" value="1" checked><label for="hydraulic_time">Hydraulic time</label><br>
			                <input type="checkbox" name="distance_travel" id="distance_travel" value="1" checked><label for="distance_travel">Distance traveled</label><br>
			                <input type="checkbox" name="no_of_loads" id="no_of_loads" value="1" checked><label for="no_of_loads">No of loads</label><br>
			              </div>
			          </div>
			        </div>
		      	</div>

		      	<div class="col-lg-3">
			        <div class="block">
			          	<div class="title"><strong class="d-block">Health/Safety </strong></div>
			          	<div class="block-body">
			            
			              <div class="form-group">
			                <input type="checkbox" name="overspeeds" id="overspeeds" checked><label for="overspeeds">Overspeeds</label><br>
			                <input type="checkbox" name="impacts" id="impacts" checked><label for="impacts">Impacts</label><br>
			                <input type="checkbox" name="seatbelt_violation" id="seatbelt_violation" checked><label for="seatbelt_violation">Seatbelts violation</label><br>
			                <input type="checkbox" name="parkbrake_violation" id="parkbrake_violation" checked><label for="parkbrake_violation">Park brake violations</label>
			              </div>
			              <br>
			          </div>
			        </div>
		      	</div>

		      	<!-- Energy Div -->
		      	<!-- <div class="col-lg-3">
			        <div class="block">
			          	<div class="title"><strong class="d-block">Energy/Economy </strong></div>
			          	<div class="block-body">
			              <div class="form-group">
			                <input type="checkbox" name="fuel_utilization"><label for="fuel_utilization">Fuel utilization</label><br>
	            			<input type="checkbox" name="load_efficiency"><label for="load_efficiency">Load efficiency</label><br>
			              </div>
			          </div>
			        </div>
		      	</div> -->
		
	      		<div class="col-md-12">
					<input type="button" class="btn pointerCursor" name="generateOperatorSCard" id="generateOperatorSCard" value="Generate scorecard" style="margin-left: 0%;">
				</div>
		    </div>
		    <label class="fontfamilyLabel"><b>Date range : </b><span id="spn_opSCardDuration"></span></label>
	  	</div>
	</section>
	<div class="col-lg-12" id="loadimage" style="display: none;">
		<img src='<?php echo HTTP_IMAGES_PATH; ?>forkliftanimation.gif' style="width: 18%;position: fixed;top: 35%;left: 45%;">
		
	</div>
	<section class="charts no-padding-top" id="displayArea">
  		<div class="container-fluid">
		    <div class="row" style="padding-bottom: 3em;">
				<div class="col-lg-12 bar-chart-example card blackBackground" id="productivity" style="float: left;margin-bottom: 1%;">
              		<div class="col-lg-12" style="margin-bottom: 30px !important;">
              			<div class="col-lg-12" id="scorecard_container" style="float: left;">
							<div class="col-lg-12 chartBorder" id="scorecard_chart"></div>
							
							<div class="col-lg-9" style="/*height: 50px;*/margin-top: 10px;float: left;padding: 0px;">
							
							  <input type="button" class="btn pointerCursor" name="show_productivity" value="Productivity details" id="show_productivity" />
							
							  <input type="button" class="btn pointerCursor" name="show_safety" value="Health/Safety details" id="show_safety" />
							
							</div>
						</div>
          			</div>
              	</div>
		    	
		    	<div class="col-lg-12" id="productivity_details">
					<div class="card-header d-flex align-items-center blackBackground">
						<h3 class="h4" id="chartName"><label class="FontFamilyHeader">Parameter wise score for operator/s</label></h3>&nbsp;&nbsp;  
					</div>
					<div class="col-lg-12" style="margin-top: 1%;">
						<span><button id="show_card1" class=" btn pointerCursor">Back</button></span>
						<span><button id="show_productivity_details" class=" btn pointerCursor">Productivity data</button></span>
					</div><br />
					<div class="col-lg-12 table-responsive" id="container" style="float: left;width : 100%;"></div>
				</div>

				<div class="col-lg-12" id="safety_details">
                  	<div class="card-header d-flex align-items-center blackBackground">
                    	<h3 class="h4" id="chartName"><label class="FontFamilyHeader">Parameter wise score for operator/s</label></h3>&nbsp;&nbsp;  
                  	</div>
                  	<div class="col-lg-12" style="margin-top: 1%;">
                    	<span><button id="show_card2" class=" btn pointerCursor">Back</button></span>
                        <span><button id="show_safety_details" class=" btn pointerCursor">Safety data</button></span>
                  	</div><br />
                  	<div class="col-lg-12 table-responsive" id="safety_container" style="float: left;"></div>
                </div>

                <div class="col-lg-12" id="individual_productivity_details" style="margin-top: 2%;margin-bottom: 2%; ">
					<div class="card-header d-flex align-items-center blackBackground">
						<h3 class="h4" id="chartName"><label class="FontFamilyHeader">Actual data for operator/s</label></h3>&nbsp;&nbsp;  
					</div>
					<div class="col-lg-12" style="margin-top: 1%;">
						<span><button id="backto_productivity" class=" btn pointerCursor">Back</button></span> 
						<span><button id="backto_scorecard" class=" btn pointerCursor">Go to scorecard </button></span>
					</div><br />

					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="menu_tabs" style="padding: 0px;height: 40px;">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 pointerCursor tabStyle" id="timeGraph"> 
						  <label class="colorWhite pointerCursor">Hours of operations</label>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 pointerCursor tabStyle" id="loadsGraph"> 
						  <label class="colorWhite pointerCursor">Number of loads</label>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 pointerCursor tabStyle" id="distanceGraph"> 
						  <label class="colorWhite pointerCursor">Distance travelled</label>
						</div>
					</div>
					<br />

					<div class="col-lg-12 chartBorder" id="individual_productivity" style="float: left;background-color: #2d3035;margin-top: 2%;margin-bottom: 2%;">
					</div>
					<div class="col-lg-12 chartBorder" id="individual_productivity2" style="float: left;background-color: #2d3035;margin-top: 2%;margin-bottom: 2%;">
					</div>
					<div class="col-lg-12 chartBorder" id="individual_productivity3" style="float: left;background-color: #2d3035;margin-top: 2%;margin-bottom: 2%;">
					</div>
					</div>

					<div class="col-lg-12" id="individual_safety_details" >
						<div class="card-header d-flex align-items-center blackBackground">
							<h3 class="h4" id="chartName"><label class="FontFamilyHeader">Actual data for operator/s</label></h3>&nbsp;&nbsp;  
						</div>
						<div class="col-lg-12" style="margin-top: 1%;">
							<span><button id="backto_safety" class=" btn pointerCursor">Back</button></span>
							<span><button id="backto_scorecard1" class=" btn pointerCursor">Go to scorecard</button></span>
						</div>
						<div class="col-lg-12 chartBorder" id="individual_safety" style="float: left;background-color: #2d3035;"></div>
					</div>
				</div>

	  		</div>
	  	</div>
	</section>
	<?php include "footer.php"; ?>
</div> 
                