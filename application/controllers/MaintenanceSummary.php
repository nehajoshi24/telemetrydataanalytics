<?php
class MaintenanceSummary extends CI_Controller 
{
	function __construct() {
        parent::__construct();
        $this->load->model('MaintenanceSummaryModel');
    }

	public function index(){
		if($this->session->userdata('logged_in') == 1)
		{
			$siteData = $this->MaintenanceSummaryModel->getSites();
			$data['siteData'] = $siteData;
			$data['errorMessage'] = "";
			$this->load->view('MaintenanceSummary.php');
		}
		else{
			$data['message'] = "Please Login";
			$this->load->view('login.php',$data);
		}
	}


	//get sites for user
	public function getSites()
	{
		//pass userid later

		//Call to model
		$siteData = $this->MaintenanceSummaryModel->getSites();

		//return output
		echo $siteData;
		return $siteData;
	}

	//get equipments of selected site and class 
	public function getEquipmentsForMaintaince()
	{
		//read inputs
		$siteId =  $this->input->post('siteId');
		$className =  $this->input->post('className');

		//Call to model
		$equipmentsData = $this->MaintenanceSummaryModel->getEquipmentsForMaintaince($siteId, $className);

		//return output
		echo $equipmentsData;
		return $equipmentsData;
	}

	//Get truck maintaince details for equipments from selected sites
	public function getTruckMaintenanceDetails()
	{
		//read inputs
		$equipmentId =  $this->input->post('equipmentId');
		$className =  $this->input->post('className');
		$fromDate = date('Y-m-d', strtotime('-7 days'));
		$toDate = date("Y-m-d");

		//Call to model
		$maintenanceData = $this->MaintenanceSummaryModel->getTruckMaintenanceDetails($equipmentId, $className, $fromDate, $toDate);

		//return output
		echo $maintenanceData;
		return $maintenanceData;
	}
}

?> 