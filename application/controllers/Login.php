<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('session');
        /*$this->load->model('LoginModel');*/
    }
    
    public function index() {
    	$data['message'] = "";
    	$this->load->view('login.php',$data);
    }

    public function checkLogin(){
    	$username = $this->input->post('username');
    	$password = $this->input->post('password');

        $action = $this->input->post('action');

    	if($username == 'admin' && $password == 'admin'){
    		$userData = array( 
			   'username'  => 'admin',  
			   'logged_in' => TRUE,
               'targetType' => '',
               'search_string' => ''
			);  
			$this->session->set_userdata($userData);  
    		
    		if($this->session->userdata('logged_in') == 1){
                echo "success : ".$action;
                if($action == "" || $action == "Login"){
                    redirect(base_url().'dtcController');    
                }
                else{
                    redirect(base_url().$action);
                }
    		}
			else{
	    		$data['message'] = "*Session Expired.Please Login again.";
	    		$this->load->view('login.php',$data);
	    	}
    	}
    	else{
    		$data['message'] = "*Please enter valid username and password";
    		$this->load->view('login.php',$data);
    	}
    }

    public function logout(){
    	$this->session->unset_userdata('username');
    	$this->session->unset_userdata('logged_in');
        $this->session->sess_destroy();
    	$data['message'] = "";
    	$this->load->view('login.php',$data);
    }

}


