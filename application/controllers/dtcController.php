<?php

class dtcController extends CI_Controller 
{
	public function __construct() {
        //Call to model
        parent::__construct();
		$this->load->model('dtcModel');
    }

    public function index(){
    	
		if($this->session->userdata('logged_in') == 1){
			$this->load->view('dtcAnalysis.php');
		}
		else{
			$data['message'] = "Please Login";
			$this->load->view('login.php',$data);
		}	
	}

	/* To get from date and to date according to method this function is written */
	function getDates($method){
		if($method == 0){
			$fromDate = "";
			$toDate = "";	
		}
		if($method == 'custom'){
			$fromDate = $this->input->post('fromDate');
			$toDate = $this->input->post('toDate');	
			
			$fromDate =  date('Y-m-d', strtotime($fromDate));
			$toDate =  date('Y-m-d', strtotime($toDate));
		}
		if($method == "weekly"){
			$fromDate = date('Y-m-d', strtotime('-7 days'));
			$toDate = date("Y-m-d");
		}

		if($method == 'monthly'){
			$fromDate = date('Y-m-d', strtotime('-30 days'));
			$toDate = date("Y-m-d");
		}
		if($method == 'yearly'){
			$fromDate = date('Y-m-d', strtotime('-365 days'));
			$toDate = date("Y-m-d");
		}

		$data['fromDate'] = $fromDate;
		$data['toDate'] = $toDate;
		return $data;
	}

	///Get unit codes data for the selected duration (for classII and classIII)
	public function GetUnitCodes()
	{
		//read inputs
		$className =  $this->input->post('className');
		
		//Call GetUnitCodes function from Model
		$unitCodesdata = $this->dtcModel->GetUnitCodes($className);

		//return output to view
		echo $unitCodesdata;	
	}

	///Get DTC data for classII and classIII where unit codes are considered
	public function getUnitCodeDTCData()
	{
		//read inputs
		$className_unitCode =  $this->input->post('className_unitCode');
		$idUnitCode = $this->input->post('idUnitCode');

		$toDate = $this->input->post('toDate');	

		$from_hrMeter = $this->input->post('from_hrMeter');
		$to_hrMeter = $this->input->post('to_hrMeter');
		$fault_flags = $this->input->post('faults');

		$data = $this->dtcModel->getUnitCodeDTCData($className_unitCode, $idUnitCode,/*$fromDate,*/ $toDate,$from_hrMeter,$to_hrMeter,$fault_flags);	

		return $data;
	}

	//Get DTC data for big trucks as per subsystem
	public function GetBigTruckDTCData()
	{
		//read inputs
		$className = $this->input->post('className');
		$btype =  $this->input->post('btype');
			
		$from_hrMeter=$this->input->post('from_hrMeter');
		$to_hrMeter = $this->input->post('to_hrMeter');

		$fault_flags = $this->input->post('fault_flags');

		$toDate = $this->input->post('toDate');		

		//Call GetBigTruckDTCData function from Model
		$btDTCdata = $this->dtcModel->GetBigTruckDTCData($className, $btype, $toDate,$from_hrMeter,$to_hrMeter,$fault_flags);
		
		//return output to view
		echo $btDTCdata;		
	}

	public function exportToExcel(){
		$expotingData = $this->session->userdata('exportData');

		require_once APPPATH . '/third_party/Phpexcel/Bootstrap.php';

		$spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();

		// add style to the header
		$styleArray = array(
			'font' => array(
					'bold' => true,
			),
			'alignment' => array(
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			),
			'borders' => array(
					'top' => array(
							'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					),
			),
			'fill' => array(
					'type' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
					'rotation' => 90,
					'startcolor' => array(
							'argb' => 'FFA0A0A0',
					),
					'endcolor' => array(
							'argb' => 'FFFFFFFF',
					),
			),
		);

		$spreadsheet->getActiveSheet()->getStyle('A1:C1')->applyFromArray($styleArray);

		// auto fit column to content
		foreach(range('A','C') as $columnID) {
			$spreadsheet->getActiveSheet()->getColumnDimension($columnID)
					->setAutoSize(true);
		}

		// set the names of header cells
		$spreadsheet->setActiveSheetIndex(0)
				->setCellValue("A1",'Error Code')
				->setCellValue("B1",'Description')
				->setCellValue("C1",'Error Count');
		
		// Add some data
		$x= 2;

		for($i=0;$i<sizeof($expotingData);$i++){
			$spreadsheet->setActiveSheetIndex(0)
					->setCellValue("A$x",$expotingData[$i]['errorCode'])
					->setCellValue("B$x",$expotingData[$i]['errorDescription'])
					->setCellValue("C$x",$expotingData[$i]['errorCount']);
			$x++;
		}
		
		// Rename worksheet
		$spreadsheet->getActiveSheet()->setTitle('DTC Distribution');

		// set right to left direction
		//$spreadsheet->getActiveSheet()->setRightToLeft(true);

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$spreadsheet->setActiveSheetIndex(0);

		/*var_dump($spreadsheet->setActiveSheetIndex(0));
		exit;*/
		// Redirect output to a client’s web browser (Excel2007)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="DTC_distribution.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Excel2007');
		$writer->save('php://output');
		
		return;
	} //End of function
}