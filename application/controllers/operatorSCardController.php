<?php
class operatorSCardController extends CI_Controller 
{
	public function __construct() {
        parent::__construct();
        //Call to model
		$this->load->model('operatorSCardModel');
		//$this->load->model('TruckUtilizationModel');
    }

	public function index(){
		if($this->session->userdata('logged_in') == 1){
			$siteData = $this->operatorSCardModel->getSites();
			$data['siteData'] = $siteData;
			$this->load->view('getSites.php',$data);
		}
		else{
			$data['message'] = "Please Login";
			$this->load->view('login.php',$data);
		}
	}

	//Get operators for selected sites (operator name and id)
	public function getOperators()
	{
		$siteIds = $this->input->post('selectSites');
		
		if(isset($siteIds))
		{
			$data['operators'] = $this->operatorSCardModel->getOperators($siteIds);
			$this->load->view('operatorSCard.php',$data);
		}
		else{
			$siteData = $this->operatorSCardModel->getSites();
			$data['siteData'] = $siteData;
			$data['errorMessage'] = "<label style='color : red'>* Please select sites/s.</label>";
			$this->load->view('getSites.php',$data);
		}
	}

	//Get operator scorecard details
	public function getOperatorScorecardData()
	{
		//read inputs
		$operatorIdsList =  $this->input->post('operatorIdsList');
		$durationUnit =  $this->input->post('durationUnit');
		$fromDate = $this->input->post('fromDate');
		$toDate = $this->input->post('toDate');

		if($durationUnit == "custom"){
			$fromDate = date('Y-m-d',strtotime($this->input->post('fromDate')));
			$toDate = date('Y-m-d',strtotime($this->input->post('toDate')));
		}
		if($durationUnit == "weekly"){
			$fromDate = date('Y-m-d', strtotime('-7 days'));
			$toDate = date("Y-m-d");
		}
		if($durationUnit == 'monthly'){
			$fromDate = date('Y-m-d', strtotime('-30 days'));
			$toDate = date("Y-m-d");
		}
		if($durationUnit == 'yearly'){
			$fromDate = date('Y-m-d', strtotime('-365 days'));
			$toDate = date("Y-m-d");
		}

		$opSCardData = $this->operatorSCardModel->getOperatorScorecardData($operatorIdsList, $fromDate, $toDate);
	}
}
?>