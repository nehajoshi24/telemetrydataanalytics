<?php
class TruckUtilization extends CI_Controller 
{
	function __construct() {
        parent::__construct();
        $this->load->model('TruckUtilizationModel');
    }

	public function index(){
		if($this->session->userdata('logged_in') == 1){
			$this->load->view('truck_utilization.php');
			
			$this->session->unset_userData('siteIds_selected');
			$this->session->unset_userData('DeptIds_selected');
			$this->session->unset_userData('EquipIds_selected');	
		}
		else{
			$data['message'] = "Please Login";
			$this->load->view('login.php',$data);
		}	
	}

	public function getDetails(){
		$target = $this->input->post('target');
		$draw = $this->input->post('targetPage');
		$pagesize = $this->input->post('length');
  
		$deptIds = NULL;
		$siteIds = NULL;

		if($_POST['search']['value'] == "" && $draw!=1 && $this->input->post('start') == 0){
			$draw = 1;
		}
		else{ 
			$draw = $this->input->post('targetPage');
		}

		$data1 = [];
		if($target == 'site'){
			$data = $this->TruckUtilizationModel->getDetails($target,$draw,$pagesize,$siteIds,$deptIds);	
			   
			$siteIds_selected = $this->session->userData('siteIds_selected');
		
			$cnt =1;
			foreach ($data['details'] as $value) {
	            $row = array();
	            
	            if(!empty($siteIds_selected)){
            		if(in_array($value->siteId,$siteIds_selected)){
            			$temp  = "<center><input type='checkbox' name='sites[]' id='sites_".$cnt."' value=".$value->siteId." checked onclick='siteSelected();'></center>   ";	
            		}
	            	else{
	            		$temp = "<center><input type='checkbox' name='sites[]' id='sites_".$cnt."' value=".$value->siteId." onclick='siteSelected();'></center>   ";
		            }
	            }
	            else{
	            	$temp = "<center><input type='checkbox' name='sites[]' id='sites_".$cnt."' value=".$value->siteId." onclick='siteSelected();'></center>   ";
	            }
	            
	            $row[] = $temp;
	            $row[] = $value->siteName;
	            $row[] = $value->dealerName;
	            $data1[] = $row;
	            $cnt++;
			}
		}

		if($target == 'dept'){
			$siteIds = $this->input->post('selectedSitesMultiplePages');

			if (in_array("on", $siteIds)){
				$pos = array_search('on', $siteIds);
				unset($siteIds[$pos]);
			}
			
			$siteIds = array_unique($siteIds);
			

			$data = $this->TruckUtilizationModel->getDetails($target,$draw,$pagesize,$siteIds,$deptIds);		

			$DeptIds_selected = $this->session->userData('DeptIds_selected');

			$cnt =1;
			foreach ($data['details'] as $value) {
	            $row = array();
	           
	            if(!empty($DeptIds_selected)){
            		if(in_array($value->deptId."_".$value->siteId,$DeptIds_selected)){
            			$temp  = "<center><input type='checkbox' name='depts[]' checked id='depts_".$cnt."' value=".$value->deptId."_".$value->siteId." onclick='deptSelected();'></center>";
            		}
	            	else{
	            		$temp = "<center><input type='checkbox' name='depts[]' id='depts_".$cnt."' value=".$value->deptId."_".$value->siteId." onclick='deptSelected();'></center>";
		            }
	            }
	            else{
	            	$temp = "<center><input type='checkbox' name='depts[]' id='depts_".$cnt."' value=".$value->deptId."_".$value->siteId." onclick='deptSelected();'></center>";
	            }

	            $row[] = $temp;
	            $row[] = $value->deptName;
	            $row[] = $value->siteName;
	            $data1[] = $row;
	            $cnt++;
			}
		}

		if($target == 'equip'){
			$deptIds = $this->input->post('selectedDeptsMultiplePages');
			$deptIds = array_unique($deptIds);
			$tempDeptIds = [];
			foreach ($deptIds as $key => $value) {
				$depts = explode("_",$value); 
				$depts1 = $depts['0']; 
				
				array_push($tempDeptIds,$depts1);
			}
			
			if (in_array("on", $tempDeptIds)){
				$pos = array_search('on',$tempDeptIds);
				unset($tempDeptIds[$pos]);
			}	

			$data = $this->TruckUtilizationModel->getDetails($target,$draw,$pagesize,$siteIds,$tempDeptIds);		

			$EquipIds_selected = $this->session->userData('EquipIds_selected');

			foreach ($data['details'] as $value) {
	            $row = array();
	            if(!empty($EquipIds_selected)){
            		if(in_array($value->equipId,$EquipIds_selected)){
            			$temp  = "<center><input type='checkbox' name='equips[]' id='equipments' checked value=".$value->equipId." onclick='equipSelected();'></center>";
            		}
	            	else{
	            		$temp = "<center><input type='checkbox' name='equips[]' id='equipments' value=".$value->equipId." onclick='equipSelected();'></center>";
		            }
	            }
	            else{
	            	$temp = "<center><input type='checkbox' name='equips[]' id='equipments' value=".$value->equipId." onclick='equipSelected();'></center>";
	            }

	            $row[] = $temp;
	            $row[] = $value->equipName;
	            $row[] = $value->prodId;
	            $row[] = $value->serialNo;
	            $row[] = $value->classname;

	            $data1[] = $row;
			}
		}

		$recordsTotal = $data['totalCount'];

    	$output = array(
            "recordsTotal" => $recordsTotal,
            "recordsFiltered" => $recordsTotal,
            "data" => $data1,
    	);

    	echo json_encode($output);
	}

	public function getHrDetails(){
		$selected_year = $this->input->post('selected_year');
		$equipIds = $this->input->post('equipIds');
		$equipIds = array_unique($equipIds);
		if (in_array("on", $equipIds)){
			$pos = array_search('on', $equipIds);
			unset($equipIds[$pos]);
		}
		
		$data = $this->TruckUtilizationModel->getHrDetails($selected_year,$equipIds);

		echo json_encode($data);
	}


	public function saveSelected(){
		$selectedSitesMultiplePages = $this->input->post('selectedSitesMultiplePages');
		$selectedDeptsMultiplePages = $this->input->post('selectedDeptsMultiplePages');
		$selectedEquipsMultiplePages = $this->input->post('selectedEquipsMultiplePages');

		$type = $this->input->post('type');
		if($type == 'site'){
			$this->session->unset_userData('siteIds_selected');
			$userData = array( 
			   'siteIds_selected'  => $selectedSitesMultiplePages,  
			);  
			$this->session->set_userData($userData);	
		}

		if($type == 'dept'){
			$this->session->unset_userData('DeptIds_selected');
			$userData = array( 
			   'DeptIds_selected'  => $selectedDeptsMultiplePages,  
			);  
			$this->session->set_userData($userData);	
		}

		if($type == 'equip'){
			$this->session->unset_userData('EquipIds_selected');
			$userData = array( 
			   'EquipIds_selected'  => $selectedEquipsMultiplePages,  
			);  
			$this->session->set_userData($userData);	
		}

		print_r($this->session->userData('siteIds_selected'));
		print_r($this->session->userData('DeptIds_selected'));
		print_r($this->session->userData('EquipIds_selected'));
	}

	public function resetSavedSelected(){
		$this->session->unset_userData('siteIds_selected');
		$this->session->unset_userData('DeptIds_selected');
		$this->session->unset_userData('EquipIds_selected');
	}

}

?>