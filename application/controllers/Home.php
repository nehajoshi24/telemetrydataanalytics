<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends CI_Controller {

    
    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('Homes');
    }
    
    public function index() {
    	$this->load->view('diagnosticTroubleCode.php');
    }

}


