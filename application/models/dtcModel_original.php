<?php 
//Set maximum execution time
ini_set('max_execution_time', 600);

class dtcModel extends CI_Model
{
	//Get classI DTC Data 
	public function getClassIDTCData($className,$idUnitCode, $fromDate, $toDate)
	{
		/* Query for fetching data from databse - Data is for number of trucks having particular error code is grouped by error code and order by count of equipments highest*/
			
		//declare data array to be return 
		$data = [];
		
		//Convert dates in required format (from d-m-Y to Y-m-d)	--? this might change as per system date format-check this ???????
		/*$fromDate =  date('Y-m-d', strtotime($fromDate));
		$toDate =  date('Y-m-d', strtotime($toDate));*/

		//Call stored procedure 
		
		/* original */
		//$sql = "EXEC dbo.usp_getClassIDTCData @className='".$className."',@fromDate='".$fromDate."',@toDate='".$toDate."'";
		$sql = "EXEC dbo.usp_getClassIDTCData_4Apr18 @className='".$className."',@idUnitCodes = '".$idUnitCode."',@fromDate='".$fromDate."',@toDate='".$toDate."'"; /* under development */

		$this->load->database();
		$query = $this->db->query($sql);

		//read result set
		if($query->num_rows() < 1)
		{
			echo json_encode(false);
			exit;
		}
		//read error codes and count of those error codes
		foreach ($query->result_array() as $row)
		{
			$tempData =[];
	        $tempData['errorCount'] = $row['errorCount'];
	        $tempData['errorCode'] = $row['errorCode'];
	        $tempData['errorDescription'] = $row['errorDescription'];
	        array_push($data, $tempData);
		}
		//read total count
		if (odbc_next_result($query->result_id)){
		    while ($row = odbc_fetch_array($query->result_id)) {
		        $data['totalCount'] = $row['totalCount'];
		    }
		}
		//read total Equipment Count
		if (odbc_next_result($query->result_id)){
		    while ($row = odbc_fetch_array($query->result_id)) {
		        $data['totalEquipmentCount'] = $row['classCount'];
		    }
		}

		//return output
		echo json_encode($data);
	}

	/* Recostructed function to get DTC data with description of class IV and V*/
	public function getClassIVclassVDTCData($className,$idUnitCode, $fromDate, $toDate)
	{
		/* Query for fetching data from databse - Data is for number of trucks having particular error code is grouped by error code and order by count of equipments highest*/
			
		//declare data array to be return 
		$data = [];

		//Call stored procedure 
		
		/* original */
		//$sql = "EXEC dbo.usp_getClassIDTCData @className='".$className."',@fromDate='".$fromDate."',@toDate='".$toDate."'";
		$sql1 = "EXEC dbo.usp_getClassIVclassVDTCData_4Apr18 @className='".$className."',@idUnitCodes = '".$idUnitCode."',@fromDate='".$fromDate."',@toDate='".$toDate."'"; /* under development */
		//echo $sql1;
		//$this->load->database();
		$query1 = $this->db->query($sql1);

		//read result set
		if($query1->num_rows() < 1)
		{
			echo json_encode(false);
			exit;
		}
		else{
			//read error codes and count of those error codes
			foreach ($query1->result_array() as $row)
			{
				$tempData =[];
	        	$tempData['errorCount'] = $row['errorCount'];
	        	$tempData['errorCode'] = $row['errorCode'];
	        	$tempData['DTCj1939FMI'] = $row['DTCj1939FMI'];
	        	//$tempData['errorDescription'] = $row['errorDescription'];
	        	array_push($data, $tempData);	
			}
			
			//read total count
			if (odbc_next_result($query1->result_id)){
			    while ($row = odbc_fetch_array($query1->result_id)) {
			        $data['totalCount'] = $row['totalCount'];
			    }
			}
			//read total Equipment Count
			if (odbc_next_result($query1->result_id)){
			    while ($row = odbc_fetch_array($query1->result_id)) {
			        $data['totalEquipmentCount'] = $row['classCount'];
			    }
			}
			//return output
			echo json_encode($data);
		}
	}

	//Get classIV and classV DTC Data 
	/*public function getClassIVclassVDTCData($className, $idUnitCode,$fromDate, $toDate)
	{
		//declare data array to be return 
		$data = [];
		
		//Convert dates in required format (from d-m-Y to Y-m-d)	--? this might change as per system date format-check this ???????
		$fromDate =  date('Y-m-d', strtotime($fromDate));
		$toDate =  date('Y-m-d', strtotime($toDate));

		//Call stored procedure 
		$sql = "EXEC dbo.usp_getClassIVclassVDTCData_4Apr18 @className='".$className."',@idUnitCodes = '".$idUnitCode."',@fromDate='".$fromDate."',@toDate='".$toDate."'";
		//echo $sql;exit;
		$this->load->database();
		$query = $this->db->query($sql);

		//read result set
		if($query->num_rows() < 1)
		{
			echo json_encode(false);
			exit;
		}
		//read error codes and count of those error codes
		foreach ($query->result_array() as $row)
		{
			$tempData =[];
        	$tempData['errorCount'] = $row['errorCount'];
        	$tempData['errorCode'] = $row['errorCode'];
        	$tempData['DTCj1939FMI'] = $row['DTCj1939FMI'];
        	$tempData['errorDescription'] = $row['errorDescription'];
        	array_push($data, $tempData);	
		}

		//read total count
		if (odbc_next_result($query->res0ult_id)){
		    while ($row = odbc_fetch_array($query->result_id)) {
		        $data['totalCount'] = $row['totalCount'];
		    }
		}
		//read total Equipment Count
		if (odbc_next_result($query->result_id)){
		    while ($row = odbc_fetch_array($query->result_id)) {
		        $data['totalEquipmentCount'] = $row['classCount'];
		    }
		}

		//return output
		echo json_encode($data);
	}*/

	///Get unit codes data for the selected duration (for class II and class III)
	public function GetUnitCodes($className, $fromDate, $toDate)
	{
		//declare data array to be return 
		$data = [];
		
		//Convert dates in required format (from d-m-Y to Y-m-d)	--? this might change as per system date format-check this ???????
		

		//Call stored procedure 
		$sql = "EXEC dbo.usp_getUnitCodes_4Apr18 @className='".$className."',@fromDate='".$fromDate."',@toDate='".$toDate."'";

		$this->load->database();
		$query = $this->db->query($sql);

		//read result set
		if($query->num_rows() < 1)
		{
			echo json_encode(false);
			exit;
		}
		//read error codes and count of those error codes
		foreach ($query->result_array() as $row)
		{
			$tempData =[];
        	$tempData['unitCodeName'] = $row['unitCodeName'];
        	$tempData['idUnitCode'] = $row['idUnitCode'];

        	array_push($data, $tempData);
		}
		
		//return output to controller
		echo json_encode($data);
	}

	///Get DTC data for class II and class III where unit codes are considered
	public function getUnitCodeDTCData($className, $idUnitCode, $fromDate, $toDate)
	{
		//declare data array to be return 
		$data = [];
	
		//Call stored procedure 
		//$sql = "EXEC dbo.usp_getUnitCodeDTCData @className='".$className."', @idUnitCode='".$idUnitCode."',@fromDate='".$fromDate."',@toDate='".$toDate."'";
		$sql = "EXEC dbo.usp_getUnitCodeDTCData_4Apr18 @className='".$className."', @idUnitCodes='".$idUnitCode."',@fromDate='".$fromDate."',@toDate='".$toDate."'";
	
		$this->load->database();
		$query = $this->db->query($sql);

		
		//read error codes and count of those error codes
		foreach ($query->result_array() as $row)
		{
			$tempData =[];
	        $tempData['errorCount'] = $row['errorCount'];
	        $tempData['errorCode'] = $row['errorCode'];
	        $tempData['errorDescription'] = $row['errorDescription'];
	        array_push($data, $tempData);
	        //echo $row['errorCount']." : ".$row['errorCode']." : ".$row['errorDescription'];
		}
		//print_r($data);
		//read total count
		if (odbc_next_result($query->result_id)){
		    while ($row = odbc_f etch_array($query->result_id)) {
		        $data['totalCount'] = $row['totalCount'];
		    }
		}
		//read total Equipment Count
		if (odbc_next_result($query->result_id)){
		    while ($row = odbc_fetch_array($query->result_id)) {
		        $data['totalEquipmentCount'] = $row['classCount'];
		    }
		}

		$this->db->close();
		echo json_encode($data);
	}

	///Get DTC data for big trucks as per subsystem
	public function GetBigTruckDTCData($className, $subsystem, $fromDate, $toDate)
	{
		//declare data array to be return 
		$data = [];
		
		//Convert dates in required format (from d-m-Y to Y-m-d)	--? this might change as per system date format-check this ???????
		$fromDate =  date('Y-m-d', strtotime($fromDate));
		$toDate =  date('Y-m-d', strtotime($toDate));

		//Call stored procedure 
		$sql = "EXEC dbo.usp_getBigTruckDTCData_4Apr2018 @className='".$className."', @subSystem='".$subsystem."',@fromDate='".$fromDate."',@toDate='".$toDate."'";
		$this->load->database();
		$query = $this->db->query($sql);

		//read result set
		if($query->num_rows() < 1)
		{
			echo json_encode(false);
			exit;
		}
		if($subsystem == 1)//Hydraulic
		{
			//read error codes and count of those error codes
			foreach ($query->result_array() as $row)
			{
				$tempData =[];
	        	$tempData['errorCount'] = $row['errorCount'];
	        	$tempData['errorCode'] = $row['errorCode'];
	        	$tempData['DTCDanaHydraulicError3'] = $row['DTCDanaHydraulicError3'];
	        	$tempData['errorDescription'] = $row['errorDescription'];
	        	array_push($data, $tempData);
			}

			//print_r($data);exit;
			//read total count
			if (odbc_next_result($query->result_id)){
			    while ($row = odbc_fetch_array($query->result_id)) {
			        $data['totalCount'] = $row['totalCount'];
			    }
			}
			//read total Equipment Count
			if (odbc_next_result($query->result_id)){
			    while ($row = odbc_fetch_array($query->result_id)) {
			        $data['totalEquipmentCount'] = $row['classCount'];
			    }
			}
			//print_r($data);exit;
			//return output to controller
			echo json_encode($data);
		}
		else if($subsystem == 2)//Transmission
		{
			//read error codes and count of those error codes
			foreach ($query->result_array() as $row)
			{
				$tempData =[];
	        	$tempData['errorCount'] = $row['errorCount'];
	        	$tempData['errorCode'] = $row['errorCode'];
	        	$tempData['errorDescription'] = $row['errorDescription'];
	        	array_push($data, $tempData);
			}

			//read total count
			if (odbc_next_result($query->result_id)){
			    while ($row = odbc_fetch_array($query->result_id)) {
			        $data['totalCount'] = $row['totalCount'];
			    }
			}
			//read total Equipment Count
			if (odbc_next_result($query->result_id)){
			    while ($row = odbc_fetch_array($query->result_id)) {
			        $data['totalEquipmentCount'] = $row['classCount'];
			    }
			}
			
			//return output to controller
			echo json_encode($data);
		}
		if($subsystem == 3)//Others (Engine,Spreader etc)
		{
			//read error codes and count of those error codes
			foreach ($query->result_array() as $row)
			{
				$tempData =[];
	        	$tempData['errorCount'] = $row['errorCount'];
	        	$tempData['errorCode'] = $row['errorCode'];
	        	$tempData['DTCj1939FMI'] = $row['DTCj1939FMI'];
	        	$tempData['errorDescription'] = $row['errorDescription'];
	        	array_push($data, $tempData);
			}

			//read total count
			if (odbc_next_result($query->result_id)){
			    while ($row = odbc_fetch_array($query->result_id)) {
			        $data['totalCount'] = $row['totalCount'];
			    }
			}
			//read total Equipment Count
			if (odbc_next_result($query->result_id)){
			    while ($row = odbc_fetch_array($query->result_id)) {
			        $data['totalEquipmentCount'] = $row['classCount'];
			    }
			}
			
			//return output to controller
			echo json_encode($data);
		}
	}
}
?>