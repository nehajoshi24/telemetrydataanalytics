<?php 
//Set maximum execution time
ini_set('max_execution_time', 6000);

class dtcModel extends CI_Model
{	
	///Get unit codes data for the selected duration (for class II and class III)
	public function GetUnitCodes($className)
	{
		//declare data array to be return 
		$data = [];
		
		//Call stored procedure 
		$sql = "EXEC dbo.usp_getUnitCodes @className='".$className."',@fromDate='',@toDate=''";

		$this->load->database();
		$query = $this->db->query($sql);

		//read result set
		if($query->num_rows() < 1)
		{
			echo json_encode(false);
			exit;
		}
		//read error codes and count of those error codes
		foreach ($query->result_array() as $row)
		{
			$tempData =[];
        	$tempData['unitCodeName'] = $row['unitCodeName'];
        	$tempData['idUnitCode'] = $row['idUnitCode'];

        	array_push($data, $tempData);
		}

		//return output to controller
		echo json_encode($data);
	}

	///Get DTC data for class I to class V where unit codes are considered
	public function getUnitCodeDTCData($className, $idUnitCode, $toDate,$from_hrMeter,$to_hrMeter,$flags)
	{
		//declare data array to be return 
		$data = [];

		if(in_array("All",$idUnitCode)){
			$idUnitCode = "All";
		}
		else{
			$idUnitCode = implode(",",$idUnitCode);	
		}

		//Call SPs as per class selected by user
		if($className == "classI"){
			$sql = "EXEC dbo.usp_getClassIDTCData @className='".$className."',@idUnitCodes = '".$idUnitCode."',@fromMainServiceMeter= '".$from_hrMeter."',@toMainServiceMeter = '".$to_hrMeter."', @toDate='".$toDate."',@flags = '".$flags."'";
		}
		if($className == "classII" || $className == "classIII"){
			$sql = "EXEC dbo.usp_getclassIIclassIIIDTCData @className='".$className."', @idUnitCodes='".$idUnitCode."',@fromMainServiceMeter= '".$from_hrMeter."',@toMainServiceMeter = '".$to_hrMeter."',@toDate='".$toDate."',@flags = '".$flags."'";
		}
		if($className == "classIV" || $className == "classV"){
			$sql = "EXEC dbo.usp_getClassIVclassVDTCData @className='".$className."', @idUnitCodes='".$idUnitCode."',@fromMainServiceMeter= '".$from_hrMeter."',@toMainServiceMeter = '".$to_hrMeter."',@toDate='".$toDate."',@flags = '".$flags."'";
		}

		$this->load->database();
		$query = $this->db->query($sql);
		if($query->num_rows() < 1)
		{
			echo json_encode(false);
			exit;
		}

		if($className == "classI" || $className == "classII" || $className == "classIII"){
			foreach ($query->result_array() as $row)
			{
				$tempData =[];
		        $tempData['errorCount'] = $row['errorCount'];
		        $tempData['errorCode'] = $row['errorCode'];
		        if(is_null($row['errorDescription'])){
		        	$desc = "Not Available";
		        }
		        else{
		        	$desc = $row['errorDescription'];
		        }
		        $tempData['errorDescription'] = $desc;
		        array_push($data, $tempData);
			}
		}
		if($className == "classIV" || $className == "classV"){
			//read error codes and count of those error codes
			foreach ($query->result_array() as $row)
			{
				$tempData =[];
	        	$tempData['errorCount'] = $row['errorCount'];
	        	$tempData['errorCode'] = $row['errorCode']."-".$row['DTCj1939FMI'];
	        	if(is_null($row['errorDescription'])){
		        	$desc = "Not Available";
		        }
		        else{
		        	$desc = $row['errorDescription'];
		        }
	        	$tempData['errorDescription'] = $desc;
	        	array_push($data, $tempData);	
			}
		}

		$this->session->unset_userdata('exportData');
		$sessionData = [];
		
		foreach ($data as $value) {
			$newdata = array(
               'errorCount'  => $value['errorCount'],
               'errorCode'     => $value['errorCode'],
               'errorDescription' => $value['errorDescription']
           	);
           	array_push($sessionData,$newdata);
		}
	
		$this->session->set_userdata('exportData', $sessionData);

		//read total count
		if (odbc_next_result($query->result_id)){
		    while ($row = odbc_fetch_array($query->result_id)) {
		        $data['totalCount'] = $row['totalCount'];
		    }
		}
		//read total Equipment Count
		if (odbc_next_result($query->result_id)){
		    while ($row = odbc_fetch_array($query->result_id)) {
		        $data['totalEquipmentCount'] = $row['classCount'];
		    }
		}

		$this->db->close();

		echo json_encode($data);	
		
	}

	///Get DTC data for big trucks as per subsystem
	public function GetBigTruckDTCData($className, $subsystem,$toDate,$from_hrMeter,$to_hrMeter,$fault_flags)
	{
		//declare data array to be return 
		$data = [];

		$sql = "EXEC dbo.usp_getBigTruckDTCData @className='".$className."',@subSystem='".$subsystem."',@fromMainServiceMeter= '".$from_hrMeter."',@toMainServiceMeter = '".$to_hrMeter."',@toDate='".$toDate."',@flags='".$fault_flags."'"; /* @fromDate='".$fromDate."',*/
		 
		$this->load->database();
		$query = $this->db->query($sql);

		//read result set
		if($query->num_rows() < 1)
		{
			echo json_encode(false);
			exit;
		}
		if($subsystem == 1)//Hydraulic
		{
			//read error codes and count of those error codes
			foreach ($query->result_array() as $row)
			{	
				$tempData =[];
	        	$tempData['errorCount'] = $row['errorCount'];
	        	$tempData['errorCode'] = $row['errorCode']."-".$row['DTCDanaHydraulicError3'];
	        	if(is_null($row['errorDescription'])){
		        	$desc = "Not Available";
		        }
		        else{
		        	$desc = $row['errorDescription'];
		        }
	        	$tempData['errorDescription'] = utf8_encode($desc);
	        	array_push($data, $tempData);
			} 
		}
			
		else if($subsystem == 2)//Transmission
		{
			//read error codes and count of those error codes
			foreach ($query->result_array() as $row)
			{
				$tempData = [];
	        	$tempData['errorCount'] = $row['errorCount'];
	        	$tempData['errorCode'] = $row['errorCode'];
	        	if(is_null($row['errorDescription'])){
		        	$desc = "Not Available";
		        }
		        else{
		        	$desc = $row['errorDescription'];
		        }
	        	$tempData['errorDescription'] = $desc;
	        	array_push($data, $tempData);
			}
		}
		if($subsystem == 3)//Others (Engine,Spreader etc)
		{
			//read error codes and count of those error codes
			foreach ($query->result_array() as $row)
			{
				$tempData =[];
	        	$tempData['errorCount'] = $row['errorCount'];
	        	$tempData['errorCode'] = $row['errorCode']."-".$row['DTCj1939FMI'];
	        	//$tempData['DTCj1939FMI'] = $row['DTCj1939FMI'];
	        	if(is_null($row['errorDescription'])){
		        	$desc = "Not Available";
		        }
		        else{
		        	$desc = $row['errorDescription'];
		        }
	        	$tempData['errorDescription'] = $desc;
	        	array_push($data, $tempData);
			}	
		}

		$this->session->unset_userdata('exportData');

		$sessionData = [];
		
		foreach ($data as $value) {
			$newdata = array(
               'errorCount'  => $value['errorCount'],
               'errorCode'     => $value['errorCode'],
               'errorDescription' => $value['errorDescription']
           	);
           	array_push($sessionData,$newdata);
		}
	
		$this->session->set_userdata('exportData', $sessionData);

		//read total count
		if (odbc_next_result($query->result_id)){
		    while ($row = odbc_fetch_array($query->result_id)) {
		        $data['totalCount'] = $row['totalCount'];
		    }
		}

		if (odbc_next_result($query->result_id)){
		    while ($row = odbc_fetch_array($query->result_id)) {
		        $data['totalEquipmentCount'] = $row['classCount'];
		    }
		}
		echo json_encode($data);	
	}
}