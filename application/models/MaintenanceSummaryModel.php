<?php 
//Set maximum execution time
ini_set('max_execution_time', 6000);

class MaintenanceSummaryModel extends CI_Model
{
	public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    
    public function getSites(){
    	$data =[];
    	$sql = "EXEC dbo.usp_getSites";

    	$query = $this->db->query($sql);

		//read result set
		//return false if no data
		if($query->num_rows() < 1)
		{
			echo json_encode(false);
			exit;
		}
		//return data 
		foreach ($query->result_array() as $row)
		{
	        array_push($data, $row);
		}

		//return output
		return json_encode($data);
    }

    //get equipments of selected site and class 
    public function getEquipmentsForMaintaince($siteId, $className)
    {
    	$data =[];
    	//call SP
    	$sql = "EXEC dbo.usp_getTruckIds  @siteIds='".$siteId."', @className='".$className."'";
    	
    	$query = $this->db->query($sql);
   
   		//check if result set has data
    	if($query->num_rows() < 1)
		{
			echo json_encode(false);
			exit;
		}
		//read result set
		foreach ($query->result_array() as $row)
		{
	        array_push($data, $row);
		}
		//return output
		return json_encode($data);
    }

    //Get truck maintaince details for equipments from selected sites
    public function getTruckMaintenanceDetails($equipmentId, $className, $fromDate, $toDate)
    {
    	$data =[];

    	//call to SP
    	$sql = "EXEC dbo.usp_getTruckMaintenanceDetails  @equipmentId='".$equipmentId."', @className='".$className."', @fromDate='".$fromDate."', @toDate='".$toDate."'";
    	
    	$query = $this->db->query($sql);
   
   		//check if result set has data
    	if($query->num_rows() < 1)
		{
			echo json_encode(false);
			exit;
		}
		//read result set
		foreach ($query->result_array() as $row)
		{	
		
	        $data['equipmentId'] = $row['equipmentId'];
	        $data['hoursOfOperation'] = $row['currentWeekHoursOfOperation'];
	        $data['overallHoursOfOperation'] = $row['overallHoursOfOperation'];
	        $data['currentWeekFaults'] = $row['currentWeekFaults'];
	        $data['overallFaults'] = $row['overallFaults'];
	        // $data['currentWeekImpacts'] = $row['currentWeekImpacts'];
	        // $data['overallImpacts'] = $row['overallImpacts'];
	        // $data['currentWeekChecklistFailures'] = $row['currentWeekChecklistFailures'];
	        // $data['overallChecklistFailures'] = $row['overallChecklistFailures'];
	        $data['latitude'] = $row['latitude'];
		    $data['longitude'] = $row['longitude'];
		}

		//return output
		echo json_encode($data);
    }
}
?>