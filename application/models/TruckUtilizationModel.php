<?php 
//Set maximum execution time
ini_set('max_execution_time', 6000);

class TruckUtilizationModel extends CI_Model
{	
    var $sql;

	public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    /* Function to get site,department and equipment details */
    public function getDetails($target,$draw,$pagesize,$siteId,$deptIds){

        $searchvalue = $_POST['search']['value'];

        if($searchvalue != "" && !strlen($searchvalue) <= 2){

            if($target == 'site'){
                if($this->session->userdata('search_string') != $searchvalue)
                {   
                    $this->sql = "exec [dbo].[usp_searchSiteDeptEqForUtilization] @selection = '".$target."',@string = '".$searchvalue."',@pageNo = '1',@pagesize = '".$pagesize."'"; 

                    $newdata1 = array(
                       'targetType'  => $target,
                       'search_string' => $searchvalue 
                    );

                    $this->session->set_userdata($newdata1);

                }
                else 
                {
                    $this->sql = "exec [dbo].[usp_searchSiteDeptEqForUtilization] @selection = '".$target."',@string = '".$searchvalue."',@pageNo = '".$draw."',@pagesize = '".$pagesize."'"; 
                }
                
            }

            if($target == 'dept'){
                $siteIds = implode(",",$siteId);

                if($this->session->userdata('search_string') != $searchvalue){

                    $this->sql = "exec [dbo].[usp_searchSiteDeptEqForUtilization] @selection = '".$target."',@string = '".$searchvalue."',@pageNo = '1',@pagesize = '".$pagesize."',@siteIds = '".$siteIds."'";     
              
                     $newdata1 = array(
                       'targetType'  => $target,
                       'search_string' => $searchvalue 
                    );

                    $this->session->set_userdata($newdata1);

                }
                else{
                    $this->sql = "exec [dbo].[usp_searchSiteDeptEqForUtilization] @selection = '".$target."',@string = '".$searchvalue."',@pageNo = '".$draw."',@pagesize = '".$pagesize."',@siteIds = '".$siteIds."'";    
                }
            }

            if($target == 'equip'){
                $deptIds = implode(",",$deptIds);
                if($this->session->userdata('search_string') != $searchvalue){

                    $this->sql = "exec [dbo].[usp_searchSiteDeptEqForUtilization] @selection = '".$target."',@string = '".$searchvalue."',@pageNo = '1',@pagesize = '".$pagesize."',@deptIds = '".$deptIds."'";   
          
                     $newdata1 = array(
                       'targetType'  => $target,
                       'search_string' => $searchvalue 
                    );

                    $this->session->set_userdata($newdata1);

                }
                else{
                    $this->sql = "exec [dbo].[usp_searchSiteDeptEqForUtilization] @selection = '".$target."',@string = '".$searchvalue."',@pageNo = '".$draw."',@pagesize = '".$pagesize."',@deptIds = '".$deptIds."'";   
                }       
            }
        }
        else{
            if($target == 'site'){
                $this->sql = "exec [dbo].[usp_getSiteDeptEqForUtilization] @selection = '".$target."',@pageNo = '".$draw."',@pagesize = '".$pagesize."'"; 
            }

            if($target == 'dept'){
                $siteIds = implode(",",$siteId);
                $this->sql = "exec [dbo].[usp_getSiteDeptEqForUtilization] @selection = '".$target."',@pageNo = '".$draw."',@pagesize = '".$pagesize."',@siteIds = '".$siteIds."'";       
            }

            if($target == 'equip'){
                $deptIds = implode(",",$deptIds);
                $this->sql = "exec [dbo].[usp_getSiteDeptEqForUtilization] @selection = '".$target."',@pageNo = '".$draw."',@pagesize = '".$pagesize."',@deptIds = '".$deptIds."'";
            }
        }

        $this->load->database();
        $query = $this->db->query($this->sql);
        if($query->num_rows() < 1)
        {
            echo json_encode(false);
            exit;
        }

        $data['details'] = $query->result();

        if (odbc_next_result($query->result_id)){
		    while ($row = odbc_fetch_array($query->result_id)) {

		        $totalCount = $row['Count'];
		    }
		}

        $data['totalCount'] = $totalCount;

        return $data;
        $this->db->close();    
    }	

    /* function to get hourly details of individual truck */
    public function getHrDetails($selected_year,$equipIds){
    	$equipIds = implode(",",$equipIds);
    	$sql = "exec [dbo].[usp_getTruckUtilizationDetails] @EquipIds = '".$equipIds."', @year = '".$selected_year."'";		

    	$this->load->database();
        $query = $this->db->query($sql);

        if($query->num_rows() < 1)
		{
			echo json_encode(false);
			exit;
		}

		$data = [];$data1 = [];
		$equipmentIds = [];

        $details = [];
        $year = 0;

        $details['details'] = $query->result_array();

        if (odbc_next_result($query->result_id)){
            while ($row = odbc_fetch_array($query->result_id)) 
            {      
                $tempData['equipId'] = $row['equipmentId'];
                $tempData['idleTime_avg'] = $row['idleTime_avg'];
                $tempData['workingTime_avg'] = $row['workingTime_avg'];
                $tempData['mainServiceMeter_avg'] = $row['mainServiceMeter_avg'];
                $tempData['MSMAchieved_hrs'] = $row['MSMAchieved_hrs'];
                $tempData['year'] = $row['Year'];
                $year = $row['Year'];
                array_push($data, $tempData);
            }

            $details['yearly_avg'] = $data;
        }
        return $details;
    }
}
?>