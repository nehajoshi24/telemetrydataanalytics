<?php 
//Set maximum execution time (seconds)
ini_set('max_execution_time', 6000);

class operatorSCardModel extends CI_Model
{
	public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

     public function getSites(){
    	$data =[];
    	$sql = "EXEC dbo.usp_getSites";

    	$query = $this->db->query($sql);

		//read result set
		if($query->num_rows() < 1)
		{
			echo json_encode(false);
			exit;
		}

		foreach ($query->result_array() as $row)
		{
	        array_push($data, $row);
		}
		//return output
		return json_encode($data);
    }

   	//Get operators for selected sites (operator name and id)
	public function getOperators($siteIds)
	{	
		$siteIds = implode(",",$siteIds);

    	$data = [];
    	$sql = "EXEC dbo.usp_getOperatorsFromSites @siteIds='".$siteIds."' ";
    	
    	$query = $this->db->query($sql);
   
    	if($query->num_rows() < 1)
		{
			echo json_encode(false);
			exit;
		}

		foreach ($query->result_array() as $row)
		{
	        array_push($data, $row);
		}
		//return output
		return json_encode($data);
	}

	//Get operator scorecard details for selected operators for selected duration
	public function getOperatorScorecardData($operatorIdsList, $fromDate, $toDate)
	{
		$data = [];
		$operatorIdsList = implode(",",$operatorIdsList);
    	// $sql = "EXEC dbo.usp_getOperatorScoreCardData @operatorIds='".$operatorIdsList."',@fromDate='".$fromDate."',@toDate='".$toDate."'";		
    	$sql = "EXEC dbo.usp_getOperatorScoreCardData @operatorIds='".$operatorIdsList."',@fromDate='".$fromDate."',@toDate='".$toDate."'";		
    	//EXEC dbo.usp_getOperatorScoreCardData @operatorIds='203413,203414,203456,205128,205130',@fromDate='2018-06-10',@toDate='2018-07-10'
    	//execute query
		$query = $this->db->query($sql);
		
		
		//read resultsets
    	if($query->num_rows() < 1)
		{
			echo json_encode(false);
			exit;
		}
		
		foreach ($query->result_array() as $row)
		{
			$tempData = [];
			$tempData['fkidOperator'] = $row['operatorId'];
			$tempData['name'] = $row['name'];
	        $tempData['idleTime'] = $row['idleTime'];
	        $tempData['WorkingTime'] = $row['WorkingTime'];
	        $tempData['hydraulicFunctionTime'] = $row['hydraulicFunctionTime'];
	        $tempData['distanceDriven'] = $row['distanceDriven'];
	        $tempData['NoOfLoads'] = $row['NoOfLoads'];
	        $tempData['OverSpeedCount'] = $row['OverSpeedCount'];
	        $tempData['ImpactCount'] = $row['ImpactCount'];
	        $tempData['seatBeltInterlockViolationCount'] = $row['seatBeltInterlockViolationCount'];
	        $tempData['parkBrakeInterlockViolationCount'] = $row['parkBrakeInterlockViolationCount'];

	        array_push($data, $tempData);
		}

	echo json_encode($data);
	}
}
?>