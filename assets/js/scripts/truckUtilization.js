var base_url = document.getElementById("hdnBaseUrl").value;

var selected_values =[];
var ind = 0;

var selectedSitesMultiplePages = [];
var selectedDeptsMultiplePages = [];
var selectedEquipsMultiplePages = [];

function siteSelected_test(){
  $("#depts_all").prop('checked', false);
  $("#equipments_all").prop('checked', false);
}

/* this function get call on check/uncheck of checkbox */
function siteSelected()
{ 
  $(':checkbox:checked').each(function(i){
    if(jQuery.inArray($(this).val(), selectedSitesMultiplePages) == -1){
      selectedSitesMultiplePages.push($(this).val());
    }    
  });

  /* To remove unchecked values from global variable */
  $("input[name='sites[]']:not(:checked)").each(function (){
    if (jQuery.inArray($(this).val(), selectedSitesMultiplePages)!='-1') {
      var i = selectedSitesMultiplePages.indexOf($(this).val());
      if(i != -1) {
        selectedSitesMultiplePages.splice(i, 1);
      }
    }
  });

  $.ajax({
    type: "POST",
    url: base_url + "TruckUtilization/saveSelected",
    async: true,
    contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
    data: {
        selectedSitesMultiplePages : selectedSitesMultiplePages,
        type : 'site'
    },
    error: function(xhr, status, error) {
        alert("Error occured while getting data! Please try again.");
    },
  });
}

function deptSelected()
{
  $("input[name='depts[]']:checked").each( function (i) {
    if(jQuery.inArray($(this).val(), selectedDeptsMultiplePages) == -1){
      selectedDeptsMultiplePages.push($(this).val());
    }    
  });

  /* To remove unchecked values from global variable */
  $("input[name='depts[]']:not(:checked)").each(function (){
    if (jQuery.inArray($(this).val(), selectedDeptsMultiplePages)!='-1') {
      var i = selectedDeptsMultiplePages.indexOf($(this).val());
      if(i != -1) {
        selectedDeptsMultiplePages.splice(i, 1);
      }
    }
  });

  $.ajax({
    type: "POST",
    url: base_url + "TruckUtilization/saveSelected",
    async: true,
    contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
    data: {
      selectedDeptsMultiplePages : selectedDeptsMultiplePages,
      type : 'dept'
    },
    error: function(xhr, status, error) {
      alert("Error occured while getting data! Please try again.");
    }
  });
}             

function equipSelected()
{
  $("input[name='equips[]']:checked").each( function (i) {
    if(jQuery.inArray($(this).val(), selectedEquipsMultiplePages) == -1){
      selectedEquipsMultiplePages.push($(this).val());
    }    
  });  

  /* To remove unchecked values from global variable */
  $("input[name='equips[]']:not(:checked)").each(function (){
    if (jQuery.inArray($(this).val(), selectedEquipsMultiplePages)!='-1') {
      var i = selectedEquipsMultiplePages.indexOf($(this).val());
      if(i != -1) {
        selectedEquipsMultiplePages.splice(i, 1);
      }
    }
  });

  $.ajax({
    type: "POST",
    url: base_url + "TruckUtilization/saveSelected",
    async: true,
    contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
    data: {
        selectedEquipsMultiplePages : selectedEquipsMultiplePages,
        type : 'equip'
    },
    error: function(xhr, status, error) {
        alert("Error occured while getting data! Please try again.");
    }
  });
} 

$(document).ready(function() {
  /* On click of reset all selected check boxes */
  $("#reset_all").on("click",function(){
    
    $("#depts_all").prop('checked', false);
    $("#equipments_all").prop('checked', false);

    $('input[name="sites[]"]').prop('checked', false);
    $('input[name="depts[]"]').prop('checked', false);
    $('input[name="equips[]"]').prop('checked', false);

    selectedSitesMultiplePages = []; 
    selectedDeptsMultiplePages = [];
    selectedEquipsMultiplePages = [];

    $.ajax({
      type: "POST",
      url: base_url + "TruckUtilization/resetSavedSelected",
      async: true,
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      error: function(xhr, status, error) {
          alert("Error occured while getting data! Please try again.");
      }
    });
  });
    
  /* Check all - check all checkbox to select all departments on current page */
  $("#depts_all").on("change",function(){
    
    if ($(this).is(':checked')) {
      $('input[name="depts[]"]').prop('checked', true);  
    } 
    else {
      $('input[name="depts[]"]').prop('checked',false);
    }

    deptSelected();
  }); /* end of onchange of select all check box for department */

  $("#equipments_all").on("change",function(){
    
    if ($(this).is(':checked')) {
      $('input[name="equips[]"]').prop('checked', true);  
    } 
    else {
      $('input[name="equips[]"]').prop('checked',false);
    }
    equipSelected();
  });


  /* Initially hide all buttons,tables and its div's */
  $("#deptTable_div").hide();
  $("#selected_dept").hide();
  $("#equipTable_div").hide();

  $("#selected_site_btn").show();
  $("#selected_equip_btn").hide();
  $("#selected_dept_btn").hide();

  $("#selected_site_btn_back").hide();
  $("#selected_dept_btn_back").hide();
  $("#selected_equip_btn_back").hide();
  
  $("#reset_all").show();

  $("#plotArea").hide(); /* Hide graph area */

  /* Generate data table for displaying sites */
  table = $('#table').DataTable({
    "processing": true, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    "order": [], //Initial no order.

    // Load data for the table's content from an Ajax source
    "ajax": {
      "url": base_url + "TruckUtilization/getDetails",
      "type": "POST",
      "data" : {
        target : 'site'
        //selectedSitesMultiplePages : selectedSitesMultiplePages
      },
    },
    //Set column definition initialisation properties.
    "columnDefs": [     
      { 
        "targets": [ -1 ], //last column
        "orderable": false, //set not orderable
      },
      { 
        "targets": [ -2 ], //2 last column (photo)
        "orderable": false, //set not orderable
      },
    ],
  }); /* end of generating data table for displaying sites*/

  /* On click of next after selecting sites , fetch data of departments and display it in data table */
  $("#selected_site_btn").on("click",function(){

    if(selectedSitesMultiplePages.length == 0){
      alert("Please select at least one site");
      return;
    }
    
    $("#siteTable").hide();

    $("#deptTable_div").show();

    $("#selected_site_btn").hide();
    $("#selected_dept_btn").show();
    $("#selected_equip_btn").hide();

    $("#selected_site_btn_back").show(); /* To go to sites */
    $("#selected_dept_btn_back").hide(); /* To go to departments */
    $("#selected_equip_btn_back").hide(); /* To go to equipments*/

    $("#deptTable").dataTable().fnDestroy();
    $('#depts_all').prop('checked',false); /* To uncheck select all checkbox */
    /* If data table is not initialised then generate data table else only display previously generated data table. */
    if ( ! $.fn.DataTable.isDataTable( '#deptTable' ) ) {
      table = $('#deptTable').DataTable({
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

              // Load data for the table's content from an Ajax source
        "ajax": {
          "url": base_url + "TruckUtilization/getDetails",
          "type": "POST",
          "data":{
            target : 'dept',
            /*siteIds : val,*/
            selectedSitesMultiplePages : selectedSitesMultiplePages
          },
        },

        //Set column definition initialisation properties.
        "columnDefs": [
          { 
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
              },
              { 
            "targets": [ -2 ], //2 last column (photo)
            "orderable": false, //set not orderable
          },
        ],
      });       
    } /* end of if */

    $("#selected_site").hide();
    $("#selected_dept").show();
  }); /* end of On click of selected_site_btn */  

  /* On click of next after selecting departments , fetch data of equipments and display it in data table */
  $("#selected_dept_btn").on("click",function(){

    if(selectedDeptsMultiplePages.length == 0 || selectedDeptsMultiplePages.length < 0){
      alert("Please select at least one department");
      return;
    }

    /* hide previous both tables */
    $("#siteTable").hide();
    $("#deptTable_div").hide();

    $("#selected_site_btn").hide();
    $("#selected_dept_btn").hide();
    $("#selected_equip_btn").show();

    $("#selected_site_btn_back").hide();
    $("#selected_dept_btn_back").show();
    $("#selected_equip_btn_back").hide();

    /* hide previous both buttons */
    $("#selected_site").hide();
    $("#selected_dept").hide();

    /* show equipment table div and table */
    $("#equipTable_div").show();

    $("#reset_all").show();

    $("#equip_table").dataTable().fnDestroy();
    $('#equipments_all').prop('checked',false); /* To uncheck select all checkbox */

    /* If data table is not initialised then generate data table else only display previously generated data table. */
    if ( ! $.fn.DataTable.isDataTable( '#equip_table' ) ) {
      table = $('#equip_table').DataTable({
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

              // Load data for the table's content from an Ajax source
        "ajax": {
          "url": base_url + "TruckUtilization/getDetails",
          "type": "POST",
          "data":{
            target : 'equip',
            /*deptIds : val*/
            selectedDeptsMultiplePages : selectedDeptsMultiplePages
          },
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
          "targets": [ -1 ], //last column
          "orderable": false, //set not orderable
        },
        { 
          "targets": [ -2 ], //2 last column (photo)
          "orderable": false, //set not orderable
        },],
      });
    } /* End of if */
  });

  /* On click of next after selecting equipments , fetch details of equipments and display it in graph */
  $("#selected_equip_btn").on("click",function(){    
    if(selectedEquipsMultiplePages.length == 0 || selectedEquipsMultiplePages.length < 0){
      alert("Please select at least one equipment");
      return;
    }

    $("#siteTable").hide();
    $("#deptTable_div").hide();

    $("#selected_site_btn").hide();
    $("#selected_dept_btn").hide();
    $("#selected_equip_btn").hide();

    $("#selected_site_btn_back").hide();
    $("#selected_dept_btn_back").hide();
    $("#selected_equip_btn_back").show();

    /* hide previous both buttons */
    $("#selected_site").hide();
    $("#selected_dept").hide();

    /* show equipment table div and table */
    $("#equipTable_div").hide();

    /* show graph area */
    $("#plotArea").show();
    
    $("#yearly_avg").show();
    $("#utilization_absolute").hide();

    $("#show_absolute_details").show();

    $("#show_avg").hide();

    $("#reset_all").hide();
    var selected_year = $("#select_year").val();
    
    $.ajax({
      beforeSend: function() {
        //Fade whole page
        $("body").prepend("<div class='overlay'></div>");

        $(".overlay").css({
            "position": "absolute",
            "width": $(document).width(),
            "height": $(document).height(),
            "z-index": 9999,
        }).fadeTo(0, 0.8);
        //show loader  
        $("#loadimage").css("z-index", "99999").show();
      },
      type: "POST",
      url: base_url + "TruckUtilization/getHrDetails",
      async: true,
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      data: {
        selected_year : selected_year,
        equipIds : selectedEquipsMultiplePages
      },
      success: function(result) {
        if(result == "false" || result == false || result == 0){
          $("#plotArea").html("<div class='col-lg-12' style='backgroundColor'><strong>No data available</strong></div>");
          return;
        }

        var data = JSON.parse(result);

        //var equipments = data['equipments'];
        var details = data['details'];
        var yearlyDetails = data['yearly_avg'];

        /*console.log(details);

        console.log(yearlyDetails);

        return;*/

        /*console.log(data['details']);
        return;*/

        var idle_avg = [];
        var working_avg = [];
        var mainServiceMeter_avg = [];
        var MSMAchieved_hrs = [];
        var equipments = [];

        /* To generate yearly avg gharph - required data is stored in array */
        $.each(yearlyDetails, function(k, v) {
          idle_avg.push(parseFloat(v.idleTime_avg));
          working_avg.push(parseFloat(v.workingTime_avg));
          mainServiceMeter_avg.push(parseFloat(v.mainServiceMeter_avg));
          MSMAchieved_hrs.push(parseFloat(v.MSMAchieved_hrs));
          equipments.push(parseInt(v.equipId));
        });


        var no_of_pages = equipments.length;
  
        var html = "";
        var container = "";
        var newContainer = "";
        var faultDescriptionDiv = "";
        var newChartHeader = "";

        /* To generate no of pages. */
        if (no_of_pages <= 5) {
          for (var i = 1; i <= no_of_pages; i++) {
            html += '<li id="pageNum_' + i + '" style="float: left;width: 50px;"><input type="button" style = "color: white; background:none!important; border:none; padding:0!important; text-decoration:underline; cursor:pointer;" onclick="getPageNo(this,' + no_of_pages + ');" value="' + i + '" /></li>';
            newContainer += '<div id="new_charts' + i + '"></div>';

            newChartHeader += '<div id = "chartHeader'+i+'"></div>';
          }
        } // end of if
        else 
        {
          //default value is 1
          selected_page_value = 1;
            //create first button
          html += '<li style="float: left;width: 50px;"><input type="button" style = "color: white; background:none!important; border:none; padding:0!important; text-decoration:underline; cursor:pointer;" onclick="getPageNo(this,' + no_of_pages + ');" value="First" /></li>';

          //create prev button
          html += '<li style="float: left;width: 50px;"><input type="button" style = "color: white; background:none!important; border:none; padding:0!important; text-decoration:underline; cursor:pointer;" onclick="getPageNo(this,' + no_of_pages + ');" value="Prev" /></li>';

          for (var i = 1; i <= no_of_pages; i++) {
              //create page number button
            if (i <= 5) {
              html += '<li id="pageNum_' + i + '" style="float: left;width: 50px;" class="activePageNum"><input type="button" style = "color: white; background:none!important; border:none; padding:0!important; text-decoration:underline; cursor:pointer;" onclick="getPageNo(this,' + no_of_pages + ');" value="' + i + '" /></li>';
            } else
              html += '<li id="pageNum_' + i + '" style="float: left;width: 50px;" class="inactivePageNum"><input type="button" style = "color: white; background:none!important; border:none; padding:0!important; text-decoration:underline; cursor:pointer;" onclick="getPageNo(this,' + no_of_pages + ');" value="' + i + '" /></li>';

              newContainer += '<div id="new_charts' + i + '"></div>';
              newChartHeader += '<div id = "chartHeader'+i+'"><label style="padding: 0px;float:right;" id="Year" title=""><strong>Year : '+selected_year+'</strong></label></div>';
          } // End of for loop

            //create next button
          html += '<li style="float: left;width: 50px;"><input type="button" style = "color: white; background:none!important; border:none; padding:0!important; text-decoration:underline; cursor:pointe+r;" onclick="getPageNo(this,' + no_of_pages + ');" value="Next" /></li>';

          //create last button
          html += '<li style="float: left;width: 50px;"><input type="button" style = "color: white; background:none!important; border:none; padding:0!important; text-decoration:underline; cursor:poin  ter;" onclick="getPageNo(this,' + no_of_pages + ');" value="Last" /></li>';
        }  // end of else

        $("#pageNo").show();
        $("#pageNo").html(html);  

        $('#chartContainer').html(newContainer);
        $('#chartContainer').css("display", "block");
        
        $("#chartName").html(newChartHeader);
        $('#chartName').css("display", "block");

        $("#chartNameYearly").html('<label style="padding: 0px;float:left;" id="Year" title=""><strong>Year : '+selected_year+'</strong></label>');
        $('#chartNameYearly').css("display", "block");



        /* generate yealy average graph */
        Highcharts.chart("yearly_avg_chart", {
          chart: {
            backgroundColor: '#2d3035',
            type: 'column'
          },
          title: {
            text: 'Average Utilization',
            style: {
              color: 'white',
              fontSize: '16px',
              fontWeight: 'bold'
            }
          },
          xAxis: {
            text: '',
            categories: equipments,
            crosshair: true,
            title: {
              text: 'Equipment',
              style: {
                color: 'white',
              },
              labels: {
                style: {
                  color: 'white'
                },
              },
            }
          },
          yAxis: {
            min: 0,
            allowDecimals: false,
            color: 'white',
            title: {
              text: 'Time (hours)',
              color: 'white'
            },
            labels: {
              style: {
                color: 'white',
              }
            }
          },
          tooltip: {
            headerFormat: '<span style="font-size:12px;color:#FF4500">Equipment id : {point.key}</span><table style="color:#FF4500;">',
            pointFormat: '<tr><td style="padding:0;font-size:12px;color:#FF4500">{series.name}: </td>' +
                '<td style="padding:0;color:#FF4500"><b>{point.y} hrs</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
          },
          plotOptions: {
            column: {
              pointPadding: 0.2,
              borderWidth: 0
            },
            series: {
              borderWidth: 0,
              borderColor: 'black'
            }
          },
          legend: {
            itemStyle: {
              color: '#fff'
            }
          },
          series: [{
            name: 'Main service meter',
            data: mainServiceMeter_avg,
            color: "#f8a13f"
          },
          {
            name: 'Working time',
            data: working_avg,
            color: "#55ce63"
          },
          {
            name: 'Idle time',
            data: idle_avg,
            color: "red"
          }]
        }); /* end of chart */


        for (i = 1; i <= equipments.length; i++) {
        //Get MSM achieved hrs for equipments[i]
          var MSMAchieved_hours = ''
          for(j=0; j< yearlyDetails.length;j++)
          {
            if(yearlyDetails[j]["equipmentId"] == equipments[i-1])
            {
              MSMAchieved_hours = yearlyDetails[j]["MSMAchieved_hrs"];
              break;
            }
          }

          chartName = "new_charts" + i;

          var labeldisplay = "";
          labeldisplay += '<div class="col-lg-12" style="padding : 0px;">';
          labeldisplay += '<div class="col-lg-12" ><label style="padding: 0px;float:left;" id="Equipment" title=""><strong>Equipment Id : '+equipments[i - 1]+'</strong></label>'; 
          labeldisplay += '<label style="padding: 0px;float:right;" id="Year" title=""><strong>Year : '+selected_year+'</strong></label></div> '; 
          labeldisplay += '<br /><div class="col-lg-12" ><label style="padding: 0px;float:left;" id="Equipment" title=""><strong>Main Service meter achieved till '+selected_year+' : '+MSMAchieved_hours+' hrs</strong></label></div>'; 

          $("#chartHeader"+i).html(labeldisplay);
        
          /* Initially hide all charts */
          $("#new_charts" + i).css("display", "none");
          $("#chartHeader"+i).css("display", "none");

          var idleTimeArray = [];
          var workingTimeArray = [];
          var mainServiceMeterArray = [];
          var monthArray = [];

          /* To generate absolute values graph*/
          $.each(details, function(k, v) {
            /*if(v.equipId == equipments[i -1]){*/
            if(v.eqId == equipments[i -1]){
              /*idleTimeArray.push(parseFloat(v.idleTime));
              workingTimeArray.push(parseFloat(v.workingTime));
              mainServiceMeterArray.push(parseFloat(v.mainServiceMeter));

              monthArray.push(v.monthYear);*/

              idleTimeArray.push(parseFloat(v.idleTime_hrs));
              workingTimeArray.push(parseFloat(v.workingTime_hrs));
              mainServiceMeterArray.push(parseFloat(v.mainServiceMeter_hrs));

              monthArray.push(v.monthYear);
            }

            Highcharts.chart(chartName, {
              chart: {
                backgroundColor: '#2d3035',
                type: 'column'
              },
              title: {
                text: 'Monthly Utilization',
                style: {
                  color: 'white',
                  fontSize: '16px',
                  fontWeight: 'bold'
                }
              },
              xAxis: {
                text: '',
                categories: monthArray,
                crosshair: true,
                title: {
                  text: 'Months',
                  style: {
                    color: 'white',
                  },
                  labels: {
                    style: {
                      color: 'white'
                    },
                  },
                }
              },
              yAxis: {
                min: 0,
                allowDecimals: false,
                color: 'white',
                title: {
                  text: 'Time (hours)',
                  color: 'white'
                },
                labels: {
                  style: {
                    color: 'white',
                  }
                }
              },
              tooltip: {
                  headerFormat: '<span style="font-size:12px;color:#FF4500">{point.key}</span><table style="color:#FF4500">',
                  pointFormat: '<tr><td style="padding:0;font-size:12px;color:#FF4500">{series.name}: </td>' +
                      '<td style="padding:0;color:#FF4500"><b>{point.y} hrs</b></td></tr>',
                  footerFormat: '</table>',
                  shared: true,
                  useHTML: true
              },
              
              plotOptions: {
                column: {
                  pointPadding: 0.2,
                  borderWidth: 0
                },
                series: {
                  borderWidth: 0,
                  borderColor: 'black'
                }
              },
              legend: {
                itemStyle: {
                  color: '#fff'
                }
              },
              series: [{
                name: 'Main service meter',
                data: mainServiceMeterArray,
                color: "#f8a13f"
              },
              {
                name: 'Working time',
                data: workingTimeArray,
                color: "#55ce63"
              },
              {
                name: 'Idle time',
                data: idleTimeArray,
                color: "red"
              }]
            }); /* end of chart */
          }); /* end of $.each */
        } /* end of for */
        $("#datatable-checkbox").html(html);
      }, /* End of suucess */
      error: function(xhr, status, error) {
        alert("Error occured while getting data! Please try again.");
      },
      complete: function() {
        $('#loadimage').hide();
        $(".overlay").remove();
        $("#new_charts1").css("display", "block");
        $("#chartHeader1").css("display", "block");
      } /* end of complete */
    });
  });
  
  /* display site table on click of this button */
  $("#selected_site_btn_back").on("click",function(){
    $("#selected_site_btn_back").hide();
    $("#selected_dept_btn_back").hide();
    $("#selected_equip_btn_back").hide();

    $("#selected_site_btn").show();
    $("#selected_equip_btn").hide();
    $("#selected_dept_btn").hide();

    $("#siteTable").show();
    
    $("#deptTable_div").hide();

    $("#reset_all").show();
  });

  $("#selected_dept_btn_back").on("click",function(){

    $("#selected_site_btn_back").show();
    $("#selected_dept_btn_back").hide();
    $("#selected_equip_btn_back").hide();

    $("#selected_site_btn").hide();
    $("#selected_equip_btn").hide();
    $("#selected_dept_btn").show();

    $("#siteTable").hide();
    $("#deptTable_div").show();
    $("#equipTable_div").hide();

    $("#reset_all").show();
  });

  $("#selected_equip_btn_back").on("click",function(){
    $("#equipTable_div").show();

    $("#plotArea").hide();
                    
    /* display next and back button */
    $("#selected_equip_btn").show();
    $("#selected_dept_btn_back").show();

    $("#selected_equip_btn_back").hide();

    $("#reset_all").show();
  });

  $("#show_absolute_details").on("click",function(){
    $("#yearly_avg").hide();
    $("#utilization_absolute").show();
    $("#show_avg").show();
    $("#show_absolute_details").hide();
  });

  $("#show_avg").on("click",function(){
    $("#yearly_avg").show();
    $("#utilization_absolute").hide();
    $("#show_avg").hide();
    $("#show_absolute_details").show();
  });

}); /* End of document.ready */

//Show data of 1 Equipment at a time - function copied from dtc.js
function getPageNo(obj, no_of_pages) {
    var action = 1;
    var page = obj.getAttribute("value");

    if (page == 'Prev') {
      if ((selected_page_value - 1) >= 1) {
        selected_page_value = selected_page_value - 1;
        page = selected_page_value;
        action = 1;

        if (!$("#pageNum_" + page).hasClass("activePageNum")) {
          $("#pageNum_" + (page)).addClass("activePageNum").removeClass("inactivePageNum");
          $("#pageNum_" + (page + 5)).addClass("inactivePageNum").removeClass("activePageNum");
        }
      } 
      else
        action = 0;
    }
    else if (page == 'Next') {
      if ((selected_page_value + 1) <= no_of_pages) {
        selected_page_value = selected_page_value + 1;
        page = selected_page_value;
        action = 1;

        if (!$("#pageNum_" + page).hasClass("activePageNum")) {
          $("#pageNum_" + (page)).addClass("activePageNum").removeClass("inactivePageNum");
          $("#pageNum_" + (page - 5)).addClass("inactivePageNum").removeClass("activePageNum");
        }
      } 
      else
        action = 0;
    } 
    else if (page == 'First') {
      //show first 5 page numbers 
      for (var i = 1;
        (i <= no_of_pages); i++) {
        if (i <= 5)
          $("#pageNum_" + i).addClass("activePageNum").removeClass("inactivePageNum");
        else
          $("#pageNum_" + i).addClass("inactivePageNum").removeClass("activePageNum");
      }

      selected_page_value = 1;
      page = selected_page_value;
      action = 1;
    } 
    else if (page == 'Last') {
        //show last 5 page numbers
        for (var i = 1;(i <= no_of_pages); i++) 
        {
          if (i > (no_of_pages - 5))
            $("#pageNum_" + i).addClass("activePageNum").removeClass("inactivePageNum");
          else
            $("#pageNum_" + i).addClass("inactivePageNum").removeClass("activePageNum");
        }

        selected_page_value = no_of_pages;
        page = selected_page_value;
        action = 1;
    } 
    else
      selected_page_value = parseInt(page);

    for (var i = 1; i <= no_of_pages; i++) {
      if (i == page && action == 1) {
        $("#graphArea" + i).css("display", "block");
        $("#new_charts" + i).css("display", "block");
        $("#faultDescription" + i).css("display", "block");
        $("#pageNum_" + i + " a").addClass("underline");
        $("#chartHeader"+i).css("display","block");
      } 
      else {
        if (action == 1) {
          $("#graphArea" + i).css("display", "none");
          $("#new_charts" + i).css("display", "none");
          $("#faultDescription" + i).css("display", "none");
          $("#chartHeader"+i).css("display","none");
          $("#pageNum_" + i + " a").removeClass("underline");
        }
      }
    }
} /* end of page no function */

