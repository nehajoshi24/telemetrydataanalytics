var selected_page_value;
var no_of_pages = 0;
$(document).ready(function() {
    var base_url = document.getElementById("hdnBaseUrl").value;

    $("#check_dateRange").on("change", function() {
        if ($(this).is(':checked')) {
            $("#thirdRow").show();
        } else {
            $("#thirdRow").hide();
            $("#toDate").val("");
        }
    });

    //hide the graph
    $('#chartArea').css("display", "none");
    $("#faultDescription").css("display", "none");

    $(function() {
        $("#fromDate").datepicker();
        $("#toDate").datepicker();
    });

    $("#thirdRow").hide();
    $("#charts").html("");
    $("#uCode").hide();
    $("#tType").hide();
    $("#loadimage").hide();
    $("#set_truckwise").hide();
    $("#div_truckwise").hide();
    $("#div_dtcwise").hide();
    $("#export_excel").hide();
    $("#print").hide();
    $("#div_NoData").css("display", "none");

    var label = "";
    var className = "";
    var BARCHARTEXMPLE;
    var date = "";

    var fromDate;
    var toDate;
    var method;
    var isCustom;

    $("#bigtruckType").on("change", function() {
        $("#export_excel").hide();
        $("#print").hide();

        /*$('#criticalFault').attr('checked', false);
        $('#loggingFault').attr('checked', false);
        $('#shutdownFault').attr('checked', false);*/

        $('#criticalFault').prop('checked', false);
        $('#loggingFault').prop('checked', false);
        $('#shutdownFault').prop('checked', false);

        if($("#bigtruckType").val() == 3){
            $("#shutdownFault").removeAttr('disabled');
        }
        else{
            $("#shutdownFault").attr('disabled','true');   
        }
    });

    /* On change of class name hide/show unit code/subsytem */
    $("#ClassName").on("change", function() {
        $("#print_table").html("");
        $("#pageNo").hide();
        $("#chartName").hide();
        $("#chartContainer").css("display", "none");
        $("#chartArea").css("display", "none");
        $("#faultDescription").css("display", "none");
        $("#div_dtcwise").hide();

        $("#export_excel").hide();
        $("#print").hide();

        /*$('#criticalFault').attr('checked', false);
        $('#loggingFault').attr('checked', false);
        $('#shutdownFault').attr('checked', false);*/

        $('#criticalFault').prop('checked', false);
        $('#loggingFault').prop('checked', false);
        $('#shutdownFault').prop('checked', false);

        className = $('#ClassName').find(":selected").val();
        if (className == 0) {
            $("#uCode").hide();
            $("#tType").hide();
            return;
        }

        $("#unitCodeSelect").html("");

        if (className != 'bigTruck') {
            $('#graphArea').html('');
            $("#uCode").show();
            $("#tType").hide();
            if(className == 'classI' || className == 'classII' || className == "classIII"){
                $("#shutdownFault").attr('disabled','true');
            }
            else{
                $("#shutdownFault").removeAttr('disabled');
            }
            //ajax call to get unit codes
            $.ajax({
                beforeSend: function() {
                    //Fade whole page
                    $("body").prepend("<div class='overlay'></div>");

                    $(".overlay").css({
                        "position": "absolute",
                        "width": $(document).width(),
                        "height": $(document).height(),
                        "z-index": 9999,
                    }).fadeTo(0, 0.8);

                    //show loader  
                    $("#loadimage").css("z-index", "99999").show();
                },
                type: "POST",
                url: base_url + "dtcController/GetUnitCodes",
                async: true,
                contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
                data: {
                    className: className,
                    /*method: isCustom,
                    fromDate: fromDate,
                    toDate: toDate*/
                },
                success: function(result) {
                    var data = JSON.parse(result);
                    var html = "";
                    html += "<option value ='All'>Select all</option>"
                    $.each(data, function(k, v) {
                        html += "<option value=" + v.idUnitCode + " unitCodeName=" + v.unitCodeName + ">" + v.unitCodeName + "</option>";
                    })
                    $("#unitCodeSelect").html(html);
                },
                error: function(xhr, status, error) {
                    alert("Error occured while getting unit codes! Please try again.");
                },
                complete: function() {
                    $('#loadimage').hide();
                    $(".overlay").remove();
                }
            });
        } else if (className == 'bigTruck') {
            $("#uCode").hide();
            $("#tType").show();
            var html = "";
            html += "<option value = 0>Select type</option><option value = 1>Hydraulic</option><option value = 2>Transmission</option><option value = 3>Others</option>";
            $("#bigtruckType").html(html);

        } else {
            $("#uCode").hide();
            $("#tType").hide();
        }
    }); 

    //Define a function on click of button 'Generate Chart'
    $("#set").on("click", function() {
       // var fromDateCheck = "" ; var toDateCheck = "";
        if ($("#from_hrMeter").val() == "" || $("#to_hrMeter").val() == "") {
            alert("Please enter main service meter range.");
            return;
        }
        
        /* To check if date is selected if date checkbox is checked */
        toDate = $("#toDate").val();
        if ($('#check_dateRange').is(':checked')) {
            if(toDate == ""){
                alert("Please select date");
                return;
            }
        }

        var fromDateCheck = parseFloat($("#from_hrMeter").val());
        var toDateCheck = parseFloat($("#to_hrMeter").val());

        if(fromDateCheck > toDateCheck) {
            alert("To main service meter value should be greater than from main service meter value.");
            return;
        }

        $("#print_table").html("");
        $('#graphArea').html("");
        $('#completeGraphArea').css("display", "none");
        $("#pageNo").hide();
        $("#chartName").show();

        $("#div_dtcwise").show();
        className = $('#ClassName').find(":selected").val();

        ///Class name is mandatory
        if (className == "0" || className == 0) {
            alert("Please select class.");
            return;
        }

        $("#export_excel").show();
        $("#print").show();

        var fault_flags = "";
        if($('#criticalFault').is(':checked')){
            fault_flags = "critical,";
        }
        if($('#loggingFault').is(':checked')){
            fault_flags += "NULL,"
        }
        if($('#shutdownFault').is(':checked')){
            fault_flags +="Shutdown"
        }

        if (className != "bigTruck") {
            var idUnitCode = $("#unitCodeSelect").val();

            //by default, All is considered as selected
            if (idUnitCode == "" || idUnitCode == null) {
                var idUnitCode = ["All"];
            }

            if (idUnitCode == 0 || idUnitCode == "0") {
                alert("Please select unit code.");
                return;
            } else {
                if($('#criticalFault').is(':checked') != true && $('#loggingFault').is(':checked') != true){
                    if($('#shutdownFault').attr('disabled')){
                        $("#export_excel").hide();
                        $("#print").hide();
                        alert("Please select at least one type of fault.");
                        return;
                    }
                    else if($('#shutdownFault').is(':checked') != true){
                        $("#export_excel").hide();
                        $("#print").hide();
                        alert("Please select at least one type of fault.");
                        return;
                    }
                }

                var unitCodeName = "";

                var len = $("#unitCodeSelect").find(':selected').length;
                var unitCodes_arr = [];
                var unitCodeName_tooltip = ""
                for (i = 0; i < len; i++) {
                    unitCodes_arr.push($("#unitCodeSelect").find(':selected')[i].innerHTML);
                }

                if(unitCodes_arr.indexOf('Select all') || len == 0){
                //if (unitCodes_arr.includes("Select all") || len == 0) {
                    unitCodeName = 'All';
                    unitCodeName_tooltip = "All";
                } else {
                    if (len <= 9) {
                        unitCodeName = unitCodes_arr.join(',');
                    } else {
                        unitCodeName = unitCodes_arr.slice(0, 8).join(',');
                        unitCodeName += ',...';
                    }
                    unitCodeName_tooltip = unitCodes_arr.join(',');
                }

                var from_hrMeter = $("#from_hrMeter").val();
                var to_hrMeter = $("#to_hrMeter").val();
                var className = $('#ClassName').find(":selected").val();

                $("#chartName").html("");

                //ajax call to get DTC data
                $.ajax({
                    beforeSend: function() {
                        //Fade whole page
                        $("body").prepend("<div class='overlay'></div>");

                        $(".overlay").css({
                            "position": "absolute",
                            "width": $(document).width(),
                            "height": $(document).height(),
                            "z-index": 9999,
                        }).fadeTo(0, 0.8);
                        //show loader  
                        $("#loadimage").css("z-index", "99999").show();
                    },
                    type: "POST",
                    url: base_url + "dtcController/getUnitCodeDTCData",
                    async: true,
                    contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
                    data: {
                        className_unitCode: className,
                        idUnitCode: idUnitCode,
                        toDate: toDate,
                        from_hrMeter: from_hrMeter,
                        to_hrMeter: to_hrMeter,
                        faults : fault_flags
                    },
                    success: function(result) {
                        if (result == "false") {
                            $("#div_NoData").css("display", "block");
                            $('#chartArea').css("display", "none");
                            $("#faultDescription").html("");
                            $("#export_excel").hide();
                            $("#print").hide();
                            return;
                        }
                        $("#div_NoData").css("display", "none");
                        $("#chartArea").css("display", "block");
                        $("#faultDescription").css("display", "block");
                        
                        $('#completeGraphArea').css("display", "block");
                        $("#faultDescription").css("display", "block");

                        var labeldisplay, l;
                        var data = JSON.parse(result);
                        var label = new Array();
                        var dataSet = new Array();
                        var errorDescription = [];

                        var count = 0;

                        var classFaultCount = data.totalCount;
                        var totalEquipmentCount = data.totalEquipmentCount;

                        $("#exportData").val(data);

                        $.each(data, function(k, v) {
                            l = v;
                            if (v.errorCode) {
                                label.push(v.errorCode);
                                dataSet.push(v.errorCount);
                                errorDescription.push(v.errorDescription);
                            }
                            count++;
                        });

                        if (count == 0) {
                            // var container = "";
                            // container = "<div class='col-lg-12'><label class='errorMsg'>No data available.</label></div> ";
                            $("#div_NoData").css("display", "block");
                            $('#chartArea').css("display", "none");
                            $("#faultDescription").html("");
                            $("#export_excel").hide();
                            $("#print").hide();
                            return;
                        } else {
                            if (count <= 10) {
                                no_of_pages = 1;
                            } else {
                                no_of_pages = (count - 2) / 10;
                                no_of_pages = Math.ceil(no_of_pages);
                            }
                            var html = "";
                            var container = "";
                            var newContainer = "";
                            var faultDescriptionDiv = "";


                            if (no_of_pages <= 5) {
                                for (var i = 1; i <= no_of_pages; i++) {
                                    html += '<li id="pageNum_' + i + '" style="float: left;width: 50px;"><input type="button" style = "color: white; background:none!important; border:none; padding:0!important; text-decoration:underline; cursor:pointer;" onclick="getPageNo(this,' + no_of_pages + ');" value="' + i + '" /></li>';

                                    newContainer += '<div id="new_charts' + i + '"></div>';
                                    faultDescriptionDiv += '<div id="faultDescription' + i + '" style="float: left;width:  100%;color:  white;" ></div>';
                                }
                            } else {
                                //default value is 1
                                selected_page_value = 1;
                                //create first button
                                html += '<li style="float: left;width: 50px;"><input type="button" style = "color: white; background:none!important; border:none; padding:0!important; text-decoration:underline; cursor:pointer;" onclick="getPageNo(this,' + no_of_pages + ');" value="First" /></li>';

                                //create prev button
                                html += '<li style="float: left;width: 50px;"><input type="button" style = "color: white; background:none!important; border:none; padding:0!important; text-decoration:underline; cursor:pointer;" onclick="getPageNo(this,' + no_of_pages + ');" value="Prev" /></li>';

                                for (var i = 1; i <= no_of_pages; i++) {
                                    //create page number button
                                    if (i <= 5) {
                                        html += '<li id="pageNum_' + i + '" style="float: left;width: 50px;" class="activePageNum"><input type="button" style = "color: white; background:none!important; border:none; padding:0!important; text-decoration:underline; cursor:pointer;" onclick="getPageNo(this,' + no_of_pages + ');" value="' + i + '" /></li>';
                                    } else
                                        html += '<li id="pageNum_' + i + '" style="float: left;width: 50px;" class="inactivePageNum"><input type="button" style = "color: white; background:none!important; border:none; padding:0!important; text-decoration:underline; cursor:pointer;" onclick="getPageNo(this,' + no_of_pages + ');" value="' + i + '" /></li>';


                                    newContainer += '<div id="new_charts' + i + '"></div>';
                                    faultDescriptionDiv += '<div id="faultDescription' + i + '" style="float: left;width:  100%;color:  white;" ></div>';
                                }

                                //create next button
                                html += '<li style="float: left;width: 50px;"><input type="button" style = "color: white; background:none!important; border:none; padding:0!important; text-decoration:underline; cursor:pointer;" onclick="getPageNo(this,' + no_of_pages + ');" value="Next" /></li>';

                                //create last button
                                html += '<li style="float: left;width: 50px;"><input type="button" style = "color: white; background:none!important; border:none; padding:0!important; text-decoration:underline; cursor:pointer;" onclick="getPageNo(this,' + no_of_pages + ');" value="Last" /></li>';
                            }


                            $("#pageNo").show();
                            $("#pageNo").html(html);
                            $("ul li#pageNum_1 > a").addClass("underline");

                            $('#chartContainer').html(newContainer);
                            $('#chartContainer').css("display", "block");
                            $("#div_NoData").css("display", "none");

                            $('#faultDescription').html(faultDescriptionDiv);

                            labeldisplay = '<div class="col-lg-12" style="padding : 0px;">' +
                                '<div class="col-lg-5" style="padding: 0px;float:left;margin-right:20px;">' +
                                '<label><strong>Class : ' + className + '</strong></label>' +
                                '</div>' +
                                '<div class="col-lg-6" style="padding-right: 20px;float:right;text-align:right;">' +
                                '<label><strong>Equipment with Critical Faults : ' + classFaultCount + '</strong></label>' +
                                '</div>' +
                                '</div>';

                            labeldisplay += '<div class="col-lg-12" style="padding : 0px;">' +
                                '<div class="col-lg-6" style="padding: 0px;float:left;" id="div_selectedUnitCode" title=' + unitCodeName_tooltip + '>' +
                                '<label><strong>Unit Code : ' + unitCodeName + '</strong></label>' +
                                '</div>' +
                                '<div class="col-lg-6" style="padding-right: 20px;float:right;text-align:right;">' +
                                '<label><strong>Total Equipment : ' + totalEquipmentCount + '</strong></label>' +
                                '</div>' +
                                '</div>';

                            $("#chartName").html(labeldisplay);
                            $("#chartName").css("display", "block");

                            for (i = 1; i <= no_of_pages; i++) {
                                var newLabel = [];
                                var newData = [];
                                var tbody = "";
                                chartName = "new_charts" + i;
                                tableId = "descriptionTable" + i;
                                $("#new_charts" + i).css("display", "none");

                                tbody += '<table id="' + tableId + '" border="1px solid white;" style="box-shadow: 2px 2px 7px 0px;border: 1px solid #ffffff;"><thead style="text-align:  center;"><tr style="height:  50px;"><th style="width: 5%;color:  white;">Sr No</th><th style="width: 15%;color:  white;">Error Code</th><th style="width: 30%;color:  white;">Description</th></tr></thead><tbody style="text-align:  center;" id="tbody"' + i + '>';
                                var k = 0;
                                if (i == 1) {
                                    for (j = 0; j < 10; j++) {
                                        if (label[j] !== void 0 && dataSet[j] !== void 0) {
                                            k = j + 1;
                                            newLabel.push(label[j]);
                                            newData.push(parseInt(dataSet[j]));
                                            tbody += '<tr style="text-align:  center;height:50px;"><td style="color:white;">' + k + '</td><td style="color:  white;">' + label[j] + '</td><td style="color:  white;">' + errorDescription[j]; + '</td></tr>';
                                        }
                                    }
                                } else if (i < no_of_pages) {
                                    for (j = (i * 10) - 10; j < (i * 10); j++) {
                                        if (label[j] !== void 0 && dataSet[j] !== void 0) {
                                            k = j + 1;
                                            newLabel.push(label[j]);
                                            newData.push(parseInt(dataSet[j]));
                                            tbody += '<tr style="text-align:  center;height:50px;"><td style="color:  white;">' + k + '</td><td style="color:  white;">' + label[j] + '</td><td style="color:  white;">' + errorDescription[j]; + '</td></tr>';
                                        }
                                    }
                                } else if (i == no_of_pages) {
                                    for (j = (i * 10) - 10; j <= (i * 10); j++) {
                                        if (label[j] !== void 0 && dataSet[j] !== void 0) {
                                            k = j + 1;
                                            newLabel.push(label[j]);
                                            newData.push(parseInt(dataSet[j]));
                                            tbody += '<tr style="text-align:  center;height:50px;"><td style="color:  white;">' + k + '</td><td style="color:  white;">' + label[j] + '</td><td style="color:  white;">' + errorDescription[j]; + '</td></tr>';
                                        }
                                    }
                                }
                                tbody += '</tbody>';

                                var printtable = "";
                                printtable += '<table id="printtable" border="1px solid black;" style="box-shadow: 2px 2px 7px 0px;border: 1px solid #ffffff;page-break-after:always;><thead style="text-align:  center;"><tr style="height:  50px;"><th style="width: 5%;color:  black;">Sr No</th><th style="width: 15%;color:  black;">Error Code</th><th style="width: 30%;color:  black;">Description</th><th style="width: 30%;color:  black;">Equipment Count</th></tr></thead><tbody style="text-align:  center;" id="printtable_tbody ">';

                                for (var n = 0; n < dataSet.length; n++) {
                                    k = n + 1;
                                    printtable += '<tr style="text-align:  center;height:50px;"><td style="color:  black;">' + k + '</td><td style="color:  black;">' + label[n] + '</td><td style="color:  black;">' + errorDescription[n] + '</td><td style="color:  black;">' + dataSet[n] + '</td></tr>';
                                }

                                printtable += "</tbody></table>";

                                $("#print_table").html(printtable);

                                //console.log(tbody);
                                $("#faultDescription" + i).html(tbody);
                                $("#faultDescription" + i).css("display", "none");
                                /* display Chart */
                                Highcharts.chart(chartName, {
                                    chart: {
                                        backgroundColor: '#2d3035',
                                        type: 'column'
                                    },
                                    title: {
                                        text: 'Equipment Count With DTC',
                                        style: {
                                            color: 'white',
                                            fontSize: '16px',
                                            fontWeight: 'bold'
                                        }
                                    },
                                    xAxis: {
                                        text: 'Error Code',
                                        categories: newLabel,
                                        crosshair: true,
                                        title: {
                                            text: 'Error Code',
                                            style: {
                                                color: 'white',
                                            },
                                            labels: {
                                                style: {
                                                    color: 'white'
                                                },
                                            },
                                        }
                                    },
                                    yAxis: {
                                        min: 0,
                                        allowDecimals: false,
                                        color: 'white',
                                        title: {
                                            text: 'No of Equipment',
                                            color: 'white'
                                        },
                                        labels: {
                                            style: {
                                                color: 'white',
                                            }
                                        }
                                    },
                                    tooltip: {
                                        headerFormat: '<span style="font-size:12px">{point.key}</span><table>',
                                        pointFormat: '<tr><td style="padding:0;font-size:12px">{series.name}: </td>' +
                                            '<td style="padding:0"><b>{point.y} </b></td></tr>',
                                        footerFormat: '</table>',
                                        shared: true,
                                        useHTML: true
                                    },
                                    plotOptions: {
                                        column: {
                                            pointPadding: 0.2,
                                            borderWidth: 0
                                        },
                                        series: {
                                            borderWidth: 0,
                                            borderColor: 'black'
                                        }
                                    },
                                    legend: {
                                        itemStyle: {
                                            color: '#fff'
                                        }
                                    },
                                    series: [{
                                        name: 'Equipment Count with DTC',
                                        data: newData,
                                        color: "#55ce63"
                                    }]
                                });
                            }
                        }
                    },
                    error: function(xhr, status, error) {
                        alert("Error while loading graph. Please try again.");
                    },
                    complete: function() {
                        $("#new_charts1").css("display", "block");
                        $("#faultDescription1").css("display", "block");
                        $('#loadimage').hide();
                        $(".overlay").remove();
                    }
                }); //end of ajax call
            }
        } 
        else if (className == "bigTruck") {
            var btype = $('#bigtruckType').find(":selected").val();
            if (btype == 0) {
                alert("Please select subsystem type for big truck.");
                $("#export_excel").hide();
                $("#print").hide();
                return;
            } else {
                if($('#criticalFault').is(':checked') != true && $('#loggingFault').is(':checked') != true){
                    if($('#shutdownFault').attr('disabled')){
                        $("#export_excel").hide();
                        $("#print").hide();
                        alert("Please select at least one type of fault.");
                        return;
                    }
                    else if($('#shutdownFault').is(':checked') != true){
                        $("#export_excel").hide();
                        $("#print").hide();
                        alert("Please select at least one type of fault.");
                        return;
                    }
                }

                var btype = $('#bigtruckType').find(":selected").val();
                var from_hrMeter = $("#from_hrMeter").val();
                var to_hrMeter = $("#to_hrMeter").val();

                $("#loadimage").show();
                $("#chartName").html("");
                $.ajax({
                    beforeSend: function() {
                        //Fade whole page
                        $("body").prepend("<div class='overlay'></div>");

                        $(".overlay").css({
                            "position": "absolute",
                            "width": $(document).width(),
                            "height": $(document).height(),
                            "z-index": 9999,
                        }).fadeTo(0, 0.8);

                        //show loader  
                        $("#loadimage").css("z-index", "99999").show();
                    },
                    type: "POST",
                    url: base_url + "dtcController/GetBigTruckDTCData",
                    data: {
                        className: className,
                        btype: btype,
                        toDate: toDate,
                        from_hrMeter: from_hrMeter,
                        to_hrMeter: to_hrMeter,
                        fault_flags : fault_flags
                    },
                    success: function(result) {
                        //no data 
                        if (result == "false") {
                            $("#div_NoData").css("display", "block");
                            $('#chartArea').css("display", "none");
                            $("#faultDescription").html("");
                            $("#export_excel").hide();
                            $("#print").hide();
                            return;
                        }
                        $("#div_NoData").css("display", "none");
                        $("#chartArea").css("display", "block");
                        $("#faultDescription").css("display", "block");
                        //data available
                        $('#completeGraphArea').css("display", "block");
                        if (btype == '1') {
                            var t = "Hydraulic";
                            date = fromDate + " To " + toDate;
                        }
                        if (btype == '2') {
                            var t = "Transmission";
                            date = fromDate + " To " + toDate;
                        }
                        if (btype == '3') {
                            var t = "Others (Engine,Spreader etc)";
                            date = fromDate + " To " + toDate;
                        }
                        var labeldisplay, l;
                        var data = JSON.parse(result);
                        var label = new Array();
                        var dataSet = new Array();
                        var errorDescription = [];
                        var count = 0;

                        var classFaultCount = data.totalCount;
                        var totalEquipmentCount = data.totalEquipmentCount;
                        if (btype == '1') {
                            $.each(data, function(k, v) {
                                l = v;
                                if (!v.totalCount) {
                                    var errorCode = v.errorCode;
                                    label.push(errorCode);
                                    dataSet.push(v.errorCount);
                                    errorDescription.push(v.errorDescription);
                                }
                                count++;
                            })
                        } else if (btype == '3') {
                            $.each(data, function(k, v) {
                                l = v;
                                if (!v.totalCount) {
                                    var errorCode = v.errorCode;
                                    label.push(errorCode);
                                    dataSet.push(v.errorCount);
                                    errorDescription.push(v.errorDescription);
                                }
                                count++;
                            })
                        } else {
                            $.each(data, function(k, v) {
                                l = v;
                                if (!v.totalCount) {
                                    label.push(v.errorCode);
                                    dataSet.push(v.errorCount);
                                    errorDescription.push(v.errorDescription);
                                }
                                count++;
                            })
                        }

                        if (classFaultCount == 0) {
                            var container = "<div class='col-lg-12'><label><strong>Data Not Available</strong></label></div>"
                        } else {
                            if (count <= 10) {
                                no_of_pages = 1;
                            } else {
                                no_of_pages = (count - 2) / 10;
                                no_of_pages = Math.ceil(no_of_pages);
                            }

                            var html = "";
                            var container = "";
                            var newContainer = "";
                            var faultDescriptionDiv = "";

                            if (no_of_pages <= 5) {
                                for (var i = 1; i <= no_of_pages; i++) {
                                    html += '<li id="pageNum_' + i + '" style="float: left;width: 50px;"><input type="button" style = "color: white; background:none!important; border:none; padding:0!important; text-decoration:underline; cursor:pointer;" onclick="getPageNo(this,' + no_of_pages + ');" value="' + i + '" /></li>';

                                    newContainer += '<div id="new_charts' + i + '"></div>';
                                    faultDescriptionDiv += '<div id="faultDescription' + i + '" style="float: left;width:  100%;color:  white;" ></div>';
                                }
                            } else {
                                //default value is 1
                                selected_page_value = 1;
                                //create first button
                                html += '<li style="float: left;width: 50px;"><input type="button" style = "color: white; background:none!important; border:none; padding:0!important; text-decoration:underline; cursor:pointer;" onclick="getPageNo(this,' + no_of_pages + ');" value="First" /></li>';

                                //create prev button
                                html += '<li style="float: left;width: 50px;"><input type="button" style = "color: white; background:none!important; border:none; padding:0!important; text-decoration:underline; cursor:pointer;" onclick="getPageNo(this,' + no_of_pages + ');" value="Prev" /></li>';

                                for (var i = 1; i <= no_of_pages; i++) {
                                    //create page number button
                                    if (i <= 5) {
                                        html += '<li id="pageNum_' + i + '" style="float: left;width: 50px;" class="activePageNum"><input type="button" style = "color: white; background:none!important; border:none; padding:0!important; text-decoration:underline; cursor:pointer;" onclick="getPageNo(this,' + no_of_pages + ');" value="' + i + '" /></li>';
                                    } else
                                        html += '<li id="pageNum_' + i + '" style="float: left;width: 50px;" class="inactivePageNum"><input type="button" style = "color: white; background:none!important; border:none; padding:0!important; text-decoration:underline; cursor:pointer;" onclick="getPageNo(this,' + no_of_pages + ');" value="' + i + '" /></li>';

                                    newContainer += '<div id="new_charts' + i + '"></div>';
                                    faultDescriptionDiv += '<div id="faultDescription' + i + '" style="float: left;width:  100%;color:  white;" ></div>';
                                }

                                //create next button
                                html += '<li style="float: left;width: 50px;"><input type="button" style = "color: white; background:none!important; border:none; padding:0!important; text-decoration:underline; cursor:pointer;" onclick="getPageNo(this,' + no_of_pages + ');" value="Next" /></li>';

                                //create last button
                                html += '<li style="float: left;width: 50px;"><input type="button" style = "color: white; background:none!important; border:none; padding:0!important; text-decoration:underline; cursor:pointer;" onclick="getPageNo(this,' + no_of_pages + ');" value="Last" /></li>';
                            }

                            $("#pageNo").show();
                            $("#pageNo").html(html);
                            $("ul li#pageNum_1 > a").addClass("underline");

                            $('#chartContainer').html(newContainer);
                            $('#chartContainer').css("display", "block");
                            $('#faultDescription').html(faultDescriptionDiv);

                            labeldisplay = "<label><strong>Class : Big Truck</strong></label>";
                            labeldisplay += '<div class="col-lg-12" style="padding : 0px;"><div class="col-lg-6" style="padding: 0px;float:left;"><label><strong>Equipment with Critical Faults : ' + classFaultCount + '</strong></label></div><div class="col-lg-6" style="padding-right: 20px;float: right;text-align: right;"><label><strong>Total Equipment : ' + totalEquipmentCount + '</strong></label></div></div>';

                            $("#chartName").html(labeldisplay);

                            for (i = 1; i <= no_of_pages; i++) {
                                var newLabel = [];
                                var newData = [];
                                var tbody = "";
                                chartName = "new_charts" + i;
                                tableId = "descriptionTable" + i;
                                tbody += '<table id="' + tableId + '" border="1px solid white;" style="box-shadow: 2px 2px 7px 0px;border: 1px solid #ffffff;"><thead style="text-align:  center;"><tr style="height:  50px;"><th style="width: 5%;color:  white;">Sr No</th><th style="width: 15%;color:  white;">Error Code</th><th style="width: 30%;color:  white;">Description</th></tr></thead><tbody style="text-align:  center;" id="tbody"' + i + '>';
                                var k = 0;
                                if (i == 1) {

                                    for (j = 0; j < 10; j++) {
                                        if (label[j] !== void 0 && dataSet[j] !== void 0) {
                                            k = j + 1;
                                            newLabel.push(label[j]);
                                            newData.push(parseInt(dataSet[j]));
                                            tbody += '<tr style="text-align:  center;height:50px;"><td style="color:white;">' + k + '</td><td style="color:  white;">' + label[j] + '</td><td style="color:  white;">' + errorDescription[j]; + '</td></tr>';
                                        }
                                    }
                                } else if (i < no_of_pages) {
                                    for (j = (i * 10) - 10; j < (i * 10); j++) {
                                        if (label[j] !== void 0 && dataSet[j] !== void 0) {
                                            k = j + 1;
                                            newLabel.push(label[j]);
                                            newData.push(parseInt(dataSet[j]));
                                            tbody += '<tr style="text-align:  center;height:50px;"><td style="color:white;">' + k + '</td><td style="color:  white;">' + label[j] + '</td><td style="color:  white;">' + errorDescription[j]; + '</td></tr>';
                                        }
                                    }
                                } else if (i == no_of_pages) {
                                    for (j = (i * 10) - 10; j <= (i * 10); j++) {
                                        if (label[j] !== void 0 && dataSet[j] !== void 0) {
                                            k = j + 1;
                                            newLabel.push(label[j]);
                                            newData.push(parseInt(dataSet[j]));
                                            tbody += '<tr style="text-align:  center;height:50px;"><td style="color:white;">' + k + '</td><td style="color:  white;">' + label[j] + '</td><td style="color:  white;">' + errorDescription[j]; + '</td></tr>';
                                        }
                                    }
                                }
                                tbody += '</tbody>';

                                var printtable = "";
                                printtable += '<table id="printtable" border="1px solid black;" style="box-shadow: 2px 2px 7px 0px;border: 1px solid #ffffff;page-break-after:always;><thead style="text-align:  center;"><tr style="height:  50px;"><th style="width: 5%;color:  black;">Sr No</th><th style="width: 15%;color:  black;">Error Code</th><th style="width: 30%;color:  black;">Description</th><th style="width: 30%;color:  black;">Equipment Count</th></tr></thead><tbody style="text-align:  center;" id="printtable_tbody ">';

                                for (var n = 0; n < dataSet.length - 2; n++) {
                                    k = n + 1;
                                    printtable += '<tr style="text-align:  center;height:50px;"><td style="color:  black;">' + k + '</td><td style="color:  black;">' + label[n] + '</td><td style="color:  black;">' + errorDescription[n] + '</td><td style="color:  black;">' + dataSet[n] + '</td></tr>';
                                }

                                printtable += "</tbody></table>";

                                $("#print_table").html(printtable);
                                $("#faultDescription" + i).html(tbody);
                                $("#faultDescription" + i).css("display", "none");

                                $("#new_charts" + i).css("display", "none");
                                Highcharts.chart(chartName, {
                                    chart: {
                                        backgroundColor: '#2d3035',
                                        type: 'column'
                                    },
                                    title: {
                                        text: 'Equipment Count With DTC',
                                        style: {
                                            color: 'white',
                                            fontSize: '16px',
                                            fontWeight: 'bold'
                                        }
                                    },
                                    xAxis: {
                                        text: 'Equipment Id',
                                        categories: newLabel,
                                        crosshair: true,
                                        color: 'white',
                                        title: {
                                            text: 'Error Codes',
                                            color: 'white',
                                            style: {
                                                color: 'white',
                                            },
                                            labels: {
                                                style: {
                                                    color: 'white',
                                                    fontSize: '14px',
                                                    fontWeight: 'italian'
                                                }
                                            },
                                        }
                                    },
                                    yAxis: {
                                        min: 0,
                                        allowDecimals: false,
                                        color: 'white',
                                        title: {
                                            text: 'No of Equipment',
                                            color: 'white'
                                        },
                                        labels: {
                                            style: {
                                                color: 'white',
                                            }
                                        }
                                    },
                                    tooltip: {
                                        headerFormat: '<span style="font-size:12px">{point.key}</span><table>',
                                        pointFormat: '<tr><td style="padding:0;font-size:12px">{series.name}: </td>' +
                                            '<td style="padding:0"><b>{point.y} </b></td></tr>',
                                        footerFormat: '</table>',
                                        shared: true,
                                        useHTML: true
                                    },
                                    plotOptions: {
                                        column: {
                                            pointPadding: 0.2,
                                            borderWidth: 0
                                        },
                                        series: {
                                            borderWidth: 0,
                                            borderColor: 'black'
                                        }
                                    },
                                    legend: {
                                        itemStyle: {
                                            color: '#fff'
                                        }
                                    },
                                    series: [{
                                        name: 'Equipment Count with DTC',
                                        data: newData,
                                        color: "#55ce63"
                                    }]
                                });
                            }
                        }

                    },
                    error: function(xhr, status, error) {
                        alert("Error while loading graph. Please try again.");
                    },
                    complete: function() {
                        $("#new_charts1").css("display", "block");
                        $("#faultDescription1").css("display", "block");
                        $('#loadimage').hide();
                        $(".overlay").remove();
                    }
                });
            }
        }
    });
});

//function to be called on click of Print
function printDiv() {
    var printContents = "";
    printContents += document.getElementById('chartHeader').innerHTML;
    for (var i = 1; i <= no_of_pages; i++) {
        printContents += "<br>" + document.getElementById('new_charts' + i).innerHTML;
    }

    printContents += "<br><br><br>" + document.getElementById('print_table').innerHTML;

    var w = window.innerWidth;
    var h = window.innerHeight;
    var win = window.open('', '', 'left=0,top=0,width = w,height = h,toolbar=0,scrollbars=0,status =0');
    win.document.write(printContents);
    win.print();
    win.close();
}

//Show data of 10 error codes at a time
function getPageNo(obj, no_of_pages) {
    var action = 1;
    var page = obj.getAttribute("value");

    if (page == 'Prev') {
        if ((selected_page_value - 1) >= 1) {
            selected_page_value = selected_page_value - 1;
            page = selected_page_value;
            action = 1;

            if (!$("#pageNum_" + page).hasClass("activePageNum")) {
                $("#pageNum_" + (page)).addClass("activePageNum").removeClass("inactivePageNum");
                $("#pageNum_" + (page + 5)).addClass("inactivePageNum").removeClass("activePageNum");
            }
        } else
            action = 0;
    } else if (page == 'Next') {
        if ((selected_page_value + 1) <= no_of_pages) {
            selected_page_value = selected_page_value + 1;
            page = selected_page_value;
            action = 1;

            if (!$("#pageNum_" + page).hasClass("activePageNum")) {
                $("#pageNum_" + (page)).addClass("activePageNum").removeClass("inactivePageNum");
                $("#pageNum_" + (page - 5)).addClass("inactivePageNum").removeClass("activePageNum");
            }
        } else
            action = 0;
    } else if (page == 'First') {
        //show first 5 page numbers 
        for (var i = 1;
            (i <= no_of_pages); i++) {
            if (i <= 5)
                $("#pageNum_" + i).addClass("activePageNum").removeClass("inactivePageNum");
            else
                $("#pageNum_" + i).addClass("inactivePageNum").removeClass("activePageNum");
        }

        selected_page_value = 1;
        page = selected_page_value;
        action = 1;
    } else if (page == 'Last') {
        //show last 5 page numbers
        for (var i = 1;
            (i <= no_of_pages); i++) {
            if (i > (no_of_pages - 5))
                $("#pageNum_" + i).addClass("activePageNum").removeClass("inactivePageNum");
            else
                $("#pageNum_" + i).addClass("inactivePageNum").removeClass("activePageNum");
        }

        selected_page_value = no_of_pages;
        page = selected_page_value;
        action = 1;
    } else
        selected_page_value = parseInt(page);

    for (var i = 1; i <= no_of_pages; i++) {
        if (i == page && action == 1) {
            $("#graphArea" + i).css("display", "block");
            $("#new_charts" + i).css("display", "block");
            $("#faultDescription" + i).css("display", "block");
            $("#pageNum_" + i + " a").addClass("underline");
        } else {
            if (action == 1) {
                $("#graphArea" + i).css("display", "none");
                $("#new_charts" + i).css("display", "none");
                $("#faultDescription" + i).css("display", "none");

                $("#pageNum_" + i + " a").removeClass("underline");
            }
        }
    }
}