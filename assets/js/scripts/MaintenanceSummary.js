  //on document load
  $(document).ready(function(){
     var _gaq = _gaq || [];
          _gaq.push(['_setAccount', 'UA-36251023-1']);
          _gaq.push(['_setDomainName', 'jqueryscript.net']);
          _gaq.push(['_trackPageview']);

          (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
          })();
    var base_url = document.getElementById("hdnBaseUrl").value;

    //datepicker
    // $(function() {
    //   $("#fromDate").datepicker();
    //   $("#toDate").datepicker();
    // });
    

    //hide map initially
    $("#map").hide();
    $("#countContainer").hide();
    $("#countContainer1").hide();
    $("#mapArea").hide();

  	//ajax call to get sites
    $.ajax({
      beforeSend: function(){
         //Fade whole page
  	    $("body").prepend("<div class='overlay'></div>");

  	    $(".overlay").css({
  	        "position": "absolute", 
  	        "width": $(document).width(), 
  	        "height": $(document).height(),
  	        "z-index": 9999, 
  	    }).fadeTo(0, 0.8);
  	    //show loader  
  	   $("#loadimage").css("z-index","99999").show();    
      },
      type:"POST",
      url: base_url + "MaintenanceSummary/getSites",
      async: true,
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      data:{
        //userID : userID,
      },
      success: function(result){
        if(result == null || result == "" || result == "false")
        {
          alert("No data available.");
          return;
        }
        //else bind sites to select option
        var data = JSON.parse(result);
        var html = "";
        html += "<option value = 0>--Select site--</option>"
    	  $.each(data,function(k,v){
    	    html += "<option value="+v.idTier5+" unitCodeName="+v.tier5Name+">"+v.tier5Name+"</option>";
    	  })
    	  $("#MSite").html(html);

    	  //load equipments on change of site and class
    	  $("#MSite").change(function(){
    	  		$("#Maintaince_className").val(0);
            $("#MEquipment").html('');
    	  });

        //Load equipments on change of class
        $("#Maintaince_className").change(function(){
        //check if site is selected
        if($("#MSite").val() == 0){
          alert("Please select site.");
          return;
        }

          //get equipments from site and class
          site_selected = $("#MSite").val();
          class_selected = $("#Maintaince_className").val();

          if(class_selected == 0){
            return;
          }
          // getEquipmentsForMaintaince(site_selected, class_selected);
          //ajax call to get equipments
            $.ajax({
              beforeSend: function(){
                 //Fade whole page
                $("body").prepend("<div class='overlay'></div>");

                $(".overlay").css({
                    "position": "absolute", 
                    "width": $(document).width(), 
                    "height": $(document).height(),
                    "z-index": 9999, 
                }).fadeTo(0, 0.8);
                //show loader  
               $("#loadimage").css("z-index","99999").show();    
              },
              type:"POST",
              url: base_url + "MaintenanceSummary/getEquipmentsForMaintaince",
              async: true,
              contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
              data:{
                siteId : site_selected,
                className : class_selected
              },
              success: function(result){
                if(result == null || result == "" || result == "false")
                {
                  alert("No data available.");
                  return;
                }

                //else bind sites to select option
                var data = JSON.parse(result);
                var html = "";
                html += "<option value = 0>--Select equipment--</option>"
                $.each(data,function(k,v){
                  html += "<option value="+v.idEquipment+">"+v.idEquipment+"</option>";
                })
                $("#MEquipment").html('');
                $("#MEquipment").html(html);

              },
              error: function(xhr,status,error){
                alert("Error while fetching equipment. Please try again.");
              },
              complete: function(){
                $("#loadimage").css("z-index","").hide();
                $(".overlay").remove();
              } 
            });//end of ajax call
        });
      },
      error: function(xhr,status,error){
        alert("Error while fetching sites. Please try again.");
      },
      complete: function(){
        $("#loadimage").css("z-index","").hide();
        $(".overlay").remove();
      } 
    });//end of ajax call


      //Register click events
      //Load details on click of reload
      // $("#maintenanceReload").off('click');
      // $("#maintenanceReload").on('click','reloadMaintainceDetails');

      $("#maintenanceReload").on('click',function(){
        // $("#totalFaultCount").html('');

        //input parameter validations
        //check if site is selected
        if($("#MSite").val() == 0){
          alert("Please select site.");
          return;
        }
        //check if class is selected
        if($("#Maintaince_className").val() == 0){
          alert("Please select class.");
          return;
        }
        //check if class is selected
        if($("#MEquipment").val() == null || $("#MEquipment").val() == 0 ){
          alert("Please select equipment.");
          return;
        }
        //date checks
        // var fromDate = $("#fromDate").val();
        // var toDate = $("#toDate").val();
        // //From and to date are mandatory
        // if(fromDate == "" || toDate == ""){
        //   alert("It is mandatory to enter from date and to date.");
        //   return;
        // }
        // else if(new Date(fromDate) > new Date(toDate)){
        //   alert("To date should be greater or equal than from date.");
        //   return;
        // }

      //remove the spinner class
      $("#totalFaultCount .spinner-holder-one").remove();
      $("#faultCount .spinner-holder-one").remove();
      $("#totalImpactCount .spinner-holder-one").remove();
      $("#impactCount .spinner-holder-one").remove();
      $("#totalChecklistCount .spinner-holder-one").remove();
      $("#checklistCount .spinner-holder-one").remove();
      $("#totalServiceMeter .spinner-holder-one").remove();
      $("#serviceMeter .spinner-holder-one").remove();
        
        //read selected equipment id
        equipment_selected = $("#MEquipment").val();
        var hoursOfOperation;
                var currentWeekFaults;
                var overallFaults;
        //ajax call to get the truck maintaince parameters
        //Impacts,Checklists,Hours of operation,Current location
        $.ajax({
          beforeSend: function(){
           //Fade whole page
            $("body").prepend("<div class='overlay'></div>");
              $(".overlay").css({
                "position": "absolute", 
                "width": $(document).width(), 
                "height": $(document).height(),
                "z-index": 9999, 
              }).fadeTo(0, 0.8);
                //show loader  
              $("#loadimage").css("z-index","99999").show();    
            },
            type:"POST",
            url: base_url + "MaintenanceSummary/getTruckMaintenanceDetails",
            async: true,
            contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
            data:{
              equipmentId : equipment_selected,
              className : class_selected
              // fromDate: fromDate,
              // toDate : toDate
            },
            success: function(result){
              
              if(result == null || result == "" || result == "false")
              {
                alert("No data available.");
                return;
              }
              var data = JSON.parse(result);
              //faults count
              
              if(data["hoursOfOperation"] != "" && data["hoursOfOperation"] != undefined){
                hoursOfOperation = data['hoursOfOperation'];
              }
              else {
                hoursOfOperation = 0;
              }
              
              if(data["overallHoursOfOperation"] != "" && data["overallHoursOfOperation"] != undefined)
              {
                overallHoursOfOperation = data['overallHoursOfOperation'];
              } 
              else {
                overallHoursOfOperation = 0;
              }
              
              if(data["currentWeekFaults"] != "" && data["currentWeekFaults"] != undefined)
              {
                currentWeekFaults = data['currentWeekFaults'];
              }  
              else {
                currentWeekFaults = 0;
              }
              
              if(data["overallFaults"] != "" && data["overallFaults"] != undefined)
              {
                overallFaults = data['overallFaults'];
              }  
              else {
                overallFaults =0;
              }
 
              //Current location of equipment
              if(data["latitude"] == "" || data["latitude"] == undefined || data["latitude"] =="0.0")
              {
                data["latitude"] = 45.512794; //portland by default
                // data["latitude"] = 0;
              }

              if(data["longitude"] == "" || data["longitude"] == undefined || data["longitude"] =="0.0")
              {
                data["longitude"] = -122.679565; //portland by default
                // data["longitude"] = 0
              }

              //Impacts count
              var impactCnt = Math.floor(Math.random() * 10) + 1;
              var totalImpactCount = Math.floor(Math.random() * 20) + 11;
              //checklist failure count
              var checklistFailureCnt = Math.floor(Math.random() * 10) + 1;
              var totalchecklistFailureCnt = Math.floor(Math.random() * 20) + 11;
              $("#countContainer").show();
              $("#countContainer1").show();
              $("#mapArea").show();

              $('.counter').each(function() {
                var $this = $(this),
                countTo = $this.attr('data-count');

                $({ countNum: $this.text()}).animate({
                    countNum: countTo
                },
                {
                    duration: 1000,
                    easing:'linear',
                    step: function() {
                      $this.text(Math.floor(this.countNum));
                    },
                    complete: function() {
                        $this.text(this.countNum);
                      //alert('finished');
                    }
                });  
              });

              /* start fault code */
              $("#falutcode_month").html(overallFaults);
              $("#falutcode_maintenance").html(currentWeekFaults);
              var PIECHART = $('#falutcode_month_chart');
              var myPieChart = new Chart(PIECHART, {
                  type: 'doughnut',
                  options: {
                      cutoutPercentage: 90,
                      legend: {
                          display: false
                      },
                      tooltips: {enabled: false},
                      hover: {mode: null}
                  },
                  data: {
                    datasets: [
                    {
                      data: [100],
                      borderWidth: [0, 0, 0, 0],
                      backgroundColor: ['#007ff9'],
                      //hoverBackgroundColor: ['#007ff9']
                    }]
                  }
              });

              var PIECHART = $('#falutcode_maintenance_chart');
              var myPieChart = new Chart(PIECHART, {
                  type: 'doughnut',
                  options: {
                      cutoutPercentage: 90,
                      legend: {
                          display: false
                      },
                      tooltips: {enabled: false},
                      hover: {mode: null}
                  },
                  data: {
                    datasets: [
                      {
                        data: [100],
                        borderWidth: [0, 0, 0, 0],
                        backgroundColor: ['#007ff9'],
                        hoverBackgroundColor: ['#007ff9']
                      }]
                  }
              });
              /* end fault code */

              /*start checklist count */
              $("#checklist_month").html("20");
              $("#checklist_mentenance").html("3");

              var PIECHART = $('#checklist_month_chart');
              var myPieChart = new Chart(PIECHART, {
                  type: 'doughnut',
                  options: {
                      cutoutPercentage: 90,
                      legend: {
                          display: false
                      },
                      tooltips: {enabled: false},
                      hover: {mode: null}
                  },
                  data: {
                      datasets: [
                      {
                        data: [100],
                        borderWidth: [0, 0, 0, 0],
                        backgroundColor: ['#007ff9'],
                        hoverBackgroundColor: ['#007ff9']
                      }]
                  }
              });

              var PIECHART = $('#checklist_mentenance_chart');
              var myPieChart = new Chart(PIECHART, {
                  type: 'doughnut',
                  options: {
                      cutoutPercentage: 90,
                      legend: {
                          display: false
                      },
                      tooltips: {enabled: false},
                      hover: {mode: null}
                  },
                  data: {
                      datasets: [
                      {
                        data: [100],
                        borderWidth: [0, 0, 0, 0],
                        backgroundColor: ['#007ff9'],
                        hoverBackgroundColor: ['#007ff9']
                      }]
                  }
              });
              /* end checklist count */

              /* start impact count */

              $("#impact_month").html("20");
              $("#impact_mainetenance").html("3");

              var PIECHART = $('#impact_month_chart');
              var myPieChart = new Chart(PIECHART, {
                  type: 'doughnut',
                  options: {
                      cutoutPercentage: 90,
                      legend: {
                          display: false
                      },
                      tooltips: {enabled: false},
                      hover: {mode: null}
                  },
                  data: {
                      datasets: [
                      {
                        data: [100],
                        borderWidth: [0, 0, 0, 0],
                        backgroundColor: ['#007ff9'],
                        hoverBackgroundColor: ['#007ff9']
                      }]
                  }
              });

              var PIECHART = $('#impact_mainetenance_chart');
              var myPieChart = new Chart(PIECHART, {
                  type: 'doughnut',
                  options: {
                      cutoutPercentage: 90,
                      legend: {
                          display: false
                      },
                      tooltips: {enabled: false},
                      hover: {mode: null}
                  },
                  data: {
                      datasets: [
                      {
                        data: [100],
                        borderWidth: [0, 0, 0, 0],
                        backgroundColor: ['#007ff9'],
                        hoverBackgroundColor: ['#007ff9']
                      }]
                  }
              });
              /* end impact count */

              /* start maine service meter */
              $("#mainServicemeter").html(overallHoursOfOperation);
              $("#mainServicemeter_maintenance").html(hoursOfOperation);

              var PIECHART = $('#mainServicemeter_total_chart');
              var myPieChart = new Chart(PIECHART, {
                  type: 'doughnut',
                  options: {
                      cutoutPercentage: 90,
                      legend: {
                          display: false
                      },
                      tooltips: {enabled: false},
                      hover: {mode: null}
                  },
                  data: {
                      datasets: [
                      {
                        data: [100],
                        borderWidth: [0, 0, 0, 0],
                        backgroundColor: ['#007ff9'],
                        hoverBackgroundColor: ['#007ff9']
                      }]
                  }
              });

              var PIECHART = $('#mainServicemeter_maintenance_chart');
              var myPieChart = new Chart(PIECHART, {
                  type: 'doughnut',
                  options: {
                      cutoutPercentage: 90,
                      legend: {
                          display: false
                      },
                      tooltips: {enabled: false},
                      hover: {mode: null}
                  },
                  data: {
                      datasets: [
                      {
                        data: [100],
                        borderWidth: [0, 0, 0, 0],
                        backgroundColor: ['#007ff9'],
                        hoverBackgroundColor: ['#007ff9']
                      }]
                  }
              });
              /* end main service meter count*/
            

              //show map
              var positionCoords = {lat: parseFloat(data["latitude"]), lng: parseFloat(data["longitude"])};
              var mapOptions = {
                center: positionCoords,//new google.maps.LatLng(51.5, -0.12),
                zoom: 10,
                mapTypeId: google.maps.MapTypeId.ROADMAP
              }
              var map = new google.maps.Map(document.getElementById("map"), mapOptions);
              var marker = new google.maps.Marker({
                position: positionCoords,
                map: map
              });

              //show map
              $("#map").show();
            },
            error: function(xhr,status,error){
              alert("Error while fetching truck maintenance details. Please try again.");
            },
            complete: function(){
              $("#loadimage").css("z-index","").hide();
              $(".overlay").remove();
            } 
        });//end of ajax call
      });
  });