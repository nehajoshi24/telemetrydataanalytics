var base_url = document.getElementById("hdnBaseUrl").value;
var durationUnit;
var fromDate;
var toDate;
var selectedOperatorIds;
/* On document ready */
$(document).ready(function(){
  /*$("#show_productivity").hide();*/
  jQuery(function () {
    jQuery("#demo").gs_multiselect_operators();
  });

  $( "#fromDate" ).datepicker({
    inline: true
  });

  $( "#toDate" ).datepicker({
    inline: true
  });

  $("#displayArea").hide();

  $("#generateOperatorSCard").on("click",function(){
    if(document.getElementById("idle_time").checked == true || document.getElementById("working_time").checked == true || document.getElementById("hydraulic_time").checked == true || document.getElementById("distance_travel").checked == true || /*document.getElementById("average_speed").checked == true ||*/ document.getElementById("no_of_loads").checked == true){
        //$("#btn_productivity").show();
        $("#show_productivity").show();
      }
      else{
        //$("#btn_productivity").hide();
        $("#show_productivity").hide();
      }

      if(document.getElementById("overspeeds").checked == true || document.getElementById("impacts").checked == true || document.getElementById("seatbelt_violation").checked == true || document.getElementById("parkbrake_violation").checked == true){
        //$("#btn_safety").show();
        $("#show_safety").show();
      }
      else{
        //$("#btn_safety").hide();
        $("#show_safety").hide();
      }

    if(document.getElementById("idle_time").checked!= true && document.getElementById("working_time").checked != true && document.getElementById("hydraulic_time").checked != true && document.getElementById("no_of_loads").checked != true && document.getElementById("distance_travel").checked != true && document.getElementById("overspeeds").checked != true && document.getElementById("impacts").checked != true && document.getElementById("seatbelt_violation").checked != true && document.getElementById("parkbrake_violation").checked != true){
      alert("Please select atleast one parameter");
      return;
    } 

    //enable all buttons by default
    $("#show_productivity").removeAttr("disabled");
    $("#show_safety").removeAttr("disabled");

    getOperatorScorecard();

  }); /* end of On click of generate chart */
 

  //Show to and from date if Custom is selected
  $("#custom_dateRange").hide();

  $("#select_dateRange").on("change",function(){
    var isCustom = $("#select_dateRange").val();
    if(isCustom == 'custom'){
      $("#custom_dateRange").show();
    }
    else{
      $("#custom_dateRange").hide();  
    }
  }); /* End of select custom range */

  //Show menu div on click of Next
  $("#next_selectOp").on("click", function(){

    /*operatorIds = document.getElementsByName("operatorIds[]");*/
    selectedOperatorIds = $('#operatorIds').val();
    
    if(operatorIds.length == 0)
    {
      $("#errorMsgOperator").text("Please select operator/s.");
      $("#errorMsgOperator").focus();
      return;
    }

    durationUnit = $("#select_dateRange").val();
    if(durationUnit == 0)
    {
     $("#errorMsgOperator").text("Please select date duration.");
     $("#errorMsgOperator").focus();
      return; 
    }
    else if(durationUnit == "custom")
    {
      fromDate = $("#fromDate").val();
      toDate = $("#toDate").val();

      if(fromDate == "" || fromDate == null || toDate =="" || toDate ==null)
      {
        $("#errorMsgOperator").text("It is mandatory to enter from date and to date.");
        $("#errorMsgOperator").focus();
        return;    
      }
      if(new Date(fromDate) > new Date(toDate))
      {
        $("#errorMsgOperator").text("To date should be greater or equal than from date.");
        $("#errorMsgOperator").focus();
        return;     
      }
    }

    $("#selectOp").css("display","none");
    $("#menuScreen").css("display","block");

    var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
 
    var from = new Date();
    var to = new Date();

    if(durationUnit == "custom")
    {
      from = new Date($("#fromDate").val());
      to = new Date($("#toDate").val());
    }
    else if(durationUnit == "weekly")
    {
       from.setDate(from.getDate() - 6); 
    }
    else if(durationUnit == 'monthly')
    {
        from.setDate(from.getDate() - 30); 
    }
    else if(durationUnit == 'yearly')
    {
        from.setDate(from.getDate() - 365); 
    }

    //display selected date range on UI
    $("#spn_opSCardDuration").text(from.getDate() + ' ' + monthNames[from.getMonth()] + ','+ from.getFullYear() + ' to ' +
    to.getDate() + ' ' + monthNames[to.getMonth()] + ','+ to.getFullYear());
  });  /* End of on click of select op */

  //Hide all graph containers when page loads
  $("#loadimage").hide();
  $("#productivity_details").hide();
  $("#scorecard_container").hide(); 
  $("#safety_details").hide();
  $("#individual_productivity_details").hide();
  $("#individual_safety_details").hide();
  $("#individual_safety").hide();


  /* Onclick of button productivity Details */
  $("#show_productivity").on("click",function(){
    $("#scorecard_container").hide();
    $("#check_parameters").hide();
    $("#productivity_details").show();
    /*$("#show_safety").hide();*/
    $("#safety_details").hide();
    $("#individual_productivity_details").hide();
    $("#individual_safety_details").hide();
    $("#individual_safety").hide();

    $("#productivity").hide();
  });

  /* Onclick of button health/Safety Details */
  $("#show_safety").on("click",function(){
    $("#scorecard_container").hide();
    $("#check_parameters").hide();
    $("#productivity_details").hide();
    /*$("#show_safety").show();*/
    $("#safety_details").show();
    $("#individual_productivity_details").hide();
    $("#individual_safety_details").hide();
    $("#individual_safety").hide();

    $("#productivity").hide();
  })

  /* To comming back to scorecard page from details page */
  $("#show_card1,#show_card2").on("click",function(){
    $("#scorecard_container").show(); 
    $("#check_parameters").show();
    $("#productivity_details").hide();
    /*$("#show_safety").hide();*/
    $("#safety_details").hide();
    $("#individual_productivity_details").hide();
    $("#individual_safety_details").hide();
    $("#individual_safety").hide();

    $("#productivity").show();

  });

  $("#show_productivity_details").on("click",function(){
    $("#individual_productivity_details").show();
    $("#individual_productivity").show();
    $("#individual_productivity2").hide();
    $("#individual_productivity3").hide();
    $("#individual_safety_details").hide();
    $("#loadimage").hide();
    $("#productivity_details").hide();
    $("#scorecard_container").hide(); 
    $("#safety_details").hide();
    $("#individual_safety").hide();

    //apply box shadow to selected tab only
    $("#timeGraph").addClass("tabBoxShadow");
    $("#loadsGraph").removeClass("tabBoxShadow");
    $("#distanceGraph").removeClass("tabBoxShadow");

    if(document.getElementById("idle_time").checked == true || document.getElementById("working_time").checked == true || document.getElementById("hydraulic_time").checked == true)
    {
      $("#timeGraph").show();
    }
    else{
      $("#timeGraph").hide();
    }

    if(document.getElementById("distance_travel").checked == true)
    {
      $("#distanceGraph").show();
    }
    else{
      $("#distanceGraph").hide();
    }

    if(document.getElementById("no_of_loads").checked == true)
    {
      $("#loadsGraph").show();
    }
    else{
      $("#loadsGraph").hide();
    }

  });

  $("#show_safety_details").on("click",function(){
    $("#individual_safety_details").show();
    $("#loadimage").hide();
    $("#productivity_details").hide();
    $("#scorecard_container").hide(); 
    $("#safety_details").hide();
    $("#individual_safety").show();
  });

  $("#backto_productivity").on("click",function(){
    $("#individual_productivity_details").hide();
    $("#individual_safety_details").hide();
    $("#scorecard_container").hide();
    $("#check_parameters").hide();
    $("#productivity_details").show();
    /*$("#show_safety").hide();*/
    $("#safety_details").hide();
    $("#individual_safety").hide();
  });

  $("#backto_safety").on("click",function(){
    $("#individual_productivity_details").hide();
    $("#individual_safety_details").hide();
    $("#scorecard_container").hide();
    $("#check_parameters").hide();
    $("#productivity_details").hide();
    /*$("#show_safety").hide();*/
    $("#safety_details").show();
    $("#individual_safety").hide();
  });

  $("#backto_scorecard, #backto_scorecard1").on("click",function(){
    $("#scorecard_container").show(); 
    $("#check_parameters").show();
    $("#productivity_details").hide();
    /*$("#show_safety").hide();*/
    $("#safety_details").hide();
    $("#individual_productivity_details").hide();
    $("#individual_safety_details").hide();
    $("#individual_safety").hide();

    $("#productivity").show();
  });

  $("#fromDate").on("click",function(){
    $("#fromDate").datepicker();
  });

  $("#toDate").on("click",function(){
    $("#toDate").datepicker();
  });  

  $(function() {
    $("#fromDate").datepicker();
    $("#toDate").datepicker();
  });

}); /* end of On document ready */


function getOperatorScorecard()
{
  //disable button to avoid multiple clicks
   $("#generateOperatorSCard").attr("disabled","disabled");

  //Fade whole page
  $("body").prepend("<div class='overlay'></div>");

  $(".overlay").css({
    "position": "absolute", 
    "width": $(document).width(), 
    "height": $(document).height(),
    "z-index": 9999, 
  }).fadeTo(0, 0.8);

  /*operatorIds = document.getElementsByName("operatorIds[]");
  // operatorIds = $('#operatorIds').val();
  operatorIdsList = "";
    //get operators separated by comma
  len = operatorIds.length;
  for(i=0; i<len; i++)
  {
    operatorIdsList = operatorIdsList + operatorIds[i].value + ",";
  }*/

  durationUnit = $("#select_dateRange").val();
  if(durationUnit == "custom")
  {
    fromDate = $("#fromDate").val();
    toDate = $("#toDate").val();
  }
  else
  {
    /*fromDate = new Date(0);//Jan 01 1970
    toDate = new Date(0);//Jan 01 1970*/
    fromDate = "";
    toDate = "";
  }

  $.ajax({
    beforeSend: function(){
      $("#loadimage").css("z-index","99999").show();    
    },
    type:"POST",
    url: base_url + "operatorSCardController/getOperatorScorecardData",
    async: true,
    contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
    data:{
      operatorIdsList : selectedOperatorIds,
      durationUnit : durationUnit,
      fromDate :fromDate,
      toDate : toDate
    },
    success: function(result){
      $("#displayArea").show();
      if(result == null || result == "" || result == "false")
      {
        alert("No data available.");
        return;
      }
      //apply logic and load graph      
      loadOperatorScorecard(selectedOperatorIds, result);
      //console.log(result);
      //loadOperatorScorecard_demo(operatorIdsList, result);
    },
    error: function(xhr,status,error){
      alert("Error while fetching operator scorecard details. Please try again.");
    },
    complete: function(){
      $("#loadimage").hide();
      $(".overlay").remove();

      //enable button to avoid multiple clicks
      $("#generateOperatorSCard").removeAttr("disabled");
    }
  }); /* End of AJAX call */
} /* End of function getOperatorScorecard */

function loadOperatorScorecard(operatorIdsList, result){
  details = JSON.parse(result);
  
  /* Array Declarations */
  var oper = [];            var operatorNames = [];
  var idleTimeArray = [];   var idleTimeArray_per = [];   var finalIdleArray = [];      var finalIdleArray_per = [];
  var workingArray = [];    var workingArray_per = [];    var finalWorkingArray = [];   var finalWorkingArray_per = [];
  var hydraulicArray = [];  var hydraulicArray_per = [];  var finalhydraulicArray = []; var finalhydraulicArray_per = [];
  var no_of_loads = [];     var no_of_loads_per = [];     var finalNumLoadsArray = [];  var finalNumLoadsArray_per = [];
  var distanceDriven = [];  var distanceDriven_per = [];  var finaldistanceArray = [];  var finaldistanceArray_per = [];

  var overspeeds = [];      var overspeeds_per = [];      var finaloverspeedArray = []; var finaloverspeedArray_per = [];
  var impacts = [];         var impacts_per = [];         var finalimpactArray = [];    var finalimpactArray_per = [];
  var seatbeltArray = [];   var seatbeltArray_per = [];   var finalseatbeltArray = [];  var finalseatbeltArray_per = [];
  var parkbrakeArray = [];  var parkbrakeArray_per = [];  var finalparkbrakeArray = []; var finalparkbrakeArray_per = [];
  
  var productivityPercent = []; var safetyPercent = [];
  var finalsafetyScoreArray = []; var finalproductivityScoresArray = [];

  /* Variable Declaration */
  weight = 0;
  weightSafety = 0;
  productivityScore = 0;
  safetyScores = 0;

  trueCount = 0;
  safetyTrueCount = 0;

  //declare thresholds
  workingTimeThreshold = 45
  hydraulicTimeThreshold = 30
  no_of_loadsThreshold = 600
  distanceDrivenThreshold = 350

  overspeedsThreshold = 10
  impactsThreshold = 4
  seatbeltViolationThreshold = 4
  parkbrakeThreshold = 4

  for(var m=0;m<details.length; m++)
  {
    oper.push(details[m]["name"]);
    operatorNames.push(details[m]["name"]);
    idleTimeArray.push(parseFloat(details[m]["idleTime"]));
    workingArray.push(parseFloat(details[m]["WorkingTime"]));
    hydraulicArray.push(parseFloat(details[m]["hydraulicFunctionTime"]));
    no_of_loads.push(parseFloat(details[m]["NoOfLoads"]));
    distanceDriven.push(parseFloat(details[m]["distanceDriven"]));

    overspeeds.push(parseFloat(details[m]["OverSpeedCount"]));
    impacts.push(parseFloat(details[m]["ImpactCount"]));
    seatbeltArray.push(parseFloat(details[m]["seatBeltInterlockViolationCount"]));
    parkbrakeArray.push(parseFloat(details[m]["parkBrakeInterlockViolationCount"]));
  }

  //console.log(details);

  for(var i=0; i<workingArray.length; i++)
  {
    var num = 0;
    if(workingArray[i] > workingTimeThreshold)
        num = workingTimeThreshold;
    else
        num = workingArray[i];

    workingArray_per.push(parseFloat((num/workingTimeThreshold)*100).toFixed(2));
  }
  //Copy idle time percentage same as working time percentage 
  for(var i=0; i<idleTimeArray.length; i++)
  {
    idleTimeArray_per.push(workingArray_per[i]);
  }

  for(var i=0; i<hydraulicArray.length; i++)
  {
    var num = 0;
    if(hydraulicArray[i] > hydraulicTimeThreshold)
        num = hydraulicTimeThreshold;
    else
        num = hydraulicArray[i];

    hydraulicArray_per.push(parseFloat((num/hydraulicTimeThreshold)*100).toFixed(2));
  }
  for(var i=0; i<no_of_loads.length; i++)
  {
    var num = 0;
    if(no_of_loads[i] > no_of_loadsThreshold)
        num = no_of_loadsThreshold;
    else
        num = no_of_loads[i];

    no_of_loads_per.push(parseFloat((num/no_of_loadsThreshold)*100).toFixed(2));
  }
  for(var i=0; i<distanceDriven.length; i++)
  {
    var num = 0;
    if(distanceDriven[i] > distanceDrivenThreshold)
        num = distanceDrivenThreshold;
    else
        num = distanceDriven[i];

    distanceDriven_per.push(parseFloat((1-(num/distanceDrivenThreshold))*100).toFixed(2));
  }

  //parameter wise percentage score - Health/Safety
  for(var i=0; i<overspeeds.length; i++)
  {
    var num = 0;
    if(overspeeds[i] > overspeedsThreshold)
        num = overspeedsThreshold;
    else
        num = overspeeds[i];

    overspeeds_per.push(parseFloat((1-(num/overspeedsThreshold))*100).toFixed(2));
  }
  for(var i=0; i<impacts.length; i++)
  {
    var num = 0;
    if(impacts[i] > impactsThreshold)
        num = impactsThreshold;
    else
        num = impacts[i];

    impacts_per.push(parseFloat((1-(num/impactsThreshold))*100).toFixed(2));
  }
  for(var i=0; i<seatbeltArray.length; i++)
  {
    var num = 0;
    if(seatbeltArray[i] > seatbeltViolationThreshold)
        num = seatbeltViolationThreshold;
    else
        num = seatbeltArray[i];

    seatbeltArray_per.push(parseFloat((1-(num/seatbeltViolationThreshold))*100).toFixed(2));
  }
  for(var i=0; i<parkbrakeArray.length; i++)
  {
    var num = 0;
    if(parkbrakeArray[i] > parkbrakeThreshold)
        num = parkbrakeThreshold;
    else
        num = parkbrakeArray[i];

    parkbrakeArray_per.push(parseFloat((1-(num/parkbrakeThreshold))*100).toFixed(2));
  }

  /*console.log(idleTimeArray_per);
  console.log(workingArray_per);
  return;*/
  
  /*console.log(idleTimeArray);
  console.log(workingArray);
  console.log(hydraulicArray);
  console.log(no_of_loads);
  console.log(distanceDriven);

  console.log(overspeeds);
  console.log(impacts);
  console.log(seatbeltArray);
  console.log(parkbrakeArray);*/
  

  if(document.getElementById("idle_time").checked == true) trueCount += 1;
  if(document.getElementById("working_time").checked == true) trueCount += 1; 
  if(document.getElementById("hydraulic_time").checked == true) trueCount += 1; 
  if(document.getElementById("no_of_loads").checked == true) trueCount += 1; 
  if(document.getElementById("distance_travel").checked == true)trueCount += 1; 
  
  if(document.getElementById("overspeeds").checked == true) safetyTrueCount +=1;
  if(document.getElementById("impacts").checked == true) safetyTrueCount +=1;
  if(document.getElementById("seatbelt_violation").checked == true) safetyTrueCount +=1;
  if(document.getElementById("parkbrake_violation").checked == true) safetyTrueCount +=1;

  weight = (100/trueCount)/100;
  weightSafety = (100/safetyTrueCount)/100;

  for(var i=0;i<operatorNames.length;i++){
    productivityScore = 0;
    safetyScores = 0;

    /* To calculate productivity score in percentage accoding to selected parameters
        weightage of each parameter decided accprding to no of parameters selected. */
    if(document.getElementById("idle_time").checked == true){
      finalIdleArray.push(idleTimeArray[i]);
      finalIdleArray_per.push(idleTimeArray_per[i]);
      productivityScore += idleTimeArray_per[i] * weight;
    }
    if(document.getElementById("working_time").checked == true){
      finalWorkingArray.push(workingArray[i]);
      finalWorkingArray_per.push(workingArray_per[i]);
      productivityScore += workingArray_per[i] * weight;
    }
    if(document.getElementById("hydraulic_time").checked == true){
      finalhydraulicArray.push(hydraulicArray[i]);
      finalhydraulicArray_per.push(hydraulicArray_per[i]);
      productivityScore += hydraulicArray_per[i] * weight;
    }
    if(document.getElementById("no_of_loads").checked == true){
      finalNumLoadsArray.push(no_of_loads[i]);
      finalNumLoadsArray_per.push(no_of_loads_per[i]);
      productivityScore += no_of_loads_per[i] * weight;
    }
    if(document.getElementById("distance_travel").checked == true){
      finaldistanceArray.push(distanceDriven[i]);
      finaldistanceArray_per.push(distanceDriven_per[i]);
      productivityScore += distanceDriven_per[i] * weight;
    }
    productivityPercent.push(Math.round(productivityScore * 100) / 100);

    if(document.getElementById("overspeeds").checked == true){
      finaloverspeedArray.push(overspeeds[i]);
      finaloverspeedArray_per.push(overspeeds_per[i]);
      safetyScores += overspeeds_per[i] * weightSafety;
    }
    if(document.getElementById("impacts").checked == true){
      finalimpactArray.push(impacts[i]);
      finalimpactArray_per.push(impacts_per[i]);
      safetyScores += impacts_per[i] * weightSafety;
    }
    if(document.getElementById("seatbelt_violation").checked == true){
      finalseatbeltArray.push(seatbeltArray[i]);
      finalseatbeltArray_per.push(seatbeltArray_per[i]);
      safetyScores += seatbeltArray_per[i] * weightSafety;
    }
    if(document.getElementById("parkbrake_violation").checked == true){
      finalparkbrakeArray.push(parkbrakeArray[i]);
      finalparkbrakeArray_per.push(parkbrakeArray_per[i]);
      safetyScores += parkbrakeArray_per[i] * weightSafety;
    }
    safetyPercent.push(Math.round(safetyScores * 100) / 100);

    /*console.log()*/

    if(document.getElementById("idle_time").checked == true || document.getElementById("working_time").checked == true || document.getElementById("hydraulic_time").checked == true || document.getElementById("no_of_loads").checked == true || document.getElementById("distance_travel").checked == true){
        if(productivityPercent[i] >= 90){// Changing threshold values
          var color = 'Green';
        }
        if(productivityPercent[i] > 0 && productivityPercent[i] <= 60){ 
          var color = 'red';
        }
        if(productivityPercent[i] > 60 && productivityPercent[i] < 90){
          var color = "#ff8300";  /* #ff982b */
        }
        data = '{ "y" :'+productivityPercent[i]+',"color" : "' +color+'" }' ;
        finalproductivityScoresArray.push(JSON.parse(data));
    } /* end of if productivity */

    //initialize safetyScoreArray if any of health/safety option is checked
    if(document.getElementById("overspeeds").checked == true || document.getElementById("impacts").checked == true || document.getElementById("seatbelt_violation").checked == true || document.getElementById("parkbrake_violation").checked == true){
      if(safetyPercent[i] >= 65){
        var color = 'Green';
      }
      if(safetyPercent[i] > 0 && safetyPercent[i] <= 35 ){ 
        var color = 'red';
      }
      if(safetyPercent[i] > 35 && safetyPercent[i] < 65){
        var color = "#ff8300";  /* #ff982b */
      }
      data = '{ "y" :'+safetyPercent[i]+',"color" : "' +color+'" }' ;
      finalsafetyScoreArray.push(JSON.parse(data));
    } /* end of if health safety check */
  } /* end of for loop */

  //Productivity -1st graph
  var series = [{
    name: 'Productivity',
    data: finalproductivityScoresArray
  }, {
    name: 'Health/Safety',
    data: finalsafetyScoreArray
  }];

  $("#scorecard_container").show();
  $("#productivity").show();

  Highcharts.chart('scorecard_chart', {
    colors: ['#000'],
    chart: {
      backgroundColor: '#2d3035',
      type: 'column'
    },
    title: {
      text: 'Operator Score Card',
      style: {
        color : 'white',
        fontSize : '16px',
        fontWeight: 'bold'
      }
    },
    xAxis: {
      gridLineWidth: 0,
      minorGridLineWidth: 0,
      categories: oper,
      color: "white",
      crosshair: true,
      title: {
        text: 'Operators',
        style: {
          color : 'white',
        }
      },
      labels : {
        style: {
          color : 'white',
        }
      }
    },
    yAxis: {
      gridLineWidth: 0,
      minorGridLineWidth: 0,
      min: 0,
      max : 300,
      color: "white",
      title: {
        text: 'Score (%)'
      },
      labels : {
        enabled : false,
        style: {
          color : 'white',
        }
      },
      stackLabels: {
        enabled: false,
        style: {
          fontWeight: 'bold',
          color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
        }
      }
    },
    legend: {
      enabled: true,
      itemStyle:{color : "white"}
      
    },
    /*legend: {
      itemStyle: {
          color: '#fff'
      }
    },*/
    tooltip: {
      headerFormat: '<b>{point.x}</b><br/>',
      pointFormat: '{series.name}: {point.y} %'
    },
    plotOptions: {
      column: {
            /*pointPadding: 0.2,
            borderWidth: 0,*/
        borderColor: '#000',
        stacking: 'normal',
        dataLabels: {
          enabled: true,
          color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
        }
      }
    },
    series: series
  }); /* Score card chart*/

  var prod_tbl = "<table class='table table-hover table-bordered col-lg-12' style='color: white; text-align: center; width:100%; '>";
  prod_tbl+= "<tr>";
  prod_tbl+= "<th class='bg_tblHeader'>Operator name</th>";
  var x=1;

  /*console.log(document.getElementById("container").offsetWidth);*/

  var containerWidth = document.getElementById("container").offsetWidth;
  console.log(containerWidth);

  if(document.getElementById("idle_time").checked == true)
  {
    prod_tbl+= "<th class='bg_tblHeader'>Idle time <br> (% scored)</th>";
    x++;
  }
  if(document.getElementById("working_time").checked == true)
  {
    prod_tbl+= "<th class='bg_tblHeader'>Working time <br> (% scored)</th>";
    x++;
  }
  if(document.getElementById("hydraulic_time").checked == true)
  {
    prod_tbl+= "<th class='bg_tblHeader'>Hydraulic time <br> (% scored)</th>";
    x++;
  }
  if(document.getElementById("no_of_loads").checked == true)
  {
    prod_tbl+= "<th class='bg_tblHeader'>Number of loads <br> (% scored)</th>";
    x++;
  }
  if(document.getElementById("distance_travel").checked == true)
  {
    prod_tbl+= "<th class='bg_tblHeader'>Distance travelled <br> (% scored)</th>";
    x++;
  }

/*  console.log($("#container").width());*/

  x = 100.0/x;
  prod_tbl+= "</tr>";
  
  //add rows
  for(var i=0; i < operatorNames.length; i++)
  {
    prod_tbl+= "<tr class='col-lg-12'>";
    prod_tbl+= "<td class='bg_tblHeader'>"+ operatorNames[i] +"</td>";

    if(document.getElementById("idle_time").checked == true)
    {
      bgcolor="";
      if(finalIdleArray_per[i] < 60) // Thresholds changed to achive 90% productivity
        bgcolor = "#f52626f7";
      else if(finalIdleArray_per[i] >=60 && finalIdleArray_per[i] <90)
        bgcolor = "#f99123";
      else if(finalIdleArray_per[i] >=90)
        bgcolor = "#199f19";

      prod_tbl+= "<td style='background: "+bgcolor+"' width='"+x+"%'>"+ finalIdleArray_per[i] +"</td>";
    }
    if(document.getElementById("working_time").checked == true)
    {
      bgcolor="";
      if(finalWorkingArray_per[i] >= 90) 
        bgcolor = "#199f19";
      else if(finalWorkingArray_per[i] >= 60 && finalWorkingArray_per[i] < 90)
        bgcolor = "#f99123";
      else if(finalWorkingArray_per[i] < 60)
        bgcolor = "#f52626f7";

      prod_tbl+= "<td style='background: "+bgcolor+"' width='"+x+"%'>"+ finalWorkingArray_per[i] +"</td>";
    }
    if(document.getElementById("hydraulic_time").checked == true)
    {
      bgcolor="";
      if(finalhydraulicArray_per[i] >= 90) 
        bgcolor = "#199f19";
      else if(finalhydraulicArray_per[i] >= 60 && finalhydraulicArray_per[i] < 90)
        bgcolor = "#f99123";
      else if(finalhydraulicArray_per[i] < 60)
        bgcolor = "#f52626f7";

      prod_tbl+= "<td style='background: "+bgcolor+"' width='"+x+"%'>"+ finalhydraulicArray_per[i] +"</td>";
    }
    if(document.getElementById("no_of_loads").checked == true)
    {
      bgcolor="";
      if(finalNumLoadsArray_per[i] >= 90) 
        bgcolor = "#199f19";
      else if(finalNumLoadsArray_per[i] >= 60 && finalNumLoadsArray_per[i] < 90)
        bgcolor = "#f99123";
      else if(finalNumLoadsArray_per[i] < 60)
        bgcolor = "#f52626f7";

      prod_tbl+= "<td style='background: "+bgcolor+"' width='"+x+"%'>"+ finalNumLoadsArray_per[i] +"</td>";
    }
    if(document.getElementById("distance_travel").checked == true)
    {
      bgcolor="";
      if(finaldistanceArray_per[i] < 35) 
         bgcolor = "#f52626f7";
      else if(finaldistanceArray_per[i] >=35 && finaldistanceArray_per[i] <=65)
        bgcolor = "#f99123";
      else if(finaldistanceArray_per[i] >65)
        bgcolor = "#199f19";

      prod_tbl+= "<td style='background: "+bgcolor+"' width='"+x+"%'>"+ finaldistanceArray_per[i] +"</td>";
    }
    prod_tbl+= "</tr>";
  }

  //add table in container
  $("#container").html(prod_tbl);

  //Productivity -3rd graph
  //store operator names in oper array
  //var oper = [];
  /*for(var m=0;m<operatorNames.length; m++)
  {
    oper.push(operatorNames[m]["name"]);
  }*/
  //oper = operatorNames;
  labels = oper;
  var series = [];
  var showTimeGraph = 0;
    
  if(document.getElementById("working_time").checked == true)
  {
    series.push({
      name: "Working Time",
      data : finalWorkingArray,
      color: 'rgba(165,170,217,1)',
      pointPadding: 0.3,
      pointPlacement: -0.2,
      tooltip: {
          valueSuffix: ' hrs'
        },
    });
    showTimeGraph = 1;
  }
  if(document.getElementById("hydraulic_time").checked == true)
  {
    series.push({
      name: "Hydraulic Time",
      data : finalhydraulicArray,
      color: 'rgba(126,86,134,.9)',
      pointPadding: 0.4,
      pointPlacement: -0.2,
      tooltip: {
          valueSuffix: ' hrs'
        },
    });
    showTimeGraph = 1;
  }
  if(document.getElementById("idle_time").checked == true)
  {
    series.push({
      name: "Idle Time",
      data : finalIdleArray,
      color: 'rgba(248,161,63,1)',
      pointPadding: 0.3,
      pointPlacement: 0.2,
      tooltip: {
        valueSuffix: ' hrs'
      },
    });
    showTimeGraph = 1;
  }


  if(showTimeGraph == 1)
  {
    $("#individual_productivity").html('');
    Highcharts.chart('individual_productivity', {
      chart: {
        type: 'column',
        backgroundColor: '#2d3035',
        height: 500,
      },
      title: {
        text: 'Operation Hours',
        style: {
          color : 'white',
          fontSize : '16px',
          fontWeight: 'bold'
        }
      },
      credits: {
        enabled: false
      },
      xAxis: {
        categories: oper,
        color: "white",
        crosshair: true,
        title: {
          text: 'Operators',
          style: {
            color : 'white'
          } 
        },
        labels: { style: {
          color : 'white'
          }
        }
      },
      yAxis: {
        min: 0,
        max: 50,
        allowDecimals: true,
        /*max: 375,*/
        title: {
            text: 'Time (hrs)'
        }
      },
      legend: {
        reversed: true,
       itemStyle: {
          color: '#fff'
        }
      },
      tooltip: {
        //shared: true
      },
      plotOptions: {
        column: {
          grouping: false,
          shadow: false,
          borderWidth: 0,
          groupPadding: 0
        }
      },
      series: series
    });
    $("#individual_productivity").css("display","block");
  }
  else
  {
    $("#individual_productivity").html('');
    $("#individual_productivity").css("display","none");
  }

  var series1 = [];
  var showNoOfLoadsGraph = 0;
  // var showDistanceGraph = 0;

  if(document.getElementById("no_of_loads").checked == true)
  {
    series1.push({
      name: "Load count",
      data : finalNumLoadsArray
    });
    showNoOfLoadsGraph = 1;
  }

  if(showNoOfLoadsGraph == 1)
  {
    $("#individual_productivity2").html('');
    Highcharts.chart('individual_productivity2', {
      chart: {
        backgroundColor: '#2d3035',
        height: 500,
        type: 'bar'
      },
      title: {
        text: 'Loads picked by operator',
        style: {
          color : 'white',
          fontSize : '16px',
          fontWeight: 'bold'
        }
      },
      xAxis: {
        categories: oper,
        color: "white",
        crosshair: true,
        title: {
          text: 'Operators',
          style: {
            color : 'white'
          } 
        },
        labels: { style: {
          color : 'white'
        }}
      },
      yAxis: {
        min: 0,
        max: 600,
        allowDecimals: false,
        /*max: 375,*/
        title: {
          text: ''
        }
      },
      legend: {
        reversed: true,
        itemStyle: {
          color: '#fff'
        }
      },
      plotOptions: {
       bar: {
          pointPadding: 0.2,
          borderWidth: 0
        },
        series: {
          stacking: 'normal'
        }
      },
      series: series1
    });
    $("#individual_productivity2").css("display","block");
  }
  else
  {
    $("#individual_productivity2").html('');
    $("#individual_productivity2").css("display","none");
  }

  var series3 = [];
  var showDistanceGraph = 0;

  if(document.getElementById("distance_travel").checked == true)
  {
    series3.push({
      name: "Distance Travelled (kms)",
      data : finaldistanceArray
    });
    showDistanceGraph = 1;
  }

  if(showDistanceGraph == 1)
  {
    $("#individual_productivity3").html('');
    Highcharts.chart('individual_productivity3', {
      chart: {
        backgroundColor: '#2d3035',
        height: 500,
        type: 'bar'
      },
      title: {
        text: 'Distance travelled by operator',
        style: {
          color : 'white',
          fontSize : '16px',
          fontWeight: 'bold'
        }
      },
      xAxis: {
        categories: oper,
        color: "white",
        crosshair: true,
        title: {
          text: 'Operators',
          style: {
            color : 'white'
          } 
        },
        labels: { style: {
          color : 'white'
        }}
      },
      yAxis: {
        min: 0,
        max: 350,
        allowDecimals: false,
        /*max: 375,*/
        title: {
            text: ''
        }
      },
      legend: {
        reversed: true,
        itemStyle: {
          color: '#fff'
        }
      },
      plotOptions: {
        bar: {
          pointPadding: 0.2,
          borderWidth: 0
        },
        series: {
          stacking: 'normal'
        }
      },
      series: series3
    });

    $("#individual_productivity3").css("display","block");
  }
  else
  {
    $("#individual_productivity3").html('');
    $("#individual_productivity3").css("display","none");
  }

  var safty_tbl = "<table class='table table-hover table-bordered' style='color: white; text-align: center; ' width=100%>";

  //create column headers
  safty_tbl+= "<tr>";
  safty_tbl+= "<th class='bg_tblHeader'>Operator name</th>";
  var x=1;
  if(document.getElementById("overspeeds").checked == true)
  {
    safty_tbl+= "<th class='bg_tblHeader'>Overspeed count <br> (% scored)</th>";
    x++;
  }
  if(document.getElementById("impacts").checked == true)
   {
    safty_tbl+= "<th class='bg_tblHeader'>Impacts count <br> (% scored)</th>";
    x++;
  }
  if(document.getElementById("seatbelt_violation").checked == true)
   {
    safty_tbl+= "<th class='bg_tblHeader'>Seatbelt violation count <br> (% scored)</th>";
    x++;
  }
  if(document.getElementById("parkbrake_violation").checked == true)
  {
    safty_tbl+= "<th class='bg_tblHeader'>Parkbrake violation count <br> (% scored)</th>";
    x++;
  }
  
  
  /*x="col-lg-4";*/

  x = 100.0/x;
  safty_tbl+= "</tr>";

  //add rows
  for(var i=0; i < operatorNames.length; i++)
  {
    safty_tbl+= "<tr>";
    safty_tbl+= "<td class='bg_tblHeader' width='"+x+"%'>"+ operatorNames[i]+"</td>";

    if(document.getElementById("overspeeds").checked == true)
    {
      bgcolor="";
      if(finaloverspeedArray_per[i] < 35) 
        bgcolor = "#f52626f7";
      else if(finaloverspeedArray_per[i] >= 35 && finaloverspeedArray_per[i] <= 65)
        bgcolor = "#f99123";
      else if(finaloverspeedArray_per[i] > 65)
        bgcolor = "#199f19";

      safty_tbl+= "<td style='background: "+bgcolor+"' width='"+x+"%'>"+ finaloverspeedArray_per[i] +"</td>";
    }
    if(document.getElementById("impacts").checked == true)
    {
      bgcolor="";
      if(finalimpactArray_per[i] < 35) 
        bgcolor = "#f52626f7";
      else if(finalimpactArray_per[i] >= 35 && finalimpactArray_per[i] <=65)
        bgcolor = "#f99123";
      else if(finalimpactArray_per[i] > 65)
        bgcolor = "#199f19";
        
      safty_tbl+= "<td style='background: "+bgcolor+"' width='"+x+"%'>"+ finalimpactArray_per[i] +"</td>";
    }
    if(document.getElementById("seatbelt_violation").checked == true)
    {
      bgcolor="";
      if(finalseatbeltArray_per[i] < 35) 
        bgcolor = "#f52626f7";
      else if(finalseatbeltArray_per[i] >=35 && finalseatbeltArray_per[i] <=65)
        bgcolor = "#f99123";
      else if(finalseatbeltArray_per[i] > 65)
        bgcolor = "#199f19";
       
      safty_tbl+= "<td style='background: "+bgcolor+"' width='"+x+"%'>"+ finalseatbeltArray_per[i] +"</td>";
    }
    if(document.getElementById("parkbrake_violation").checked == true)
    {
      bgcolor="";
      if(finalparkbrakeArray_per[i] < 35) 
        bgcolor = "#f52626f7";
      else if(finalparkbrakeArray_per[i] >= 35 && finalparkbrakeArray_per[i] <= 65)
        bgcolor = "#f99123";
      else if(finalparkbrakeArray_per[i] > 65)
        bgcolor = "#199f19";

      safty_tbl+= "<td style='background: "+bgcolor+"' width='"+x+"%'>"+ finalparkbrakeArray_per[i] +"</td>";
    }
    safty_tbl+= "</tr>";
  }

  //add table in container
  $("#safety_container").html(safty_tbl);

  series = [];
  if(document.getElementById("parkbrake_violation").checked == true)
  {
    series.push({
      name: "parkBrake Violation",
      data : finalparkbrakeArray
    });
  }
  if(document.getElementById("seatbelt_violation").checked == true)
  {
    series.push({
      name: "Seatbelt Violation",
      data : finalseatbeltArray
    });
  }
  if(document.getElementById("impacts").checked == true)
  {
    series.push({
      name: "Impacts",
      data : finalimpactArray
    });
  }
  if(document.getElementById("overspeeds").checked == true)
  {
    series.push({
      name: "OverSpeed",
      data : finaloverspeedArray
    });
  }
            
  Highcharts.chart('individual_safety', {
    chart: {
      backgroundColor: '#2d3035',
      type: 'bar'
    },
    title: {
      text: 'Health/Safety',
      style: {
        color : 'white',
        fontSize : '16px',
        fontWeight: 'bold'
      }
    },
    xAxis: {
      categories: labels
    },
    yAxis: {
      min: 0,
      allowDecimals: false,
      //max: 55,
      title: {
          text: ''
      },
      color: 'white',
      crosshair: true
    },
    legend: {
      reversed: true,
      itemStyle: {
        color: '#fff'
      }
    },
    plotOptions: {
      bar: {
        pointPadding: 0.2,
        borderWidth: 0
      },
      series: {
        stacking: 'normal'
      }
    },
    series: series
  });

  //show graphs as per selected tabs
  $("#timeGraph").click(function(){
    $("#individual_productivity").css("display","block");
    $("#individual_productivity2").css("display","none");
    $("#individual_productivity3").css("display","none");

    //apply box shadow to selected tab only
    $("#timeGraph").addClass("tabBoxShadow");
    $("#loadsGraph").removeClass("tabBoxShadow");
    $("#distanceGraph").removeClass("tabBoxShadow");
  });

  $("#loadsGraph").click(function(){
    $("#individual_productivity").css("display","none");
    $("#individual_productivity2").css("display","block");
    $("#individual_productivity3").css("display","none");

    //apply box shadow to selected tab only
    $("#timeGraph").removeClass("tabBoxShadow");
    $("#loadsGraph").addClass("tabBoxShadow");
    $("#distanceGraph").removeClass("tabBoxShadow");
  });

  $("#distanceGraph").click(function(){
    $("#individual_productivity").css("display","none");
    $("#individual_productivity2").css("display","none");
    $("#individual_productivity3").css("display","block");

    //apply box shadow to selected tab only
    $("#timeGraph").removeClass("tabBoxShadow");
    $("#loadsGraph").removeClass("tabBoxShadow");
    $("#distanceGraph").addClass("tabBoxShadow");
  });
}